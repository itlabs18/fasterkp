﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AdminWeb.Models;

namespace AdminWeb.Controllers
{
    public class AccountController : Controller
    {
	    private string password = "technology10";
		// GET: Account
		public ActionResult Admin()
		{
			if (User.Identity.IsAuthenticated)
			{
				return View();
			}
			return RedirectToAction("Index", "Home");
		}
	    public ActionResult CreateUser(UserModel model)
	    {
		    if (User.Identity.IsAuthenticated)
		    {
		    model = model ?? new UserModel();
		    return View(new RegisterModel
		    {
			    Name = model.Name,
			    Password = model.Password,
			    ID = model.ID,
			    Login = model.Login,
			    Image_name = model.Image_name,
			    Is_admin = model.Is_admin,
			    Surname = model.Surname,
			    Telefon_number = model.Telefon_number,
			    Position = model.Position,
			    Is_active = false
			});
		    }
		    return RedirectToAction("Index", "Home");
		}
		public ActionResult Login()
        {
            return View();
        }

	    [HttpPost]
	    public ActionResult Imageupload(object imagedownload)
	    {
		    HttpPostedFileBase[] posted = (HttpPostedFileBase[])imagedownload;

		    return PartialView("Imageupload", ImageDownload(posted[0].InputStream, posted[0].FileName));

	    }

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Login(LoginModel model)
		{
			if (ModelState.IsValid)
			{
				// поиск пользователя в бд

				UserModel user = null;
				using (UserContext db = new UserContext())
				{
					user = db.UserModels.FirstOrDefault(u => u.Login == model.Login && u.Password == model.Password);

				}
				//if (user == null)
				//{
				//	// создаем нового пользователя
				//	using (UserContext db = new UserContext())
				//	{
				//		db.UserModels.AddOrUpdate(new UserModel
				//		{
				//			Name = "Василий",
				//			Password = model.Password,
				//			ID = 1,
				//			Login = model.Login,
				//			Image_name = "картинка.jpg",
				//			Is_admin = true,
				//			Surname = "Тёркин",
				//			Telefon_number = "88002353555",
				//			Position = "Начальник всея руси",
				//			Is_active = false
				//		});
				//		db.SaveChanges();

				//		user = db.UserModels.FirstOrDefault(u => u.Login == model.Login && u.Password == model.Password);
				//	}
				//	// если пользователь удачно добавлен в бд
				//	if (user != null)
				//	{
				//		FormsAuthentication.SetAuthCookie(model.Login, true);
				//		return RedirectToAction("Admin", "Account");
				//	}
				//}
				if (user != null && user.Is_admin)
				{
					FormsAuthentication.SetAuthCookie(model.Login, true);
					return RedirectToAction("Admin", "Account");
				}
				else
				{
					ModelState.AddModelError("", "Пользователя с таким логином и паролем нет");
				}
			}

			return View();
		}

	    [HttpPost]
		public  ActionResult UserAction (UserModel model, string action) 
	    {
		    UserModel user = null;
		    using (UserContext db = new UserContext())
		    {
			    user = db.UserModels.FirstOrDefault(u => u.ID == model.ID);
			    switch (action)
			    {
				    case "Kick":
					   if (user != null)
					   {
						    user.Is_active = false;
							db.UserModels.AddOrUpdate(user);
						   db.SaveChanges();
							return RedirectToAction("Admin", "Account");
					   }
					   else
					   {
						   ModelState.AddModelError("", "Такого пользователя не существует");
					   }
					   break;
					case "Update":
						if (user != null)
						{
							return RedirectToAction("CreateUser", "Account", user);
						}
						else
						{
							ModelState.AddModelError("", "Такого пользователя не существует");
						}
						break;
				    case "Delete":
					    if (user != null)
					    {
						    db.UserModels.Remove(user);
						    db.SaveChanges();
						    return RedirectToAction("Admin", "Account");
					    }
					    else
					    {
						    ModelState.AddModelError("", "Такого пользователя не существует");
					    }
					    break;
				}
		    }
			return View();
	    }

		public ActionResult Register()
		{
			return View();
		}
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Register(RegisterModel model)
		{
			
			//model.Image_name = model.Image_name ?? posted[0].FileName;
			
				UserModel user = null;
				using (UserContext db = new UserContext())
				{
					user = db.UserModels.FirstOrDefault(u => u.Login == model.Login);
				}
				if (user == null)
				{
					// создаем нового пользователя
					using (UserContext db = new UserContext())
					{
						db.UserModels.AddOrUpdate(new UserModel
						{
							Name = model.Name,
							Password = model.Password,
							ID = 0,
							Login = model.Login,
							Image_name = model.Image_name,
							Is_admin = model.Is_admin,
							Surname = model.Surname,
							Telefon_number = model.Telefon_number,
							Position = model.Position,
							Is_active = false
						});
						db.SaveChanges();

						user = db.UserModels.FirstOrDefault(u => u.Login == model.Login && u.Password == model.Password);
					}
					// если пользователь удачно добавлен в бд
					if (user != null)
					{
						using (SmtpClient client = new SmtpClient("smtp.mail.ru"))
						{
							client.EnableSsl = true;
							client.Port = 25;
							client.Credentials = new NetworkCredential("it@bm-technology.ru", password);
							MailMessage mailMessage = new MailMessage();
							mailMessage.From = new MailAddress("it@bm-technology.ru", "IT отдел BMGroup");
							mailMessage.To.Add(model.Login);
							mailMessage.Subject = "Новый аккаунт для КАБАН";
							mailMessage.Body = "Здравствуйте!"+ Environment.NewLine +
							                   "Поздравляем вас! В проекте КАБАН для вас зарегистрирован пользователь " +
							                   model.Name + " " + model.Surname + "!"+ Environment.NewLine +
							                   "Ваш логин: " + model.Login + Environment.NewLine +
							                   "Ваш пароль: " + model.Password + Environment.NewLine +
							                   "С уважением,"+ Environment.NewLine + " BM Group";
							client.Send(mailMessage);
						}
						return RedirectToAction("ResultView","Home",new ResultModel(){Text = "Пользователь добавлен"});
					}
				}
				else
				{
					using (UserContext db = new UserContext())
				{
					if (db.UserModels.Find(model.ID).Login != model.Login)
					{
						using (SmtpClient client = new SmtpClient("smtp.mail.ru"))
						{
							client.EnableSsl = true;
							client.Port = 25;
							client.Credentials = new NetworkCredential("it@bm-technology.ru", password);
							MailMessage mailMessage = new MailMessage();
							mailMessage.From = new MailAddress("it@bm-technology.ru", "IT отдел BMGroup");
							mailMessage.To.Add(model.Login);
							mailMessage.Subject = "Новый аккаунт для КАБАН";
							mailMessage.Body = "Здравствуйте!" + Environment.NewLine+
											   "Поздравляем вас! В проекте КАБАН для вас зарегистрирован пользователь " +
							                   model.Name + " " + model.Surname + "!"+ Environment.NewLine +
							                   "Ваш логин: " + model.Login + Environment.NewLine +
							                   "Ваш пароль: " + model.Password + Environment.NewLine +
							                   "С уважением,"+ Environment.NewLine + "BM Group";
							client.Send(mailMessage);
						}
					}
					else
					{
						using (SmtpClient client = new SmtpClient("smtp.mail.ru"))
						{
							client.EnableSsl = true;
							client.Port = 25;
							client.Credentials = new NetworkCredential("it@bm-technology.ru", password);
							MailMessage mailMessage = new MailMessage();
							mailMessage.From = new MailAddress("it@bm-technology.ru","IT отдел BMGroup");
							mailMessage.To.Add(model.Login);
							mailMessage.Subject = "Аккаунт для КАБАН";
							mailMessage.Body = "Здравствуйте!"+ Environment.NewLine +
							                   "Ваш аккаунт в КАБАН " +
							                   model.Name + " " + model.Surname + "изменён!"+ Environment.NewLine +
							                   "Ваш логин: " + model.Login + Environment.NewLine +
							                   "Ваш пароль: " + model.Password + Environment.NewLine +
							                   "С уважением,"+Environment.NewLine+" BM Group";
							client.Send(mailMessage);
						}
					}

					db.UserModels.AddOrUpdate(new UserModel
						{
							Name = model.Name,
							Password = model.Password,
							ID = model.ID,
							Login = model.Login,
							Image_name = model.Image_name,
							Is_admin = model.Is_admin,
							Surname = model.Surname,
							Telefon_number = model.Telefon_number,
							Position = model.Position,
							Is_active = model.Is_active
						});
						db.SaveChanges();

						user = db.UserModels.FirstOrDefault(u => u.Login == model.Login && u.Password == model.Password);
						if (user != null)
						{
							
						return RedirectToAction("ResultView", "Home", new ResultModel() { Text = "Данные пользователя изменены" });
						}

				}

				}
			return RedirectToAction("ResultView", "Home", new ResultModel() { Text = "Что то пошло не так :(" });
		}
		public ActionResult Logoff()
		{
			FormsAuthentication.SignOut();
			return RedirectToAction("Index", "Home");
		}

	    private string ImageDownload(Stream stream, string filename)
	    {
			    if (stream.Length != 0)
			    {
				    Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + @"Images");
				    using (FileStream fileStream =
					    System.IO.File.Create(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + @"Images\" + filename,
						    (int)stream.Length))
				    {
					    byte[] data = new byte[stream.Length];
					    stream.Read(data, 0, (int)data.Length);
					    fileStream.Write(data, 0, data.Length);
				    }
			    }
			    return filename;
	    }
    }
}