﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;
using AdminWeb.Models;
using Microsoft.Office.Interop.Excel;

namespace AdminWeb.Controllers
{
    public class FilesController : Controller
    {
        // GET: Files
        public ActionResult Tables()
		{
			//if (User.Identity.IsAuthenticated)
			//{
				return View();
			//}
			//return RedirectToAction("Index", "Home");
		}

	    public CollectHeads Heads = new CollectHeads();

		[System.Web.Mvc.HttpPost]
	    [ValidateAntiForgeryToken]
	    public ActionResult DownloadFile(string path, string click, object file)
	    {
		    try
		    {
			    HttpPostedFileBase[] posted = (HttpPostedFileBase[]) file;
			    Stream stream = posted[0].InputStream;
			   
			    switch (click)
			    {
				    case "Body19,5":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\19,5\Body.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Body21,5":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\21,5\Body.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Body32":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\32\Body.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Body43":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\43\Body.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Body48":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\48\Body.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Body49":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\49\Body.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Body55":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\55\Body.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Body65":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\65\Body.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Body70":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\70\Body.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Typesensor19,5":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\19,5\Typesensor.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Typesensor21,5":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\21,5\Typesensor.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Typesensor32":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\32\Typesensor.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Typesensor43":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\43\Typesensor.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Typesensor48":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\48\Typesensor.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Typesensor49":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\49\Typesensor.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Typesensor55":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\55\Typesensor.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Typesensor65":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\65\Typesensor.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Typesensor70":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\70\Typesensor.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Computer":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\Computer.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "Display":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\Display.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;
				    case "ExtraDevice":
					    using (FileStream fileStream =
						    System.IO.File.Create(
							    System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
							    $@"base\{path}\ExtraDevice.xlsx",
							    (int) stream.Length))
					    {
						    byte[] data = new byte[stream.Length];
						    stream.Read(data, 0, (int) data.Length);
						    fileStream.Write(data, 0, data.Length);
					    }

					    ;
					    break;

			    }

			    return RedirectToAction("ResultView", "Home", new ResultModel{Text = "Успех!"});
		    }
		    catch 
		    {
			    return RedirectToAction("ResultView", "Home", new ResultModel { Text = "Что-то пошло не так :(" });
			}

	    }
	    [System.Web.Mvc.HttpPost]
		public ActionResult SetDollar(string dollar)
	    {
			FileStream fs = System.IO.File.Create(HostingEnvironment.ApplicationPhysicalPath + "dollar.txt");
		    StreamWriter sw = new StreamWriter(fs);
		    sw.Write(dollar);
		    sw.Close();
			FileInfo fi = new FileInfo(HostingEnvironment.ApplicationPhysicalPath + "dollar.txt");
			return PartialView("SetDollar","Изменено: "+fi.LastWriteTime);
	    }
	    [System.Web.Mvc.HttpPost]
	    public ActionResult SetDirectory(string path)
	    {
			ObservableCollection<string[]> collect = new ObservableCollection<string[]>();
			collect.Add(Directory.GetDirectories(HostingEnvironment.ApplicationPhysicalPath+@"base/"+path));
			collect.Add(Directory.GetFiles(HostingEnvironment.ApplicationPhysicalPath + @"base/" + path));
			return PartialView(collect);
	    }
        [System.Web.Mvc.HttpPost]
        public ActionResult SelectNames(string Block, string Type, string Header, string Display)
        {
            ObservableCollection<DeviceModel> dv = new ObservableCollection<DeviceModel>();
            
            string _type = Type;
            switch (_type.Length)
            {
                case 1:
                    string fist = _type[0].ToString();
                    using (UserContext db = new UserContext())
                    {
                        foreach (var x in db.DeviceModels.Where(t => t.Block.Contains(Block) && t.Header.Contains(Header) && t.Display.Contains(Display) && t.Type.Contains(fist)))
                        {
                            dv.Add(x);
                        }
                    }
                    break;
                case 2:
                    fist = _type[0].ToString();
                    string second = _type[1].ToString();
                    using (UserContext db = new UserContext())
                    {
                        foreach (var x in db.DeviceModels.Where(t => t.Block.Contains(Block) && t.Header.Contains(Header) && t.Display.Contains(Display) && t.Type.Contains(fist) && t.Type.Contains(second)))
                        {
                            dv.Add(x);
                        }
                    }
                    break;
                case 3:
                    fist = _type[0].ToString();
                    second = _type[1].ToString();
                    string third = _type[2].ToString();
                    using (UserContext db = new UserContext())
                    {
                        foreach (var x in db.DeviceModels.Where(t => t.Block.Contains(Block) && t.Header.Contains(Header) && t.Display.Contains(Display) && t.Type.Contains(fist) && t.Type.Contains(second) && t.Type.Contains(third)))
                        {
                            dv.Add(x);
                        }
                    }
                    break;
                case 4:
                    fist = _type[0].ToString();
                    second = _type[1].ToString();
                    third = _type[2].ToString();
                    string forth = _type[3].ToString();
                    using (UserContext db = new UserContext())
                    {
                        foreach (var x in db.DeviceModels.Where(t => t.Block.Contains(Block) && t.Header.Contains(Header) && t.Display.Contains(Display) && t.Type.Contains(fist) && t.Type.Contains(second) && t.Type.Contains(third) && t.Type.Contains(forth)))
                        {
                            dv.Add(x);
                        }
                    }
                    break;
            }

            return PartialView("SelectNames1", dv);

        }
        [System.Web.Mvc.HttpPost]
        public ActionResult SelectNam(string Header)
        {
            ObservableCollection<DeviceModel> dv = new ObservableCollection<DeviceModel>();
            try
            {
                
                using (UserContext db = new UserContext())
                {
                    foreach (var xt in db.DeviceModels.Where(t=>t.Header== "Уличное исполнение Киоски"))
                    {
                        dv.Add(xt);
                        dv.LastOrDefault().Header = "Уличное исполнение Киоски Панели Терминалы";
                    }
                    foreach (var yDeviceModel in dv)
                    {
                        db.DeviceModels.AddOrUpdate(yDeviceModel);
                    }
                    dv = new ObservableCollection<DeviceModel>();
                    dv.Add(new DeviceModel { Name = "Всё ок" });
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                dv = new ObservableCollection<DeviceModel>();
                dv.Add(new DeviceModel { Name = e.Message });
            }
            return PartialView("SelectNames1", dv);

        }

        [System.Web.Mvc.HttpPost]
        public ActionResult SelectDescription(string Block, string Header, string Display, string Name)
        {
            ObservableCollection<DeviceModel> dv = new ObservableCollection<DeviceModel>();
            using (UserContext db = new UserContext())
            {
                foreach (var x in db.DeviceModels.Where(t => t.Block == Block && t.Header == Header && t.Display == Display && t.Name == Name))
                {
                    dv.Add(x);
                }
            }
            //dv.Add(new DeviceModel
            //{
            //    Block = "Display",
            //    Description = "sfsdgdsf",
            //    Display = "22",
            //    Header = "FullHD",
            //    ID = 1,
            //    Name = "Samsung",
            //    Price = "1200",
            //    Type = "1234"
            //});
            //dv.Add(new DeviceModel
            //{
            //    Block = "Display",
            //    Description = "ssss",
            //    Display = "23",
            //    Header = "FullHD",
            //    ID = 1,
            //    Name = "asdggss",
            //    Price = "1200",
            //    Type = "1234"
            //});
            //dv.Add(new DeviceModel
            //{
            //    Block = "Display",
            //    Description = "sfaaaaaaaaasdgdsf",
            //    Display = "22",
            //    Header = "FullHD",
            //    ID = 1,
            //    Name = "ssdda",
            //    Price = "1200",
            //    Type = "1234"
            //});
      
                return PartialView(dv);

        }

        [System.Web.Mvc.HttpPost]
        public ActionResult SelectPrice(string Block, string Header, string Display, string Name)
        {
            ObservableCollection<DeviceModel> dv = new ObservableCollection<DeviceModel>();
            using (UserContext db = new UserContext())
            {
                foreach (var x in db.DeviceModels.Where(t => t.Block == Block && t.Header == Header && t.Display == Display && t.Name == Name))
                {
                    dv.Add(x);
                }
            }
            //dv.Add(new DeviceModel
            //{
            //    Block = "Display",
            //    Description = "sfsdgdsf",
            //    Display = "22",
            //    Header = "FullHD",
            //    ID = 1,
            //    Name = "Samsung",
            //    Price = "1200",
            //    Type = "1234"
            //});
            //dv.Add(new DeviceModel
            //{
            //    Block = "Display",
            //    Description = "ssss",
            //    Display = "23",
            //    Header = "FullHD",
            //    ID = 1,
            //    Name = "asdggss",
            //    Price = "1200",
            //    Type = "1234"
            //});
            //dv.Add(new DeviceModel
            //{
            //    Block = "Display",
            //    Description = "sfaaaaaaaaasdgdsf",
            //    Display = "22",
            //    Header = "FullHD",
            //    ID = 1,
            //    Name = "ssdda",
            //    Price = "1200",
            //    Type = "1234"
            //});
            return PartialView(dv);

        }
        [System.Web.Mvc.HttpPost]
        public ActionResult ChangeType(string Block, string Header, string Display, string Name)
        {
            DeviceModel dv;
            using (UserContext db = new UserContext())
            {
                dv = db.DeviceModels.FirstOrDefault(t =>
                    t.Block == Block && t.Header == Header && t.Display == Display && t.Name == Name);
            }
            //dv.Add(new DeviceModel
            //{
            //    Block = "Display",
            //    Description = "sfsdgdsf",
            //    Display = "22",
            //    Header = "FullHD",
            //    ID = 1,
            //    Name = "Samsung",
            //    Price = "1200",
            //    Type = "1234"
            //});
            //dv.Add(new DeviceModel
            //{
            //    Block = "Display",
            //    Description = "ssss",
            //    Display = "23",
            //    Header = "FullHD",
            //    ID = 1,
            //    Name = "asdggss",
            //    Price = "1200",
            //    Type = "1234"
            //});
            //dv.Add(new DeviceModel
            //{
            //    Block = "Display",
            //    Description = "sfaaaaaaaaasdgdsf",
            //    Display = "22",
            //    Header = "FullHD",
            //    ID = 1,
            //    Name = "ssdda",
            //    Price = "1200",
            //    Type = "1234"
            //});
            return PartialView(dv);

        }
        [System.Web.Mvc.HttpPost]
        public ActionResult Additem(string Block, string Type, string Header, string Display, int Count)
        {
            ObservableCollection<DeviceModel> dv = new ObservableCollection<DeviceModel>();
            //using (UserContext db = new UserContext())
            //{
            //    foreach (var x in db.DeviceModels)
            //    {
            //        dv.Add(x);
            //    }
            //}
            for (int i = 0; i < Count; i++)
            {
                dv.Add(new DeviceModel
                {
                    Block = Block,
                    Display = Display,
                    Header = Header,
                    Type = Type
                });
            }
           
            return PartialView(dv);
        }
        [System.Web.Mvc.HttpPost]
        public ActionResult Saveitem(DeviceModel[] dv)
        {
            using (UserContext db = new UserContext())
            {
                foreach (var dm in dv)
                {
                    db.DeviceModels.Add(dm);
                    db.SaveChanges();
                }
            }
            return PartialView();
        }
        public ActionResult SaveChanges(DeviceModel dv, string met)
        {
            if (met == "delete")
            {
                using (UserContext db = new UserContext())
                {
                    db.DeviceModels.Remove(db.DeviceModels.FirstOrDefault(t=>t.ID==dv.ID));
                    db.SaveChanges();
                }
                return PartialView("SaveChanges", "../../Icons/x-button.png");
            }
            else
            {
                using (UserContext db = new UserContext())
                {
                    db.DeviceModels.AddOrUpdate(dv);
                    db.SaveChanges();
                }
                return PartialView("SaveChanges","../../Icons/ok-mark.png");
            }
           
        }

        //[HttpPost]
        //public ActionResult GetTable(string path)
        //{

        //	    Getter.Path = path;
        //		Application _appExcel = null;
        //		//Dispatcher Dispatcher = Dispatcher.CurrentDispatcher;
        //		try
        //		{
        //			_appExcel = new Application();
        //			Workbook _workbook = null;
        //			if (!string.IsNullOrEmpty(path))
        //			{
        //				//Dispatcher.Invoke(() =>
        //				//{
        //				_workbook = _appExcel.Workbooks.Open(path);
        //				//});
        //			}

        //			Worksheet _worksheet = (Worksheet)_workbook.Sheets[1];
        //			foreach (Range x in _worksheet.Columns[1].Cells)
        //			{
        //				if (x.Row > 50)
        //					break;
        //				string head = Convert.ToString(x.Value);
        //				if (!string.IsNullOrEmpty(head))
        //				{
        //					ObservableCollection<Item> items = new ObservableCollection<Item>();
        //					foreach (Range y in _worksheet.Rows[x.Row].Cells)
        //					{
        //						if (y.Column == x.Column)
        //							continue;
        //						if (y.Column > 20)
        //							break;
        //						Range p = _worksheet.Cells[y.Row + 1, y.Column];
        //						string property = Convert.ToString(y.Value);
        //						string price = Convert.ToString(p.Value);
        //						if (!string.IsNullOrEmpty(property) || !string.IsNullOrEmpty(price))
        //						{
        //							items.Add(new Item() { Price = price, Property = property });
        //						}
        //					}


        //					//Dispatcher.Invoke(() =>
        //					//{
        //					Heads.Headerses.Add(new Headers() { Head = head, Collection = items });
        //					//});
        //				}
        //			}

        //			//Dispatcher.Invoke(() =>
        //			//{
        //			_appExcel.ActiveWorkbook.Close(SaveChanges: false);
        //			_appExcel.Quit();
        //			//});
        //		}
        //		catch (Exception e)
        //		{
        //		//Dispatcher.Invoke(() =>
        //		//{
        //		_appExcel.ActiveWorkbook.Close(SaveChanges: false);
        //		_appExcel.Quit();
        //		//});
        //		Heads.Headerses.Add(new Headers() { Head = e.Message, Collection = null });
        //			return PartialView(Heads);
        //		}

        //		return PartialView(Heads);

        //}
        [System.Web.Mvc.HttpPost]
        public ActionResult GetTable([FromBody]string Type)
        {
            if (string.IsNullOrEmpty(Type))
            {
                return PartialView("PutDevice");
            }
            ObservableCollection<DeviceModel> dv = new ObservableCollection<DeviceModel>();
            
            string _type = Type;
            switch (_type.Length)
            {
                case 1:
                    string fist = _type[0].ToString();
                    using (UserContext db = new UserContext())
                    {
                        foreach (var x in db.DeviceModels.Where(t => t.Type.Contains(fist)))
                        {
                            dv.Add(x);
                        }
                    }
                    break;
                case 2:
                    fist = _type[0].ToString();
                    string second = _type[1].ToString();
                    using (UserContext db = new UserContext())
                    {
                        foreach (var x in db.DeviceModels.Where(t => t.Type.Contains(fist)&& t.Type.Contains(second)))
                        {
                            dv.Add(x);
                        }
                    }
                    break;
                case 3:
                    fist = _type[0].ToString();
                    second = _type[1].ToString();
                    string third = _type[2].ToString();
                    using (UserContext db = new UserContext())
                    {
                        foreach (var x in db.DeviceModels.Where(t => t.Type.Contains(fist)&& t.Type.Contains(second) && t.Type.Contains(third)))
                        {
                            dv.Add(x);
                        }
                    }
                    break;
                case 4:
                    fist = _type[0].ToString();
                    second = _type[1].ToString();
                    third = _type[2].ToString();
                    string forth = _type[3].ToString();
                    using (UserContext db = new UserContext())
                    {
                        foreach (var x in db.DeviceModels.Where(t => t.Type.Contains(fist)&& t.Type.Contains(second)&& t.Type.Contains(third)&& t.Type.Contains(forth)))
                        {
                            dv.Add(x);
                        }
                    }
                    break;
            }
            
            //dv.Add(new DeviceModel
            //{
            //    Block = "Display",
            //    Description = "sfsdgdsf",
            //    Display = "22",
            //    Header = "FullHD",
            //    ID = 1,
            //    Name = "Samsung",
            //    Price = "1200",
            //    Type = "1234"
            //});
            //dv.Add(new DeviceModel
            //{
            //    Block = "Display",
            //    Description = "ssss",
            //    Display = "23",
            //    Header = "FullHD",
            //    ID = 1,
            //    Name = "asdggss",
            //    Price = "1200",
            //    Type = "1234"
            //});
            //dv.Add(new DeviceModel
            //{
            //    Block = "Display",
            //    Description = "sfaaaaaaaaasdgdsf",
            //    Display = "22",
            //    Header = "FullHD",
            //    ID = 1,
            //    Name = "ssdda",
            //    Price = "1200",
            //    Type = "1234"
            //});
            //if (!dv.Any())
            //    return RedirectToAction("ResultView", "Home", new ResultModel { Text = "Проблемы с Базой Данных :(" });

            ViewBag.AllTypes = new List<string[]>();
            ViewBag.AllTypes.Add(new[] { "Интерактивные столы", "1" });
            ViewBag.AllTypes.Add(new[] { "Интерактивные панели", "2" });
            ViewBag.AllTypes.Add(new[] { "Сенсорные киоски", "3" });
            ViewBag.AllTypes.Add(new[] { "Сенсорные терминалы", "4" });
            ViewBag.AllTypes.Add(new[] { "Интерактивные столы, Интерактивные панели", "12" });
            ViewBag.AllTypes.Add(new[] { "Интерактивные столы, Сенсорные киоски", "13" });
            ViewBag.AllTypes.Add(new[] { "Интерактивные столы, Сенсорные терминалы", "14" });
            ViewBag.AllTypes.Add(new[] { "Интерактивные панели, Сенсорные киоски", "23" });
            ViewBag.AllTypes.Add(new[] { "Интерактивные панели, Сенсорные терминалы", "24" });
            ViewBag.AllTypes.Add(new[] { "Сенсорные терминалы, Сенсорные терминалы", "34" });
            ViewBag.AllTypes.Add(new[] { "Интерактивные столы, Интерактивные панели, Сенсорные киоски", "123" });
            ViewBag.AllTypes.Add(new[] { "Интерактивные столы, Интерактивные панели, Сенсорные терминалы", "124" });
            ViewBag.AllTypes.Add(new[] { "Интерактивные столы, Сенсорные киоски, Сенсорные терминалы", "134" });
            ViewBag.AllTypes.Add(new[] { "Интерактивные панели, Сенсорные киоски, Сенсорные терминалы", "234" });
            ViewBag.AllTypes.Add(new[] { "Все типы оборудования", "1234" });
            ViewBag.Types = Type;
            ViewBag.Tittles = Getter.TittleStrings(Type);
            
            return PartialView(dv);
        }

        [System.Web.Mvc.HttpPost]
	    public ActionResult Models(string path)
	    {

		    return View(Directory.GetDirectories(HostingEnvironment.ApplicationPhysicalPath + "Models"));

	    }
		[System.Web.Mvc.HttpPost]
	    public ActionResult SetTable(CollectHeads hdr)
	    {
		    string path = Getter.Path;
		    Application _appExcel = null;
		    //Dispatcher Dispatcher = Dispatcher.CurrentDispatcher;
		    try
		    {
			    _appExcel = new Application();
			    Workbook _workbook = null;
			    if (!string.IsNullOrEmpty(path))
			    {
				    //Dispatcher.Invoke(() =>
				    //{
				    _workbook = _appExcel.Workbooks.Open(path);
				    //});
			    }

			    Worksheet _worksheet = (Worksheet)_workbook.Sheets[1];
			    _worksheet.Cells.Delete();
			    int row = 1;
			    
			    foreach (Headers headCell in hdr.Headerses)
			    {
				    _worksheet.Cells[row, 1].Value = headCell.Head;
				    int i = 2;
				    foreach (Item it in headCell.Collection)
				    {
					    _worksheet.Cells[row, i].Value = it.Property;
					    _worksheet.Cells[row + 1, i].Value = it.Price;
					    i++;
				    }
				    row += 2;
					
			    }

			    //Dispatcher.Invoke(() =>
			    //{
			    _appExcel.ActiveWorkbook.Close(SaveChanges: true);
			    _appExcel.Quit();
			    //});
		    }
		    catch (Exception e)
		    {
				//Dispatcher.Invoke(() =>
				//{
				_appExcel.ActiveWorkbook.Close(SaveChanges: false);
				_appExcel.Quit();
				//});
			    return PartialView("SetTable", e.Message);
		    }
			FileInfo fi = new FileInfo(path);
		    return PartialView("SetTable", "Изменено: "+ fi.LastWriteTime);

	    }

	    [System.Web.Mvc.HttpGet]
	    public ActionResult LoginFile()
	    {
		    return View();
	    }

		[System.Web.Http.HttpGet]
		public ActionResult GetBaseClientManager(string login=null, string password=null)
		{
				try
				{//TODO: Выгрузка клиентской базы
					if (!Request.Form.HasKeys()||login!="Base10"||password!="pass12")
						return RedirectToAction("LoginFile","Files");
					BaseModel model = null;
				
					using (UserContext db = new UserContext())
					{
						Application excel = new Application();
						Workbook wb = null;
						if (System.IO.File.Exists(HostingEnvironment.ApplicationPhysicalPath + "excel1.xlsx"))
							System.IO.File.Delete(HostingEnvironment.ApplicationPhysicalPath + "excel1.xlsx");
						
						wb = excel.Workbooks.Add();
						Worksheet sheet = (Worksheet)wb.Sheets[1];
						sheet.Cells[1,1].Value = "Номер КП";
						sheet.Cells[1,2].Value = "Имя КП";
						sheet.Cells[1,3].Value = "Дата КП";
						sheet.Cells[1,4].Value = "Имя таблицы себестоимости";
						sheet.Cells[1,5].Value = "Дата таблицы себестоимости";
						sheet.Cells[1,6].Value = "Менеджер";
						sheet.Cells[1,7].Value = "Имя Менеджера";
						sheet.Cells[1,8].Value = "Фамилия Менеджера";
						sheet.Cells[1,9].Value = "Название компании";
						sheet.Cells[1,10].Value = "Контактное лицо";
						sheet.Cells[1,11].Value = "Контактные данные";
						for (int i = 2; i < db.BaseModels.Count()+2; i++)
						{
							sheet.Cells[i,1].Value  = db.BaseModels.Local[i-2].ID;
							sheet.Cells[i,2].Value  = db.BaseModels.Local[i-2].Name_KP;
							sheet.Cells[i,3].Value  = db.BaseModels.Local[i-2].Date_KP;
							sheet.Cells[i,4].Value  = db.BaseModels.Local[i-2].Name_SB;
							sheet.Cells[i,5].Value  = db.BaseModels.Local[i-2].Date_SB;
							sheet.Cells[i,6].Value  = db.BaseModels.Local[i-2].Login;
							sheet.Cells[i,7].Value  = db.BaseModels.Local[i-2].Name_Manager;
							sheet.Cells[i,8].Value  = db.BaseModels.Local[i-2].Surname_Manager;
							sheet.Cells[i,9].Value  = db.BaseModels.Local[i-2].Name_Company;
							sheet.Cells[i,10].Value = db.BaseModels.Local[i-2].Name_Client;
							sheet.Cells[i,11].Value = db.BaseModels.Local[i-2].Post_Client;
						}
						wb.SaveAs(HostingEnvironment.ApplicationPhysicalPath + "excel1.xlsx");
						wb.Close(SaveChanges: true);
						excel.Application.Quit();
						
					}
               
                FileStream fs = System.IO.File.OpenRead(HostingEnvironment.ApplicationPhysicalPath + "excel1.xlsx");
					return File(fs, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
					
				}
				catch (Exception e)
				{
					return null;
				}
		
		}
	}

	public static class Getter
	{
		public static string Dollar()
		{
			string x = null;
			if(File.Exists(HostingEnvironment.ApplicationPhysicalPath + "dollar.txt"))
			{
				FileStream fs = System.IO.File.OpenRead(HostingEnvironment.ApplicationPhysicalPath + "dollar.txt");
				StreamReader sw = new StreamReader(fs);
				x = sw.ReadToEnd();
				sw.Close();
			}
			return x;
	    }

	    public static string[] TittleStrings(string Type)
	    {
	        switch (Type)
	        {
	            case "1": return new []{"Интерактивные столы"};
                case "2": return new []{"Интерактивные панели"};
                case "3": return new []{"Сенсорные киоски"};
                case "4": return new []{"Сенсорные терминалы"};
	            case "12": return new[] { "Интерактивные столы", "Интерактивные панели" };
	            case "13": return new[] { "Интерактивные столы", "Сенсорные киоски" };
	            case "14": return new[] { "Интерактивные столы", "Сенсорные терминалы" };
	            case "23": return new[] { "Интерактивные панели", "Сенсорные киоски" };
	            case "24": return new[] { "Интерактивные панели", "Сенсорные терминалы" };
	            case "34": return new[] { "Сенсорные терминалы", "Сенсорные терминалы" };
                case "123": return new[] { "Интерактивные столы", "Интерактивные панели", "Сенсорные киоски" };
	            case "124": return new[] { "Интерактивные столы", "Интерактивные панели", "Сенсорные терминалы" };
	            case "134": return new[] { "Интерактивные столы", "Сенсорные киоски", "Сенсорные терминалы" };
	            case "234": return new[] { "Интерактивные панели", "Сенсорные киоски", "Сенсорные терминалы" };
	            case "1234": return new[] { "Все типы оборудования" };
	            default: return null;
            }


            
	    }

    private static string _path;
		public static string Path
		{
			get => _path;
			set => _path = value;
		}

		public static string Time()
		{
			FileInfo fi = new FileInfo(HostingEnvironment.ApplicationPhysicalPath + "dollar.txt");
			return fi.LastWriteTime.ToString();
		}
	} 
}