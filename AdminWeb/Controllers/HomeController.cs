﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminWeb.Models;

namespace AdminWeb.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View(new LoginModel());
		}
		public ActionResult ResultView(ResultModel resultModel)
		{
			return View("ResultView",resultModel);
		}

		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}
	}
}