﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Windows.Threading;
using AdminWeb.Models;

namespace AdminWeb.Controllers
{
    public class ImagesController : Controller
    {
        // GET: Images
        public ActionResult Models()
        {
            return View();
        }

        public ActionResult CheckFiles(string folder)
        {
           
            ObservableCollection<ImageModels> Modelses = new ObservableCollection<ImageModels>();
            System.Windows.Threading.Dispatcher Dispatcher = Dispatcher.CurrentDispatcher;
            var derDirectories = Directory.GetDirectories(HostingEnvironment.ApplicationPhysicalPath + $@"\Models");
            var files = Directory.GetFiles(derDirectories.FirstOrDefault(x => x.Contains(folder)));
            if (files.Length == 0)
            {
                return null;
            }
            foreach (var x in files.Where(t => t.Contains(".png") || t.Contains(".jpg")))
            {
                Dispatcher.Invoke(() =>
                {
                    FileInfo fileInfo = new FileInfo(x);
                    Modelses.Add(new ImageModels
                        { Name = fileInfo.Name.Replace(".png", "").Replace(".jpg", ""), Path = "http://195.133.1.197:2019/Api/table/getimage/?nameImage=" + fileInfo.Name + "&type=" + folder });

                });

            }

            return PartialView(Modelses);
        }

        public ActionResult DeleteFile(string folder, string name)
        {
            string[] derDirectories = Directory.GetDirectories(HostingEnvironment.ApplicationPhysicalPath + $@"\Models");
            System.IO.File.Delete(Directory.GetFiles(derDirectories.FirstOrDefault(x => x.Contains(folder))).FirstOrDefault(t => t.Contains(name)));
            Directory.GetFiles(derDirectories.FirstOrDefault(x => x.Contains(folder)))
                .FirstOrDefault(t => t.Contains(name));
            return CheckFiles(folder);
        }
    }
}