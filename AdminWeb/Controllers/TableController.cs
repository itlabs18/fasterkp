﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.WebPages.Razor;
using System.Windows.Media;
using System.Windows.Threading;
using AdminWeb.Models;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;

namespace AdminWeb.Controllers
{
	public class TableController : ApiController
	{
		private ObservableCollection<Headers> _collectionheads;
		public ObservableCollection<Headers> Collectionheads
		{
			get => _collectionheads ?? (_collectionheads = new ObservableCollection<Headers>());
			set => _collectionheads = value;
		}

		private ObservableCollection<ImageModels> _modelses;
		public ObservableCollection<ImageModels> Modelses
		{
			get => _modelses ?? (_modelses = new ObservableCollection<ImageModels>());
			set => _modelses = value;
		}

		private string apikey = "85252f5f6da47b658c460d5ea943175a";
		private string apitoken = "5bbc4447cca9da429cb5a3018b16e123dde93fe954c59d6ecfc30208ec15db37";

		[HttpGet]
		public async Task<ObservableCollection<BaseModel>> GetDocsAsync([FromUri] string login)
		{
			return await Task.Run(() =>
			{
				Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
				                          $"\\SaveDocs\\{login}");
				string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath +
				              $"SaveDocs\\{login}\\";
				var x = Directory.GetFiles(path);
				
				
				ObservableCollection<BaseModel> model = new ObservableCollection<BaseModel>();
				using (UserContext db = new UserContext())
				{
					if (x != null)
						foreach (var doc in db.BaseModels.Where(t=>t.Login==login))
						{
							if (doc.Name_KP!=null||doc.Name_SB != null)
							{
							if (x.Any(t => t.Contains(doc.Name_KP??"kek12")||t.Contains(doc.Name_SB??"kek12")))
								model.Add(doc);
							}
						}
					return model;
				}
			});
		}
		[HttpGet]
		public async Task<byte[]> DownDocAsync([FromUri] string path)
		{
			return await Task.Run(() =>
				{
					return File.ReadAllBytes(HostingEnvironment.ApplicationPhysicalPath + path);

				});}
		[HttpPost]
		public async Task<bool> UploadDocAsync([FromBody] UploadModel model)
		{
			return await Task.Run(() =>
			{
				try
				{
					BaseModel models = null;
					using (UserContext db = new UserContext())
					{
						models = db.BaseModels.FirstOrDefault(x => x.ID == model.ID || x.Login == model.login)??new BaseModel{Login = model.login, ID = 0};
						Directory.CreateDirectory(HostingEnvironment.ApplicationPhysicalPath +
						                          $"\\SaveDocs\\{model.login}");
						if (model.pathd != null)
						{
							byte[] user = model.pathd;
							if(!string.IsNullOrWhiteSpace(models.Name_KP))
								File.Delete(HostingEnvironment.ApplicationPhysicalPath +
								            $"\\SaveDocs\\{model.login}\\{models.Name_KP}");

							using (FileStream fs = File.Create(HostingEnvironment.ApplicationPhysicalPath +
							                                   $"\\SaveDocs\\{model.login}\\{model.name}.docx",
								(int) user.Length))
							{
								fs.Write(user, 0, user.Length);
							}
							FileInfo fi = new FileInfo(HostingEnvironment.ApplicationPhysicalPath +
							                           $"\\SaveDocs\\{model.login}\\{model.name}.docx");
							models.Name_KP = model.name + ".docx";
							models.Date_KP = fi.CreationTime.ToString();
						}

						if (model.pathe != null)
						{
							byte[] user = model.pathe;
							if (!string.IsNullOrWhiteSpace(models.Name_SB))
								File.Delete(HostingEnvironment.ApplicationPhysicalPath +
								            $"\\SaveDocs\\{model.login}\\{models.Name_SB}");
							using (FileStream fs = File.Create(HostingEnvironment.ApplicationPhysicalPath +
							                                   $"\\SaveDocs\\{model.login}\\{model.name}.xlsx",
								(int) user.Length))
							{
								fs.Write(user, 0, user.Length);
							}
							FileInfo fi = new FileInfo(HostingEnvironment.ApplicationPhysicalPath +
							                           $"\\SaveDocs\\{model.login}\\{model.name}.xlsx");
							models.Name_SB = model.name + ".xlsx";
							models.Date_SB = fi.CreationTime.ToString();
						}

						db.BaseModels.AddOrUpdate(models);
						db.SaveChanges();

					}

					return true;
				}
				catch (Exception e)
				{
					return false;
				}
			});
		}

	    
		[HttpGet]
		public async Task<ObservableCollection<Headers>> GetTableAsync([FromUri] string Diag, [FromUri] string Type, [FromUri] string Name)
		{
			return await Task.Run(() =>
			    {
			        ObservableCollection<Headers> headerCollection = new ObservableCollection<Headers>();
                    try
			        {
			            string _type;
			            switch (Type)
			            {
			                case "ИНТЕРАКТИВНЫЕ СТОЛЫ":
			                    _type = "1";
			                    break;
			                case "ИНТЕРАКТИВНЫЕ ПАНЕЛИ":
			                    _type = "2";
			                    break;
			                case "СЕНСОРНЫЕ КИОСКИ":
			                    _type = "3";
			                    break;
			                case "СЕНСОРНЫЕ ТЕРМИНАЛЫ":
			                    _type = "4";
			                    break;
			                default:
			                    _type = "0";
			                    break;
			            }
                        
                        using (UserContext db = new UserContext())
                        {
                            foreach (var x in db.DeviceModels.Where(t =>
                                t.Block.Contains(Name) && t.Type.Contains(_type) &&
                                (Diag == null || (t.Display.Contains(Diag)))))
                            {
                                if (headerCollection.Any(t => t.Head == x.Header))
                                {
                                    headerCollection.FirstOrDefault(t => t.Head == x.Header)?.Collection.Add(new Item
                                    {
                                        Desc = x.Description,
                                        Price = x.Price.ToString(),
                                        Property = x.Name
                                    });
                                }
                                else
                                {
                                    headerCollection.Add(new Headers
                                    {
                                        Head = x.Header,
                                        Collection = new ObservableCollection<Item>
                                    {
                                        new Item
                                        {
                                            Desc = x.Description,
                                            Price = x.Price.ToString(),
                                            Property = x.Name
                                        }
                                    }
                                    });
                                }
                            }
                        }
                        return headerCollection;
                    }
                    catch (Exception e)
                    {
                        headerCollection.Add(new Headers() { Head = e.Message, Collection = null });
                        return headerCollection;
                    }
			});
		}
		[HttpGet]
		public async Task<ObservableCollection<ImageModels>> GetModelsAsync([FromUri] string Type)
		{
			return await Task.Run(() =>
			{
				Dispatcher Dispatcher = Dispatcher.CurrentDispatcher;
				var derDirectories = Directory.GetDirectories(HostingEnvironment.ApplicationPhysicalPath + $@"\Models");
				var files = Directory.GetFiles(derDirectories.FirstOrDefault(x => x.Contains(Type)));
				if (files.Length==0)
				{
					return null;
				}
				foreach (var x in files.Where(t => t.Contains(".png")|| t.Contains(".jpg")))
				{
					Dispatcher.Invoke(() =>
					{
						FileInfo fileInfo = new FileInfo(x);
						Modelses.Add(new ImageModels
							{ Name = fileInfo.Name.Replace(".png","").Replace(".jpg",""), Path = "http://195.133.1.197:2019/Api/table/getimage/?nameImage=" + fileInfo.Name+"&type="+Type });

					});

				}
				return Modelses;
			});
		}
		[HttpGet]
		public HttpResponseMessage GetImage([FromUri] string nameImage, [FromUri] string type)
		{

			HttpResponseMessage response = new HttpResponseMessage();
			response.Content =
				new StreamContent(new FileStream(HostingEnvironment.ApplicationPhysicalPath + $"\\Models\\{type}\\" + nameImage,
					FileMode.Open)); // this file stream will be closed by lower layers of web api for you once the response is completed. 
			response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");

			return response;
		}

		[HttpGet]
		public async Task<string> GetDollar()
		{
			return await Task.Run(() =>
			{
				try
				{
					return File.Exists(HostingEnvironment.ApplicationPhysicalPath + "dollar.txt")?File.ReadAllText(HostingEnvironment.ApplicationPhysicalPath + "dollar.txt"): "file not found";
				}
				catch (Exception e)
				{
					return null;
				}
			});
		}

		[HttpGet]
		public async Task<bool> SetDollar(string dollar)
		{
			return await Task.Run(() =>
			{
				try
				{
					FileStream fs = File.Create(HostingEnvironment.ApplicationPhysicalPath + "dollar.txt");
					StreamWriter sw = new StreamWriter(fs);
					sw.Write(dollar);
					sw.Close();
					return true;
				}
				catch (Exception e)
				{
					return false;
				}
			});
		}

		[HttpGet]
		public async Task<BaseModel> GetID([FromUri]string adress, [FromUri]string login)
		{
			return await Task.Run(async () =>
			{
				try
				{
					BaseModel model = null;
					UserModel user = null;
				    if (string.IsNullOrWhiteSpace(adress))
				    {
				        using (UserContext db = new UserContext())
				        {
				            user = db.UserModels.FirstOrDefault(x => x.Login == login);
				            model = new BaseModel
				            {
				                Name_Manager = user.Name,
				                Surname_Manager = user.Surname,
				                Login = user.Login,
				                ID = 0,
				            };
				            db.BaseModels.AddOrUpdate(model);
				            db.SaveChanges();
				            model = db.BaseModels.FirstOrDefault(x =>
				                x.Name_Company == model.Name_Company && x.Name_Client == model.Name_Client &&
				                x.Post_Client == model.Post_Client && x.Login == model.Login);
				        }
				        return model;
                    }
					string trellocardid = Regex.Match(adress, "(?<=c/).*").Value;
					HttpClient _httpClient = new HttpClient();
					return await await _httpClient.GetAsync("https://api.trello.com/1/cards/"+trellocardid+$"?fields=desc&key={apikey}&token={apitoken}").ContinueWith(
							async r =>
							{
								if (r.IsFaulted) return null;
								string desc="";
								StreamReader dsg = new StreamReader(await r.Result.Content.ReadAsStreamAsync());
								desc = dsg.ReadToEnd();
								using (UserContext db = new UserContext())
								{
									user = db.UserModels.FirstOrDefault(x => x.Login == login);
									model = new BaseModel
									{
										Name_Company = Regex.Match(desc, "(?<=Заказчик: ).*?(?= \\n)").Value,
										Name_Client = Regex.Match(desc, "(?<=Ответственное лицо и должность Заказчика: ).*?(?= \\n)").Value,
										Post_Client = Regex.Match(desc, "(?<=Почта: ).*?(?= \\n)").Value,
										Name_Manager = user.Name,
										Surname_Manager = user.Surname,
										Login = user.Login,
										ID = 0,
									};
									db.BaseModels.AddOrUpdate(model);
									db.SaveChanges();
									model = db.BaseModels.FirstOrDefault(x =>
										x.Name_Company == model.Name_Company && x.Name_Client == model.Name_Client &&
										x.Post_Client == model.Post_Client && x.Login == model.Login);
								}
								return model;
							});
					
				}
				catch (Exception e)
				{
					return null;
				}
			});
		}

	}
}