﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using System.Windows.Media;
using System.Windows.Threading;
using AdminWeb.Models;
using Newtonsoft.Json;

namespace AdminWeb.Controllers
{
	public class UserController : ApiController
	{
		[HttpGet]
		public async Task<UserModel> GetUserAsync([FromUri] int ID, [FromUri]bool logout = false)
		{
			return await Task.Run(() =>
			{
				UserModel user = null;
				using (UserContext db = new UserContext())
				{
					user = db.UserModels.FirstOrDefault(u => u.ID == ID);
					if (logout)
					{
						user.Is_active = false;
						db.UserModels.AddOrUpdate(user);
						db.SaveChanges();
					}
					
				}

				return user;
			});
		}

		[HttpGet]
		public async Task<UserModel> LoginUserAsync([FromUri] string login, [FromUri] string password)
		{
			return await Task.Run(() =>
			{
				UserModel user = null;
				using (UserContext db = new UserContext())
				{
					user = db.UserModels.FirstOrDefault(u => u.Login == login && u.Password == password);
					if (user != null)
					{
						user.Is_active = true;
						db.UserModels.AddOrUpdate(user);
						db.SaveChanges();
						user = db.UserModels.FirstOrDefault(u => u.Login == login && u.Password == password);
					}

				}
				return user;
			});
		}

		[HttpGet]
		public HttpResponseMessage GetImage([FromUri] string nameImage)
		{

			HttpResponseMessage response = new HttpResponseMessage();
			response.Content =
				new StreamContent(new FileStream(HostingEnvironment.ApplicationPhysicalPath + "\\Images\\" + nameImage,
					FileMode.Open)); // this file stream will be closed by lower layers of web api for you once the response is completed. 
			response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");

			return response;
		}
	}
}
