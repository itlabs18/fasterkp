namespace AdminWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeviceModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DeviceModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                        Block = c.String(),
                        Display = c.String(),
                        Header = c.String(),
                        Name = c.String(),
                        Price = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.DeviceModels");
        }
    }
}
