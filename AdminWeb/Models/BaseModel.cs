﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminWeb.Models
{
	public class BaseModel
	{
		public int ID { get; set; }
		public string Name_KP { get; set; }
		public string Date_KP { get; set; }
		public string Name_SB { get; set; }
		public string Date_SB { get; set; }
		public string Login { get; set; }
		public string Name_Manager { get; set; }
		public string Surname_Manager { get; set; }
		public string Name_Company { get; set; }
		public string Name_Client { get; set; }
		public string Post_Client { get; set; }
	}
}