﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdminWeb.Models
{
	public class DeviceModel
	{
		public int ID { get; set; }
	    public string Type { get; set; }
        public string Block { get; set; }
        public string Display { get; set; }
        public string Header { get; set; }
        public string Name { get; set; }
		public string Price { get; set; }
        public string Description { get; set; }
        
		//public List <DescriptionModel> Description { get; set; }

	}
}