﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Windows.Media;
using Faster_Kp_Client.Annotations;

namespace AdminWeb.Models
{
	public class Headers : INotifyPropertyChanged
	{
		private string _head;
		public string Head
		{
			get => _head;
			set
			{
				_head = value;
				OnPropertyChanged();
			}
		}
		private ObservableCollection<Item> _collection;
		public ObservableCollection<Item> Collection
		{
			get => _collection ?? (_collection = new ObservableCollection<Item>());
			set
			{
				_collection = value;
				OnPropertyChanged();
			}
		}


		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}

	public class CollectHeads
	{
		private ObservableCollection<Headers> _headerses;

		public ObservableCollection<Headers> Headerses
		{
			get => _headerses ?? (_headerses = new ObservableCollection<Headers>());
			set => _headerses = value;
		}
	}
	public class Item : INotifyPropertyChanged
	{
		private string _property;
		public string Property
		{
			get => _property;
			set
			{
				_property = value;
				OnPropertyChanged();
			}
		}
		private string _price;
		public string Price
		{
			get => _price;
			set
			{
				_price = value;
				OnPropertyChanged();
			}
		}
	    private string _desc;
	    public string Desc
	    {
	        get => _desc;
	        set
	        {
	            _desc = value;
	            OnPropertyChanged();
	        }
	    }

        public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
	public class ImageModels : INotifyPropertyChanged
	{
		private string _path;
		public string Path
		{
			get => _path;
			set
			{
				_path = value;
				OnPropertyChanged();
			}
		}
		private string _name;
		public string Name
		{
			get => _name;
			set
			{
				_name = value;
				OnPropertyChanged();
			}
		}



		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}