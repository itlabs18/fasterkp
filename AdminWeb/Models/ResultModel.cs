﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminWeb.Models
{
	public class ResultModel
	{
		private string _text { get; set; }
		public string Text
		{
			get => _text;
			set => _text = value;
		}
	}
}