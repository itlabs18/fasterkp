﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminWeb.Models
{
    public class TypesModel
    {
        public bool IS { get; set; }
        public bool IP { get; set; }
        public bool SK { get; set; }
        public bool ST { get; set; }
    }
}