﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminWeb.Models
{
	public class UploadModel
	{
		public byte[] pathd { get; set; }
		public byte[] pathe { get; set; }
		public string login { get; set; }
		public string name { get; set; }
		public int ID { get; set; }
	}
}