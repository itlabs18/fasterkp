﻿using AdminWeb.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Faster_Kp_Client.KPPages;

namespace AdminWeb.Models
{
	public class UserContext:DbContext
	{
		public UserContext() :
			base("FastersKpDB")
		{ }
		public DbSet<UserModel> UserModels { get; set; }
        public DbSet<BaseModel> BaseModels { get; set; }
        public DbSet<DeviceModel> DeviceModels { get; set; }
    }
}