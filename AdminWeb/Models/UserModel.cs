﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdminWeb.Models
{
	public class LoginModel
	{
		[Required]
		public string Login { get; set; }

		[Required]
		[DataType(DataType.Password)]
		public string Password { get; set; }
	}
	public class RegisterModel
	{
		[Required]
		public string Name { get; set; }

		[Required]
		[DataType(DataType.Password)]
		public string Password { get; set; }

		[Required]
		public int ID { get; set; }

		[Required]
		public string Login { get; set; }

		[Required]
		public string Position { get; set; }

		[Required]
		public string Surname { get; set; }

		[Required]
		public string Telefon_number { get; set; }

		[Required]
		public string Image_name { get; set; }
		[Required]
		public bool Is_admin { get; set; }
		[Required]
		public bool Is_active { get; set; }
	}
	public class UserModel
	{
		public int ID { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }
		public string Name { get; set; }
		public string Position { get; set; }
		public string Surname { get; set; }
		public string Telefon_number { get; set; }
		public string Image_name { get; set; }
		public bool Is_admin { get; set; }
		public bool Is_active { get; set; }
	}	
}