﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace AdminWeb.Models
{
	public static class UsersList
	{
		private static ObservableCollection<UserModel> _users;

		public static ObservableCollection<UserModel> Users
		{
			get => _users ?? new ObservableCollection<UserModel>();
			set => _users = value;
		}
	}
}