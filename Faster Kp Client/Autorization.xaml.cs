﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Faster_Kp_Client.Models;
using Faster_Kp_Client.Utilities;

namespace Faster_Kp_Client
{
    /// <summary>
    /// Логика взаимодействия для Autorization.xaml
    /// </summary>
    public partial class Autorization : Page
    {
        public Autorization(MainWindow mw)
        {
            InitializeComponent();
	        main = mw;
        }

	    private MainWindow main;
		//private ICommand _navigateTo;
		//public ICommand NavigateTo => _navigateTo ?? (_navigateTo = new Command(() =>
		//{
		// NavigationService.Navigate(new TypeOfDevise(main));
		//}));

		private ICommand _navigateTo;
		public ICommand NavigateTo => _navigateTo ?? (_navigateTo = new Command(async a =>
			                              {
			UserAuthModel UM = a as UserAuthModel;
			if (!string.IsNullOrWhiteSpace(UM.Email) && !string.IsNullOrWhiteSpace(UM.Password))
			{
				main.Preloader.Visibility = Visibility.Visible;
				Enter.IsEnabled = false;
												  UserModel model = await WebApi.Login(UM.Email, UM.Password);
				//model = new UserModel
				//{
			 //    Login = UM.Email,
				// Password = UM.Password,
				// ID = 4,
				// Image_name = @"C:\Users\Николай\Pictures\Camera Roll\WIN_20181004_09_28_15_Pro.jpg",
				// Is_active = true
				//};
				if (model != null)
				{
					UserSingleton.GetInstance(model, main);
					main.Account.Name = model.Name;
					main.Account.Surname = model.Surname;
					main.Account.Position = model.Position;
					main.Account.Icon = "http://195.133.1.197:2019/Api/user/getimage/?nameImage=" + model.Image_name;
					Task.Run(async () => { await main.Account.CheckTask(main, model.Is_active); });
					main.Preloader.Visibility = Visibility.Collapsed;
					Enter.IsEnabled = true;
					using (StreamWriter sw = new StreamWriter(File.Create("session.dat"), Encoding.UTF8))
					{
						sw.WriteLine(model.ID.ToString(), Encoding.UTF8);
						sw.WriteLine(model.Login, Encoding.UTF8);
						sw.WriteLine(model.Password, Encoding.UTF8);
					}
					NavigationService.Navigate(new TypeOfDevise(main));
				}
				else
				{
					Enter.IsEnabled = true;
					main.Preloader.Visibility = Visibility.Collapsed;
					UM.ErrorEmail = "Неверный логин или пароль";
				}
			}
			else
			{
				if (string.IsNullOrWhiteSpace(UM.Email))
					UM.ErrorEmail = "Введите электронный адрес";
				
				if (string.IsNullOrWhiteSpace(UM.Password))
					UM.ErrorPassword = "Введите пароль";
			}
		}
		));
		private ICommand _backMain;
	    public ICommand BackMain => _backMain ?? (_backMain = new Command(() =>
	    {
			NavigationService.Navigate(null);
	    }));
    }
	public class PasswordBoxMonitor : DependencyObject
	{
		public static bool GetIsMonitoring(DependencyObject obj)
		{
			return (bool)obj.GetValue(IsMonitoringProperty);
		}

		public static void SetIsMonitoring(DependencyObject obj, bool value)
		{
			obj.SetValue(IsMonitoringProperty, value);
		}

		public static readonly DependencyProperty IsMonitoringProperty =
			DependencyProperty.RegisterAttached("IsMonitoring", typeof(bool), typeof(PasswordBoxMonitor), new UIPropertyMetadata(false, OnIsMonitoringChanged));

		public static string GetPassword(DependencyObject obj)
		{
			return (string)obj.GetValue(PasswordProperty);
		}

		public static void SetPassword(DependencyObject obj, string value)
		{
			obj.SetValue(PasswordProperty, value);
		}

		public static readonly DependencyProperty PasswordProperty =
			DependencyProperty.RegisterAttached("Password", typeof(string), typeof(PasswordBoxMonitor), new UIPropertyMetadata(default(string)));

		public static int GetPasswordLength(DependencyObject obj)
		{
			return (int)obj.GetValue(PasswordLengthProperty);
		}

		public static void SetPasswordLength(DependencyObject obj, int value)
		{
			obj.SetValue(PasswordLengthProperty, value);
		}

		public static readonly DependencyProperty PasswordLengthProperty =
			DependencyProperty.RegisterAttached("PasswordLength", typeof(int), typeof(PasswordBoxMonitor), new UIPropertyMetadata(0));

		private static void OnIsMonitoringChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var pb = d as PasswordBox;
			if (pb == null)
			{
				return;
			}
			if ((bool)e.NewValue)
			{
				pb.PasswordChanged += PasswordChanged;
				pb.PasswordChanged += PasswordTextChanged;
			}
			else
			{
				pb.PasswordChanged -= PasswordChanged;
				pb.PasswordChanged -= PasswordTextChanged;
			}
		}

		static void PasswordChanged(object sender, RoutedEventArgs e)
		{
			var pb = sender as PasswordBox;
			if (pb == null)
			{
				return;
			}
			SetPasswordLength(pb, pb.Password.Length);
		}
		static void PasswordTextChanged(object sender, RoutedEventArgs e)
		{
			var pb = sender as PasswordBox;
			if (pb == null)
			{
				return;
			}
			SetPassword(pb, pb.Password);
		}
	}
}
