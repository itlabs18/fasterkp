﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Mime;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using Faster_Kp_Client.Annotations;
using Faster_Kp_Client.controls;
using Faster_Kp_Client.KPPages;
using Faster_Kp_Client.Models;
using Faster_Kp_Client.Utilities;
using Newtonsoft.Json;
using Word = Microsoft.Office.Interop.Word;
using Excel = Microsoft.Office.Interop.Excel;

namespace Faster_Kp_Client
{
	/// <summary>
	/// Логика взаимодействия для CostPrice.xaml
	/// </summary>
	public partial class CostPrice : Page, INotifyPropertyChanged
	{
		
		public CostPrice(Page p, MainWindow mw)
		{
			InitializeComponent();
            main = mw;
            page = p;
            ReadyDevices = new ReadyDevice(this);

            NameTabl = new NamesTables(this);
            foreach (var x in DeviceSingle.getInstance().Device)
            {
                if (x.Value)
                {
                    switch (x.Key)
                    {
                        case "ВИРТУАЛЬНЫЙ ПРОМОУТЕР":
                            Item.Add(new ItemProperty() { Text = x.Key, Page = new ObservableCollection<Page> { new OtherType(this,x.Key) } });
                            break;
                        case "ГОЛОГРАФИЧЕСКИЙ ВЕНТИЛЯТОР":
                            Item.Add(new ItemProperty() { Text = x.Key, Page = new ObservableCollection<Page> { new OtherType(this,x.Key) } });
                            break;
                        case "ГОЛОГРАФИЧЕСКАЯ ПИРАМИДА":
                            Item.Add(new ItemProperty() { Text = x.Key, Page = new ObservableCollection<Page> { new OtherType(this,x.Key) } });
                            break;
                        case "ГОЛОГРАФИЧЕСКИЙ КУБ":
                            Item.Add(new ItemProperty() { Text = x.Key, Page = new ObservableCollection<Page> { new OtherType(this,x.Key) } });
                            break;
                        default:
                            Item.Add(new ItemProperty() { Text = x.Key, Page = new ObservableCollection<Page> { new DescriptionDevice(this,x.Key) } });
                            break;
                    }
                }
            }


        }
		public ReadyDevice ReadyDevices;
		public ReadyDevice ReadyDevicesClone;
		public MainWindow main;
		public Page page;
		private BaseModel model;
		public object PasteTable;

	

		private ObservableCollection<ItemProperty> _item; 
		public ObservableCollection<ItemProperty> Item
		{
			get => _item ?? (_item = new ObservableCollection<ItemProperty>());
			set
			{
				_item = value;
				OnPropertyChanged();
			}
		}
		private object PathToTemplate = Directory.GetCurrentDirectory()+ @"\base\template.docx";
		private ObservableCollection<KPModel> _collection;
		public ObservableCollection<KPModel> Collection
		{
			get => _collection ?? (_collection = new ObservableCollection<KPModel>());
			set { _collection = value; }
		}
		private ObservableCollection<KPOther> _collectionOther;
		public ObservableCollection<KPOther> CollectionOther
		{
			get => _collectionOther ?? (_collectionOther = new ObservableCollection<KPOther>());
			set { _collectionOther = value; }
		}

		public NamesTables NameTabl;

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			
		}

		#region Комманды

		private ICommand _radioCommand;
		public ICommand RadioCommand => _radioCommand ?? (_radioCommand = new Command(p =>
			                                {
				                                if (Description.Content is ReadyDevice)
					                                Product.IsChecked=true;
												ItemProperty ip = (ItemProperty) p;
				                                Description.Navigate(ip.Page.FirstOrDefault());
			                                }
		                                ));
		private ICommand _close;
		public ICommand Close => _close ?? (_close = new Command(() => { ErrorCount.Visibility = Visibility.Collapsed; }));

		private ICommand _backMain;
		public ICommand BackMain => _backMain ?? (_backMain = new Command(() => { main.Pages.Navigate(page); }));
		private ICommand _goKP;
		public ICommand GoKP => _goKP ?? (_goKP = new Command(async () =>
		{
				Preloader.Visibility = Visibility.Visible;
					Collection.Clear();
				CollectionOther.Clear();
					foreach (var xProperty in Item)
					{

						if (xProperty.Page.All(t=> t is DescriptionDevice))
						{
						    foreach (DescriptionDevice ds in xProperty.Page)
						        Collection.Add(new KPModel()
						        {
						            Header = xProperty.Header,
						            Name = xProperty.Text + "\r\n" + (ds.Model.Content ?? "").ToString(),
						            Page = ds,
						            Count = ds.LeftRight.Textprop.ToString(),
						            Price = (Convert.ToInt32(ds.Cost.Content) /
						                     Convert.ToInt32(ds.LeftRight.Textprop)).ToString() ?? "0",
						            Cost = (ds.Cost.Content ?? "0").ToString(),
						            Body = ds.Body.Placehold ?? "",
						            Computer = ds.Computer.Placehold ?? "",
                                    ComputerParts = ds.comp,
						            Display = ds.Display.Placehold ?? "",
						            Extradevice = ds.ExtraDevice.Placehold ?? "",
						            Typesensor = ds.Typesensor.Placehold ?? "",
						            TypePO = ds.PO.Placehold ?? "",
						            Path = (ds.Model.Tag ?? "").ToString(),
                                    Type = xProperty.Text
						        });
						}
						if (xProperty.Page.All(t => t is OtherType))
						{
						    foreach (OtherType other in xProperty.Page)
                            CollectionOther.Add(new KPOther()
							{
								Header = xProperty.Header,
								Name = xProperty.Text + "\r\n" + (other.Model.Content ?? "").ToString(),
								Page = other,
								Count = other.LeftRight.Textprop.ToString(),
								Price = (Convert.ToInt32(other.Cost.Content) /
								         Convert.ToInt32(other.LeftRight.Textprop)).ToString() ?? "0",
								Cost = (other.Cost.Content ?? "0").ToString(),
								Path = (other.Model.Tag ?? "").ToString(),
								CollectionOthers = other.Collection,
                                Type = xProperty.Text
							});
						}
				}
			model = await WebApi.GetBaseModel(NameTabl.Text, UserSingleton.instance.Model.Login);
		    if (!Sebes.IsChecked)
		    {
		        await Task.Run(() => { WordSingleton.getInstance("WORD VIEWER"); });
            }
		    await Task.Run(() => { ExcelSingleton.getInstance("EXCEL VIEWER"); });
            ExcelSingleton.instance.Application.Visible = false;
            if ((Sebes.IsChecked || await CreateKP(WordSingleton.instance.Application)) && await CreateSB(ExcelSingleton.instance.Application))
					{
						
						Description.Navigate(new Preview(model.ID,this,null,null));
					}
            else
            {
                KillWordandExcel.Kill();
                TextError.Text = "Непредвиденная ошибка - попробуйте ещё раз";
                ErrorCount.Visibility = Visibility.Visible;
            }
		  


            Preloader.Visibility = Visibility.Collapsed;
				}
			
		));

		private ICommand _ok;
		public ICommand OK => _ok ?? (_ok = new Command(() =>
		{
			if (Item.Where(x => x.Page.All(t=>t is DescriptionDevice))
				.All(p =>p.Page.All(t=> Sebes.IsChecked||!string.IsNullOrWhiteSpace((string)((DescriptionDevice)t).ContinueSolution.Tag))))
			{
			if (Item.Where(x => x.Page.All(t => t is OtherType))
			    .All(p => p.Page.All(t => Sebes.IsChecked || !string.IsNullOrWhiteSpace((string)((OtherType)t).ContinueSolution.Tag))))
                {
				if (Item.Where(x => x.Page.All(t => t is OtherType))
					.All(p => p.Page.All(t => (bool)((OtherType)t).Collection.All(x => !string.IsNullOrWhiteSpace(x.Name) &
					                                                          !string.IsNullOrWhiteSpace(x.Price) &
					                                                          !string.IsNullOrWhiteSpace(x.Prop)))))
				{
					if (Item.Where(x => x.Page.All(t => t is DescriptionDevice))
                            .All(p => Sebes.IsChecked || p.Page.All(t=> (bool) ((DescriptionDevice)t).CustomDesign.IsChecked) ||
                                      p.Page.All(t => (bool)((DescriptionDevice)t).ReadySolution.IsChecked)
					    && Item.Where(x => x.Page.All(t => t is OtherType))
                                          .All(x => Sebes.IsChecked || x.Page.All(t => (bool)((OtherType)t).CustomDesign.IsChecked) ||
                                                    x.Page.All(t => (bool)((OtherType)t).ReadySolution.IsChecked))))
                    {
						if (Item.Where(x => x.Page.All(t => t is DescriptionDevice))
							    .All(x => x.Page.All(t => Convert.ToInt32(((DescriptionDevice)t).LeftRight.Textprop) >= 1)) &&
						    Item.Where(x => x.Page.All(t => t is OtherType))
							    .All(x => x.Page.All(t => Convert.ToInt32(((OtherType)t).LeftRight.Textprop) >= 1)))
						{
							NavigationService.Navigate(NameTabl);
						}
						else
						{
							ItemProperty pg =
								Item.Where(x => x.Page.All(t => t is DescriptionDevice)).FirstOrDefault(x =>
								    x.Page.All(t => Convert.ToInt32(((DescriptionDevice)t).LeftRight.Textprop) < 1)) ??
								Item.Where(x => x.Page.All(t => t is OtherType)).FirstOrDefault(x =>
								    x.Page.All(t => Convert.ToInt32(((OtherType)t).LeftRight.Textprop) < 1));
                            if(pg.Page.All(t=> t is DescriptionDevice))
							Description.Navigate(pg.Page.FirstOrDefault(t => Convert.ToInt32(((DescriptionDevice)t).LeftRight.Textprop) < 1));
						    if (pg.Page.All(t => t is OtherType))
						        Description.Navigate(pg.Page.FirstOrDefault(t => Convert.ToInt32(((OtherType)t).LeftRight.Textprop) < 1));
                            pg.Checked = true;

							TextError.Text = "Количество оборудования не может быть 0";
							ErrorCount.Visibility = Visibility.Visible;
						}

					}
					else
					{
						ItemProperty pg =
							Item.Where(x => x.Page.All(t => t is DescriptionDevice))
								.FirstOrDefault(p => !p.Page.All(t => (bool)((DescriptionDevice)t).CustomDesign.IsChecked || (bool)((DescriptionDevice)t).ReadySolution.IsChecked))
								                      
							?? Item.Where(x => x.Page.All(t => t is OtherType))
								.FirstOrDefault(p => !p.Page.All(t => (bool)((OtherType)t).CustomDesign.IsChecked || (bool)((OtherType)t).ReadySolution.IsChecked));
					    if (pg.Page.All(t => t is DescriptionDevice))
					        Description.Navigate(pg.Page.FirstOrDefault(t => !(bool)((DescriptionDevice)t).CustomDesign.IsChecked || !(bool)((DescriptionDevice)t).ReadySolution.IsChecked));
					    if (pg.Page.All(t => t is OtherType))
					        Description.Navigate(pg.Page.FirstOrDefault(t => !(bool)((OtherType)t).CustomDesign.IsChecked || !(bool)((OtherType)t).ReadySolution.IsChecked));
                            pg.Checked = true;
						TextError.Text = "Необходимо выбрать модель";
						ErrorCount.Visibility = Visibility.Visible;
					}
				}
				else
				{
				    ItemProperty pg = Item.Where(x => x.Page.All(t => t is OtherType))
				        .FirstOrDefault(p => p.Page.All(t => ((OtherType) t).Collection.Any(x =>
				            string.IsNullOrWhiteSpace(x.Name) ||
				            string.IsNullOrWhiteSpace(x.Price) ||
				            string.IsNullOrWhiteSpace(x.Prop))));
					Description.Navigate(pg.Page.First(t=> ((OtherType)t).Collection.Any(x =>
					    string.IsNullOrWhiteSpace(x.Name) ||
					    string.IsNullOrWhiteSpace(x.Price) ||
					    string.IsNullOrWhiteSpace(x.Prop))));
					pg.Checked = true;
					TextError.Text = "Заполните все позиции";
					ErrorCount.Visibility = Visibility.Visible;
				}
			}
			else
			{
				ItemProperty pg = Item.Where(x => x.Page.All(t => t is OtherType))
					.FirstOrDefault(p => p.Page.Any(t => string.IsNullOrWhiteSpace((string)((OtherType)t).ContinueSolution.Tag)));
				Description.Navigate(pg.Page.FirstOrDefault(t=> string.IsNullOrWhiteSpace((string)((OtherType)t).ContinueSolution.Tag)));
				pg.Checked = true;
				TextError.Text = "Выберите фото реализованного продукта";
				ErrorCount.Visibility = Visibility.Visible;
			}
			}
			else
			{
				ItemProperty pg = Item.Where(x => x.Page.All(t => t is DescriptionDevice))
					.FirstOrDefault(p => p.Page.Any(t => string.IsNullOrWhiteSpace((string)((DescriptionDevice)t).ContinueSolution.Tag)));
				Description.Navigate(pg.Page.First(t => string.IsNullOrWhiteSpace((string)((DescriptionDevice)t).ContinueSolution.Tag)));
				pg.Checked = true;
				TextError.Text = "Выберите фото реализованного продукта";
				ErrorCount.Visibility = Visibility.Visible;
			}
		}));
		private ICommand _ready;
		public ICommand Ready => _ready ?? (_ready = new Command(() => { Description.Navigate(ReadyDevices); }));
		private ICommand _products;
		public ICommand Products => _products ?? (_products = new Command(() =>
		{
			if(Description.Content is ReadyDevice)
			Description.Navigate(null);
		}));

	    private ICommand _rightButton;
	    public ICommand RightButton => _rightButton ?? (_rightButton = new Command(() =>
	    {
	        if (Description.Content is DescriptionDevice)
	        {
	            if (Item.First(t => t.Page.Any(x => x.Equals((DescriptionDevice) Description.Content))).Page.Count >
	                1)
	            {
	                ItemProperty it = Item.First(t =>
	                    t.Page.Any(x => x.Equals((DescriptionDevice) Description.Content)));
	                DescriptionDevice dd;
	                int last = it.Page.IndexOf(it.Page.Last());

	                switch (it.Page.IndexOf((DescriptionDevice) Description.Content))
	                {
	                    case 0:
	                        dd = (DescriptionDevice) it.Page[
	                            it.Page.IndexOf((DescriptionDevice) Description.Content) + 1];
	                        break;
	                    default:
	                        dd = it.Page.IndexOf(it.Page.Last()) ==
	                             it.Page.IndexOf((DescriptionDevice) Description.Content)
	                            ? (DescriptionDevice) it.Page[0]
	                            : (DescriptionDevice) it.Page[
	                                it.Page.IndexOf((DescriptionDevice) Description.Content) + 1];
	                        break;
	                }
	                Description.Navigate(dd);
	            }
	        }
	        if (Description.Content is OtherType)
	        {
	            if (Item.First(t => t.Page.Any(x => x.Equals((OtherType)Description.Content))).Page.Count > 1)
	            {
	                ItemProperty it = Item.First(t => t.Page.Any(x => x.Equals((OtherType)Description.Content)));
	                OtherType dd;
	                switch (it.Page.IndexOf((OtherType)Description.Content))
	                {
	                    case 0:
	                        dd = (OtherType)it.Page[
	                            it.Page.IndexOf((OtherType)Description.Content) + 1];
	                        break;
	                    default:
	                        dd = it.Page.IndexOf(it.Page.Last()) ==
	                             it.Page.IndexOf((OtherType)Description.Content)
	                            ? (OtherType)it.Page[0]
	                            : (OtherType)it.Page[
	                                it.Page.IndexOf((OtherType)Description.Content) + 1];
	                        break;
                    }
	                Description.Navigate(dd);
	            }
	        }
        }));
	    private ICommand _leftButton;
	    public ICommand LeftButton => _leftButton ?? (_leftButton = new Command(() =>
	    {
	        if (Description.Content is DescriptionDevice)
	        {
	            if (Item.First(t => t.Page.Any(x => x.Equals((DescriptionDevice)Description.Content))).Page.Count >
	                1)
	            {
	                ItemProperty it = Item.First(t =>
	                    t.Page.Any(x => x.Equals((DescriptionDevice)Description.Content)));
	                DescriptionDevice dd;
                    switch (it.Page.IndexOf((DescriptionDevice)Description.Content))
	                {
                        case 0:
                            dd = (DescriptionDevice) it.Page[it.Page.IndexOf(it.Page.Last())];
                            break;
	                    default:
	                        dd = (DescriptionDevice)it.Page[it.Page.IndexOf((DescriptionDevice)Description.Content) - 1];
	                        break;
                    }
	                Description.Navigate(dd);
	            }
	        }
	        if (Description.Content is OtherType)
	        {
	            if (Item.First(t => t.Page.Any(x => x.Equals((OtherType)Description.Content))).Page.Count > 1)
	            {
	                ItemProperty it = Item.First(t => t.Page.Any(x => x.Equals((OtherType)Description.Content)));
	                OtherType dd;
	                switch (it.Page.IndexOf((OtherType)Description.Content))
	                {
	                    case 0:
	                        dd = (OtherType)it.Page[it.Page.IndexOf(it.Page.Last())];
	                        break;
	                    default:
	                        dd = (OtherType)it.Page[it.Page.IndexOf((OtherType)Description.Content) - 1];
	                        break;
	                }
	                Description.Navigate(dd);
	            }
	        }
        }));

        private ICommand _deleteDevice;
	    public ICommand DeleteDevice => _deleteDevice ?? (_deleteDevice = new Command(() =>
	    {
	        
                if (Description.Content is DescriptionDevice)
                {
                    if (Item.First(t => t.Page.Any(x => x.Equals((DescriptionDevice) Description.Content))).Page.Count >
                        1)
                    {
                        ItemProperty it = Item.First(t =>
                            t.Page.Any(x => x.Equals((DescriptionDevice) Description.Content)));
                        DescriptionDevice dd = it.Page.IndexOf((DescriptionDevice) Description.Content) == 0
                            ? (DescriptionDevice) it.Page[it.Page.IndexOf((DescriptionDevice) Description.Content) + 1]
                            : (DescriptionDevice) it.Page[it.Page.IndexOf((DescriptionDevice) Description.Content) - 1];
                        it.Page.Remove((DescriptionDevice) Description.Content);
                        Description.Navigate(dd);
                    }
                }
	            if (Description.Content is OtherType)
	            {
	                if (Item.First(t => t.Page.Any(x => x.Equals((OtherType)Description.Content))).Page.Count > 1)
	                {
                    ItemProperty it = Item.First(t => t.Page.Any(x => x.Equals((OtherType)Description.Content)));
	                OtherType dd = it.Page.IndexOf((OtherType)Description.Content) == 0 ? (OtherType)it.Page[it.Page.IndexOf((OtherType)Description.Content) + 1] :
	                    (OtherType)it.Page[it.Page.IndexOf((OtherType)Description.Content) - 1];
                    it.Page.Remove((OtherType)Description.Content);
	                Description.Navigate(dd);
	                }
	            }
        }));

	    private ICommand _typeDevice;
	    public ICommand TypeDevice => _typeDevice ?? (_typeDevice = new Command(() =>
	    {
	        if (Description.Content is DescriptionDevice)
	        {
	           ItemProperty it =  Item.First(t => t.Page.Any(x => x.Equals((DescriptionDevice)Description.Content)));
	            DescriptionDevice dd = new DescriptionDevice(this,it.Text);
               it.Page.Add(dd);
	            Description.Navigate(dd);
	        }
	        if (Description.Content is OtherType)
	        {
	            ItemProperty it = Item.First(t => t.Page.Any(x => x.Equals((OtherType)Description.Content)));
	            OtherType dd = new OtherType(this,it.Text);
                it.Page.Add(dd);
	            Description.Navigate(dd);
            }
        }));
		#endregion

		Word.Document _document;
		private void SetComputer(Word.Table _workTable, List<string> t, int rowpaste)
		{
		    if (t[6].Replace("Кулер ЦПУ", "") != "")
		    {
		        _workTable.Rows.Add(_workTable.Cell(rowpaste, 3));
		        _workTable.Cell(rowpaste, 2).Range.Text = "Кулер ЦПУ";
		        _workTable.Cell(rowpaste, 3).Range.Text = t[5].Replace("Кулер ЦПУ", "");
		    }
            if (t[5].Replace("Блок питания", "") != "")
			{
				_workTable.Rows.Add(_workTable.Cell(rowpaste, 3));
				_workTable.Cell(rowpaste, 2).Range.Text = "Блок питания";
				_workTable.Cell(rowpaste, 3).Range.Text = t[5].Replace("Блок питания", "");
			}
			if (t[4].Replace("Материнская плата", "") != "")
			{
				_workTable.Rows.Add(_workTable.Cell(rowpaste, 3));
				_workTable.Cell(rowpaste, 2).Range.Text = "Материнская плата";
				_workTable.Cell(rowpaste, 3).Range.Text = t[4].Replace("Материнская плата", "");
			}
			if (t[3].Replace("Видеокарты", "") != "")
			{
				_workTable.Rows.Add(_workTable.Cell(rowpaste, 3));
				_workTable.Cell(rowpaste, 2).Range.Text = "Видеокарты";
				_workTable.Cell(rowpaste, 3).Range.Text = t[3].Replace("Видеокарты", "");
			}
			if (t[2].Replace("Накопители", "") != "")
			{
				_workTable.Rows.Add(_workTable.Cell(rowpaste, 3));
				_workTable.Cell(rowpaste, 2).Range.Text = "Накопители";
				_workTable.Cell(rowpaste, 3).Range.Text = t[2].Replace("Накопители", "").Replace(",", "\r\n ");
			}
			if (t[1].Replace("ОЗУ", "") != "")
			{
				_workTable.Rows.Add(_workTable.Cell(rowpaste, 3));
				_workTable.Cell(rowpaste, 2).Range.Text = "ОЗУ";
				_workTable.Cell(rowpaste, 3).Range.Text = t[1].Replace("ОЗУ", "").Replace(",", "\r\n ");
			}
			if (t[0].Replace("Процессоры", "") != "")
			{
				_workTable.Rows.Add(_workTable.Cell(rowpaste, 3));
				_workTable.Cell(rowpaste, 2).Range.Text = "Процессоры";
				_workTable.Cell(rowpaste, 3).Range.Text = t[0].Replace("Процессоры", "");
			}
		}
        /// <summary>
        /// Создать КП
        /// </summary>
        /// <param name="_word">ворд</param>
        /// <returns></returns>
        private async Task<bool> CreateKP(Word.Application _word)
		{
			return await Task.Run(async() => {
				try
				{
					Word.Range _range;
					Dispatcher.Invoke(() => { _document = _word.Documents.Open(PathToTemplate); });
					Word.Table _table = _document.Tables[1];
					Dispatcher.Invoke(() => { _table.Cell(1, 1).Range.Text ="Исх. " + model.ID + " от " + NameTabl.DateText; });
					_table = _document.Tables[3];
					Dispatcher.Invoke(() =>
					{
					    _table.Cell(1, 1).Range.Text = UserSingleton.instance.Model.Surname + " " + UserSingleton.instance.Model.Name;
					    _table.Cell(2, 1).Range.Text = UserSingleton.instance.Model.Position;
					    _table.Cell(4, 1).Range.Text = UserSingleton.instance.Model.Telefon_number;
					    _table.Cell(6, 1).Range.Text = UserSingleton.instance.Model.Login;
					});
					_table = _document.Tables[2];
					Word.Table _workTable;
					Dispatcher.Invoke(() => { Clipboard.Clear(); });
					if (PasteTable != null)
					{
						_range = _document.Range(_table.Range.End + 2, _table.Range.End + 2);
						Dispatcher.Invoke(() => { Clipboard.SetData("Rich Text Format", PasteTable); });
						_range.Paste();
                        _range.InsertBreak(Word.WdBreakType.wdPageBreak);
					}

				    foreach (KPModel xmodel in Collection.GroupBy(t => t.Type).Select(t => t.FirstOrDefault()))
				    {
				        _range = _document.Range(_table.Range.End, _table.Range.End);
				        _document.Range(_table.Range.Start - 1, _table.Range.End + 1).Copy();
				        _range.Paste();
				        _workTable = _range.Tables[1];
				        _document.Range(_workTable.Range.Start, _workTable.Range.Start).InsertBreak(Word.WdBreakType.wdPageBreak);

                        //_workTable.Cell(1, 1).Range.Text = xmodel.Header;

                        foreach (KPModel kpModel in Collection.Where(t=>t.Type.Contains(xmodel.Type)))
				        {
				           
				            int row = 2;
				            if (kpModel != xmodel)
				            {
				                _document.Range(_table.Range.Start, _table.Range.End).Select();
				                _word.Selection.Copy();
                                _document.Range(_workTable.Range.End+2,_workTable.Range.End+2).Paste();
				                _workTable = _document.Range(_workTable.Range.End + 2, _document.Range().End).Tables[1];
				                _document.Range(_workTable.Range.Start, _workTable.Range.Start).InsertBreak(Word.WdBreakType.wdPageBreak);
                            }
                            Dispatcher.Invoke(() => { Clipboard.Clear(); });
				            List<string> dictionary = new List<string>();
				            dictionary.Add(kpModel.Computer.Split(';').Where(x => x.Contains("Процессоры"))
				                               .ElementAtOrDefault(0) ?? "Процессоры");
				            dictionary.Add(kpModel.Computer.Split(';').Where(x => x.Contains("ОЗУ"))
				                               .ElementAtOrDefault(0) ?? "ОЗУ");
				            dictionary.Add(kpModel.Computer.Split(';').Where(x => x.Contains("Накопители"))
				                               .ElementAtOrDefault(0) ?? "Накопители");
				            dictionary.Add(kpModel.Computer.Split(';').Where(x => x.Contains("Видеокарты"))
				                               .ElementAtOrDefault(0) ?? "Видеокарты");
				            dictionary.Add(kpModel.Computer.Split(';').Where(x => x.Contains("Материнская плата"))
				                               .ElementAtOrDefault(0) ?? "Материнская плата");
				            dictionary.Add(kpModel.Computer.Split(';').Where(x => x.Contains("Блок питания"))
				                               .ElementAtOrDefault(0) ?? "Блок питания");
				            dictionary.Add(kpModel.Computer.Split(';').Where(x => x.Contains("Кулер ЦПУ"))
				                               .ElementAtOrDefault(0) ?? "Кулер ЦПУ");

                            _workTable.Cell(row, 1).Range.Text = kpModel.Name;
				            _workTable.Cell(row, 3).Range.Text = kpModel.Body;
				            _workTable.Cell(row+2, 3).Range.Text = kpModel.Display;
				            _workTable.Cell(row + 3, 3).Range.Text = kpModel.Typesensor;
				            await Dispatcher.Invoke(async () =>
				            {
				                HttpClient _httpClient = new HttpClient();
				                await await _httpClient.GetAsync(kpModel.Path)
				                    .ContinueWith(
				                        async r =>
				                        {
				                            //if (r.IsFaulted) return null;
				                            //byte[] user = JsonConvert.DeserializeObject<byte[]>(await r.Result.Content.ReadAsStringAsync());
				                            byte[] str = await r.Result.Content.ReadAsByteArrayAsync();

				                            //StreamReader sr = new StreamReader(str);
				                            //StreamWriter sw = new StreamWriter(Directory.GetCurrentDirectory() + @"\1.png");
				                            //while (!sr.EndOfStream)
				                            //{
				                            //	sw.Write(sr.ReadLine());
				                            //}
				                            //sw.Close();
				                            FileStream fs = File.Create(Directory.GetCurrentDirectory() + @"\1.png",
				                                (int) str.Length);
				                            fs.Write(str, 0, str.Length);
				                            fs.Dispose();
				                        });

				                string pathes = Directory.GetCurrentDirectory() + @"\1.png";
				                Word.InlineShape shape = _workTable.Cell(row + 10, 1).Range.InlineShapes
				                    .AddPicture(pathes, LinkToFile: false, SaveWithDocument: true);
				                float size = shape.Height / _workTable.Cell(row + 10, 1).Height;
				                shape.Height = _workTable.Cell(row + 10, 1).Height;
				                shape.Width = shape.Width / size;
				                File.Delete(Directory.GetCurrentDirectory() + @"\1.png");
				                //Word.InlineShape shape1 = _workTable.Cell(row + 9, 1).Range.InlineShapes
				                //    .AddPicture((string) kpModel.Page.ContinueSolution.Tag, LinkToFile: false,
				                //        SaveWithDocument: true);
				                //size = shape1.Height / _workTable.Cell(row + 9, 1).Height;
				                //shape1.Height = _workTable.Cell(row + 9, 1).Height;
				                //shape1.Width = shape1.Width / size;
				            });
				            Dispatcher.Invoke(() => { SetComputer(_workTable, dictionary, row+3); });
				            if (!kpModel.Extradevice.Contains("Выберите "))
				            {

				                _workTable.Rows.Add(_workTable.Cell(row + 5, 3));
				                _workTable.Cell(row +5, 2).Range.Text = "Доп. Оборудование";
				                _workTable.Cell(row +5, 3).Range.Text = kpModel.Extradevice.Replace(",", "\r\n ");
				            }

				            if (!kpModel.TypePO.Contains("Выберите "))
				            {
				                _workTable.Rows.Add(_workTable.Cell(row + 3, 3));
				                _workTable.Cell(row+ 3, 2).Range.Text = "Программное обеспечение";
				                _workTable.Cell(row +3, 3).Range.Text = kpModel.TypePO;
				            }

				            _workTable.Cell(row, 4).Range.Text = kpModel.Count;
				            _workTable.Cell(row, 5).Range.Text = Convert.ToInt32(kpModel.Price).ToString("N3", CultureInfo.GetCultureInfo("ru-RU"))

                                 + "\r\n(" + PriceToText(kpModel.Price) + ")";
				            _workTable.Cell(row, 6).Range.Text = Convert.ToInt32(kpModel.Cost).ToString("N3", CultureInfo.GetCultureInfo("ru-RU")) + "\r\n(" + PriceToText(kpModel.Cost) + ")";
				        }
				    }

				    foreach (KPOther xmodel in CollectionOther.GroupBy(t => t.Type).Select(t => t.FirstOrDefault()))
				    {
				        _range = _document.Range(_table.Range.End, _table.Range.End);
				        _document.Range(_table.Range.Start - 1, _table.Range.End + 1).Copy();
				        _range.Paste();
				        _workTable = _range.Tables[1];
				        _document.Range(_workTable.Range.Start, _workTable.Range.Start)
				            .InsertBreak(Word.WdBreakType.wdPageBreak);
				        //_workTable.Cell(1, 1).Range.Text = xmodel.Header;
				        foreach (KPOther kpModel in CollectionOther.Where(t => t.Type.Contains(xmodel.Type)))
				        {
				            int row = 2;
				            if (kpModel != xmodel)
				            {
				                row = 1;
				                _document.Range(_table.Cell(row, 1).Range.Start, _table.Range.End).Select();
				                _word.Selection.Copy();
				                _document.Range(_workTable.Range.End + 2, _workTable.Range.End + 2).Paste();
				                _workTable = _document.Range(_workTable.Range.End + 1, _document.Range().End)
				                    .Tables[1];
				                _document.Range(_workTable.Range.Start, _workTable.Range.Start)
				                    .InsertBreak(Word.WdBreakType.wdPageBreak);
				            }
				            Dispatcher.Invoke(() => { Clipboard.Clear(); });

				            _workTable.Cell(row, 1).Range.Text = kpModel.Name;
				            await Dispatcher.Invoke(async () =>
				            {
				                HttpClient _httpClient = new HttpClient();
				                await await _httpClient.GetAsync(kpModel.Path)
				                    .ContinueWith(
				                        async r =>
				                        {
				                            //if (r.IsFaulted) return null;
				                            //byte[] user = JsonConvert.DeserializeObject<byte[]>(await r.Result.Content.ReadAsStringAsync());
				                            byte[] str = await r.Result.Content.ReadAsByteArrayAsync();

				                            //StreamReader sr = new StreamReader(str);
				                            //StreamWriter sw = new StreamWriter(Directory.GetCurrentDirectory() + @"\1.png");
				                            //while (!sr.EndOfStream)
				                            //{
				                            //	sw.Write(sr.ReadLine());
				                            //}
				                            //sw.Close();
				                            FileStream fs = File.Create(Directory.GetCurrentDirectory() + @"\1.png",
				                                (int) str.Length);
				                            fs.Write(str, 0, str.Length);
				                            fs.Dispose();
				                        });

				                string pathes = Directory.GetCurrentDirectory() + @"\1.png";
				                Word.InlineShape shape = _workTable.Cell(row+10, 1).Range.InlineShapes
				                    .AddPicture(pathes, LinkToFile: false, SaveWithDocument: true);
				                float size = shape.Height / _workTable.Cell(row+10, 1).Height;
				                shape.Height = _workTable.Cell(row+10, 1).Height;
				                shape.Width = shape.Width / size;
				                File.Delete(Directory.GetCurrentDirectory() + @"\1.png");
				                //Word.InlineShape shape1 = _workTable.Cell(row+9, 1).Range.InlineShapes
				                //    .AddPicture((string) kpModel.Page.ContinueSolution.Tag, LinkToFile: false,
				                //        SaveWithDocument: true);
				                //size = shape1.Height / _workTable.Cell(row+9, 1).Height;
				                //shape1.Height = _workTable.Cell(row+9, 1).Height;
				                //shape1.Width = shape1.Width / size;
				            });
				            _workTable.Cell(row+1, 2).Delete(ShiftCells: 2);
				            _workTable.Cell(row+1, 2).Delete(ShiftCells: 2);
				            _workTable.Cell(row+1, 2).Delete(ShiftCells: 2);
				            _workTable.Cell(row, 2).Range.Text = kpModel.CollectionOthers.First().Name;
				            _workTable.Cell(row, 3).Range.Text = kpModel.CollectionOthers.First().Prop;

				            foreach (PropertiesOther col in kpModel.CollectionOthers.Where(p =>
				                kpModel.CollectionOthers.IndexOf(p) != 0))
				            {
				                _workTable.Rows.Add(_workTable.Cell(row+1, 3));
				                _workTable.Cell(row+1, 2).Range.Text = col.Name;
				                _workTable.Cell(row+1, 3).Range.Text = col.Prop;
				            }

				            _workTable.Cell(row, 4).Range.Text = kpModel.Count;
				            _workTable.Cell(row, 5).Range.Text = Convert.ToInt32(kpModel.Price).ToString("N3", CultureInfo.GetCultureInfo("ru-RU"))  + "\r\n(" + PriceToText(kpModel.Price) + ")";
				            _workTable.Cell(row, 6).Range.Text = Convert.ToInt32(kpModel.Cost).ToString("N3", CultureInfo.GetCultureInfo("ru-RU")) + "\r\n(" + PriceToText(kpModel.Cost) + ")";
				        }
				    }

				    _document.Range(_table.Range.Start, _document.Tables[3].Range.Start).Delete();
				    Dispatcher.Invoke(() => { Clipboard.Clear(); });
				    
					//object fl = Directory.GetCurrentDirectory() + @"\base\save.docx";

					//_document.Close(SaveChanges: false);
					//_word.Quit(SaveChanges: false);
					//	_word = null;
					return true;
				}
				catch (Exception e)
				{
					try
					{
						Dispatcher.Invoke(() =>
						{
						    WordSingleton.instance?.Application.Documents.Close(SaveChanges: false);
						    WordSingleton.instance?.Application.Quit(SaveChanges: false);
							WordSingleton.instance.Application = null;
							WordSingleton.instance = null;
							KillWordandExcel.Kill();
						});

					}
					catch (Exception ex)
					{
						Dispatcher.Invoke(() =>
						{
							KillWordandExcel.Kill();
						});

					}
					

					return false;
				}
			});
		}

	    private void SetComputer(Excel.Worksheet _worksheet, ComputerParts cpParts)
	    {
	        Excel.Range _range = _worksheet.Rows[8].EntireRow;
            if (cpParts.Cooler.Any())
	        {
                _range.Insert();
	            _range = _worksheet.Range[_worksheet.Cells[8, 1], _worksheet.Cells[8, 2]];
                _range.Merge();
	            _range.Value = "Кулер ЦПУ";
	            _range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 147, 196, 125));
                _worksheet.Cells[8, 3].Value = cpParts.Cooler[0];
	            _worksheet.Cells[8, 4].Value = cpParts.Cooler[1];
	            _range = _worksheet.Cells[8, 5];
	            _range.Formula = "=(D8*E2)";
            }
	        _range = _worksheet.Rows[8].EntireRow;
            if (cpParts.Block.Any())
	        {
	            _range.Insert();
	            _range = _worksheet.Range[_worksheet.Cells[8, 1], _worksheet.Cells[8, 2]];
	            _range.Merge();
	            _range.Value = "Блок питания";
	            _range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 147, 196, 125));
                _worksheet.Cells[8, 3].Value = cpParts.Block[0];
	            _worksheet.Cells[8, 4].Value = cpParts.Block[1];
	            _range = _worksheet.Cells[8, 5];
	            _range.Formula = "=(D8*E2)";
            }
	        _range = _worksheet.Rows[8].EntireRow;
            if (cpParts.Mat.Any())
	        {
	            _range.Insert();
	            _range = _worksheet.Range[_worksheet.Cells[8, 1], _worksheet.Cells[8, 2]];
	            _range.Merge();
	            _range.Value = "Материнская плата";
	            _range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 147, 196, 125));
                _worksheet.Cells[8, 3].Value = cpParts.Mat[0];
	            _worksheet.Cells[8, 4].Value = cpParts.Mat[1];
	            _range = _worksheet.Cells[8, 5];
	            _range.Formula = "=(D8*E2)";
            }
	        _range = _worksheet.Rows[8].EntireRow;
            if (cpParts.OZU.Any())
	        {
	            _range = _worksheet.Rows[8].EntireRow;
                foreach (string[] x in cpParts.OZU)
	            {
	                _range = _worksheet.Rows[8].EntireRow;
                    _range.Insert();
	                _worksheet.Cells[8, 3].Value = x[0];
	                _worksheet.Cells[8, 4].Value = x[1];
	                _range = _worksheet.Cells[8, 5];
	                _range.Formula = "=(D8*E2)";
                }
	            _range = _worksheet.Range[_worksheet.Cells[8, 1], _worksheet.Cells[7 + cpParts.OZU.Count, 2]];
	            _range.Merge();
	            _range.Value = "ОЗУ";
	            _range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 147, 196, 125));
            }
	        if (cpParts.Disk.Any())
	        {
	            _range = _worksheet.Rows[8].EntireRow;
                foreach (string[] x in cpParts.Disk)
	            {
	                _range = _worksheet.Rows[8].EntireRow;
                    _range.Insert();
	                _worksheet.Cells[8, 3].Value = x[0];
	                _worksheet.Cells[8, 4].Value = x[1];
	                _range = _worksheet.Cells[8, 5];
	                _range.Formula = "=(D8*E2)";
	            }
	            _range = _worksheet.Range[_worksheet.Cells[8, 1], _worksheet.Cells[7 + cpParts.Disk.Count, 2]];
	            _range.Merge();
	            _range.Value = "Накопители";
	            _range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 147, 196, 125));
            }
	        _range = _worksheet.Rows[8].EntireRow;
            if (cpParts.Processor.Any())
	        {
	            _range.Insert();
	            _range = _worksheet.Range[_worksheet.Cells[8, 1], _worksheet.Cells[8, 2]];
	            _range.Merge();
	            _range.Value = "Процессор";
	            _range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 147, 196, 125));
                _worksheet.Cells[8, 3].Value = cpParts.Processor[0];
	            _worksheet.Cells[8, 4].Value = cpParts.Processor[1];
	            _range = _worksheet.Cells[8, 5];
	            _range.Formula = "=(D8*E2)";
            }
	        _range = _worksheet.Rows[8].EntireRow;
            if (cpParts.VideoCard.Any())
	        {
	            _range.Insert();
	            _range = _worksheet.Range[_worksheet.Cells[8, 1], _worksheet.Cells[8, 2]];
	            _range.Merge();
	            _range.Value = "Видеокарта";
	            _range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 147, 196, 125));
                _worksheet.Cells[8, 3].Value = cpParts.VideoCard[0];
	            _worksheet.Cells[8, 4].Value = cpParts.VideoCard[1];
	            _range = _worksheet.Cells[8, 5];
	            _range.Formula = "=(D8*E2)";
            }

	       

        }

        public async Task<bool> CreateSB(Excel.Application _excel)
	    {
	        try
	        {
	        return await Task.Run(() =>
	        {
	            Excel.Workbook _workbook = _excel.Workbooks.Open(Directory.GetCurrentDirectory() + @"\base\Sebestemplate.xlsx");

	            Excel.Worksheet _worksheet = _workbook.Worksheets[1];
	            Excel.Range _range;

                foreach (KPModel xmodel in Collection.GroupBy(t => t.Type).Select(t => t.FirstOrDefault()))
                {
                    foreach (KPModel kpModel in Collection)
                    {

                        _workbook.Worksheets[_workbook.Worksheets.Count].UsedRange.Copy();
                        _worksheet = _workbook.Worksheets.Add();
                        _worksheet.UsedRange.PasteSpecial(Excel.XlPasteType.xlPasteColumnWidths);
                        _worksheet.UsedRange.PasteSpecial();
                        try
                        {
                            _worksheet.Name = kpModel.Name.Replace("\r\n", " ");
                        }
                        catch (Exception e)
                        {
                            _worksheet.Name = kpModel.Name.Split(new []{"\r\n"},StringSplitOptions.RemoveEmptyEntries)[1];
                        }
                       
                        int row = 3;
                       

                        _worksheet.Cells[1, 1].Value = kpModel.Name;
                        _worksheet.Cells[2, 2].Value = kpModel.Name.Split(' ')[1];
                        _worksheet.Cells[4, 3].Value = kpModel.Body;
                        
                        _worksheet.Cells[7, 3].Value = kpModel.Display;
                        
                        _worksheet.Cells[8, 3].Value = kpModel.Typesensor;
                        
                        _worksheet.Cells[9, 3].Value = kpModel.TypePO;
                        
                        _worksheet.Cells[2, 5].Value = kpModel.Count;
                        Dispatcher.Invoke(() => {
                            _worksheet.Cells[4, 4].Value = kpModel.Page.Body.Price.Contains("$")? Convert.ToInt32((Convert.ToDouble((kpModel.Page.Body.Price.Replace("$", "")))) * (65.63)).ToString() : kpModel.Page.Body.Price;
                            _worksheet.Cells[7, 4].Value = kpModel.Page.Display.Price.Contains("$") ? Convert.ToInt32((Convert.ToDouble((kpModel.Page.Display.Price.Replace("$", "")))) * (65.63)).ToString() : kpModel.Page.Display.Price; ;
                            _worksheet.Cells[8, 4].Value = kpModel.Page.Typesensor.Price.Contains("$") ? Convert.ToInt32((Convert.ToDouble((kpModel.Page.Typesensor.Price.Replace("$", "")))) * (65.63)).ToString() : kpModel.Page.Typesensor.Price; ;
                            _worksheet.Cells[9, 4].Value = kpModel.Page.PO.Price.Contains("$") ? Convert.ToInt32((Convert.ToDouble((kpModel.Page.PO.Price.Replace("$", "")))) * (65.63)).ToString() : kpModel.Page.PO.Price; ;
                        });
                        if (kpModel.ComputerParts.Extradevice.Any())
                        {
                            _range = _worksheet.Rows[8].EntireRow;
                            foreach (string[] x in kpModel.ComputerParts.Extradevice)
                            {
                                _range = _worksheet.Rows[8].EntireRow;
                                _range.Insert();
                                _worksheet.Cells[8, 3].Value = x[0];
                                _worksheet.Cells[8, 4].Value = x[1];
                                _range = _worksheet.Cells[8, 5];
                                _range.Formula = "=(D8*E2)";
                            }
                            _range = _worksheet.Range[_worksheet.Cells[8, 1],
                            _worksheet.Cells[7 + kpModel.ComputerParts.Extradevice.Count, 2]];
                            _range.Merge();
                            _range.Value = "Доп оборудование";
                            _range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(255, 244, 204, 204));
                        }
                        SetComputer(_worksheet, kpModel.ComputerParts);
                    }
                }

                //foreach (KPOther xmodel in CollectionOther.GroupBy(t => t.Type).Select(t => t.FirstOrDefault()))
                //{
                //    _range = _document.Range(_table.Range.End + 2, _table.Range.End + 2);
                //    _document.Range(_table.Range.Start - 1, _table.Range.End + 1).Copy();
                //    _range.Paste();
                //    _workTable = _range.Tables[1];
                //    _document.Range(_workTable.Range.Start, _workTable.Range.Start)
                //        .InsertBreak(Word.WdBreakType.wdPageBreak);
                //    _workTable.Cell(1, 1).Range.Text = xmodel.Header;
                //    foreach (KPOther kpModel in CollectionOther.Where(t => t.Type.Contains(xmodel.Type)))
                //    {
                //        int row = 3;
                //        if (kpModel != xmodel)
                //        {
                //            row = 1;
                //            _document.Range(_table.Cell(3, 1).Range.Start, _table.Range.End).Select();
                //            _word.Selection.Copy();
                //            _document.Range(_workTable.Range.End + 2, _workTable.Range.End + 2).Paste();
                //            _workTable = _document.Range(_workTable.Range.End + 1, _document.Range().End)
                //                .Tables[1];
                //            _document.Range(_workTable.Range.Start, _workTable.Range.Start)
                //                .InsertBreak(Word.WdBreakType.wdPageBreak);
                //        }
                //        Dispatcher.Invoke(() => { Clipboard.Clear(); });

                //        _workTable.Cell(row, 1).Range.Text = kpModel.Name;

                //        _workTable.Cell(row + 1, 2).Delete(ShiftCells: 2);
                //        _workTable.Cell(row + 1, 2).Delete(ShiftCells: 2);
                //        _workTable.Cell(row + 1, 2).Delete(ShiftCells: 2);
                //        _workTable.Cell(row, 2).Range.Text = kpModel.CollectionOthers.First().Name;
                //        _workTable.Cell(row, 3).Range.Text = kpModel.CollectionOthers.First().Prop;

                //        foreach (PropertiesOther col in kpModel.CollectionOthers.Where(p =>
                //            kpModel.CollectionOthers.IndexOf(p) != 0))
                //        {
                //            _workTable.Rows.Add(_workTable.Cell(4, 3));
                //            _workTable.Cell(row + 1, 2).Range.Text = col.Name;
                //            _workTable.Cell(row + 1, 3).Range.Text = col.Prop;
                //        }

                //        _workTable.Cell(row, 4).Range.Text = kpModel.Count;
                //        _workTable.Cell(row, 5).Range.Text = Convert.ToInt32(kpModel.Price).ToString("N3", CultureInfo.GetCultureInfo("ru-RU")) + "\r\n(" + PriceToText(kpModel.Price) + ")";
                //        _workTable.Cell(row, 6).Range.Text = Convert.ToInt32(kpModel.Cost).ToString("N3", CultureInfo.GetCultureInfo("ru-RU")) + "\r\n(" + PriceToText(kpModel.Cost) + ")";
                //    }

	       
               
                return true;
	        });
	        }
	        catch (Exception e)
	        {
	            return false;
	        }
        }

		#region Перевод числа в слово
		
		public uint p;
		/// <summary>
		/// Этот метод переводит число в слово
		/// </summary>
		/// <param name="price">Число, которое нужно перевести</param>
		/// <returns>Текстовое представление</returns>
		public string PriceToText(string price)
		{
			string text = "";
			p = Convert.ToUInt32(price);

			text = Iteration("рубль", "рубля", "рублей");
			if (p%1000 != 0)
			{
				string txt = Iteration("тысяча", "тысячи", "тысяч");
				if (txt.Contains("один "))
				{
					txt = txt.Replace("один ", "одна ");
				}
				if (txt.Contains("два "))
				{
					txt = txt.Replace("два ", "две ");
				}
				text = txt + " " + text;
			}
			else
				p /= 1000;

			if (p%1000 != 0)
				text = Iteration("миллион", "миллиона", "миллионов") + " " + text;
			else
				p /=1000;
			if (p % 1000 != 0)
				text = Iteration("миллиард", "миллиарда", "миллиардов")+ " "+ text;
			else
				p /= 1000;
			return text;
		}
		private string Iteration(string s1, string s2, string s3)
		{
			string text = "";
			if (p % 100 >= 10 & p % 100 < 20)
			{
				text = Decimes(p % 100)+" "+s3;
				p/=100;
			}
			else
			{
				if (p % 10 != 0)
				{
					text = One(p % 10);
					if (p % 10 == 1)
						text = text + " "+s1;
					if (p % 10 > 1 & p % 10 < 5)
						text = text + " "+s2;
					if (p % 10 > 4)
						text = text + " " + s3;
				}
				else
					text = text + s3;
				p /= 10;
				if (p % 10 != 0)
					text = Decimes(p % 10) + " " + text;
				p /= 10;
			}
			if(p % 10!=0)
				text = Hundreds(p % 10) + " " + text;
			p /= 10;
			return text;

		}

		private string Hundreds(uint hun)
		{
			switch (hun)
			{
				case 1: return "сто";
				case 2: return "двести";
				case 3: return "триста";
				case 4: return "четыреста";
				case 5: return "пятьсот";
				case 6: return "шестьсот";
				case 7: return "семьсот";
				case 8: return "восемьсот";
				case 9: return "девятьсот";
			}
			return "";
		}

		private string One(uint one)
		{
			switch (one)
			{
				case 1: return "один";
				case 2: return "два";
				case 3: return "три";
				case 4: return "четыре";
				case 5: return "пять";
				case 6: return "шесть";
				case 7: return "семь";
				case 8: return "восемь";
				case 9: return "девять";
			}
				return "";
		}

		private string Decimes(uint dec)
		{
			if (dec>9)
			{
				switch (dec)
			{
				case 10: return "десять";
				case 11: return "одиннадцать";
				case 12: return "двенадцать";
				case 13: return "тринадцать";
				case 14: return "четырнадцать";
				case 15: return "пятнадцать";
				case 16: return "шестнадцать";
				case 17: return "семнадцать";
				case 18: return "восемнадцать";
				case 19: return "девятнадцать";
			}
			}
			else
			{
				switch (dec)
				{
					case 2: return "двадцать";
					case 3: return "тридцать";
					case 4: return "сорок";
					case 5: return "пятьдесят";
					case 6: return "шесьдесят";
					case 7: return "семьдесят";
					case 8: return "восемьдесят";
					case 9: return "девяносто";
				}

			}


			return "";
		}
		#endregion
		

		private void Description_OnNavigated(object sender, NavigationEventArgs e)
		{
			if (Description.Content is Preview)
			{
				Blur.Visibility = Visibility.Visible;
			}
			else
			{
				Blur.Visibility = Visibility.Collapsed;
			}
			if(Description.Content is null)
				foreach (var xPro in Item.Where(x=>x.Checked==true))
				{
					xPro.Checked = false;
				}
		}
	}

	public class ItemProperty: INotifyPropertyChanged
	{
		public string Text { get; set; }

	    private ObservableCollection<Page> _page;

	    public ObservableCollection<Page> Page
	    {
	        get => _page ?? (_page = new ObservableCollection<Page>());
	        set { _page = value; }
	    }

	    public string Header { get; set; }
		private bool _checked;
		public bool Checked
		{
			get => _checked;
			set
			{
				_checked = value;
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
