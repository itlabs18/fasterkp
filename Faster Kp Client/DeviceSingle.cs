﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Faster_Kp_Client.Annotations;
using Faster_Kp_Client.KPPages;
using Faster_Kp_Client.Models;

namespace Faster_Kp_Client
{
	class Listik : INotifyPropertyChanged
	{
		private string _key;
		public string Key
		{
			get => _key;
			set
			{
				_key = value;
				OnPropertyChanged();
			}
		}
		private string _header;
		public string Header
		{
			get => _header;
			set
			{
				_header = value;
				OnPropertyChanged();
			}
		}

		private bool _value;
		public bool Value
		{
			get => _value;
			set
			{
				_value = value;
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
	class DeviceSingle : INotifyPropertyChanged
	{
		private static DeviceSingle instance;

		private ObservableCollection<Listik> _device;

		public  ObservableCollection<Listik> Device { get => _device ??(_device = new ObservableCollection<Listik>());
			set
			{
				_device = value;
				OnPropertyChanged();
			}
		}

		protected DeviceSingle(string [] key)
		{
			foreach (var x in key)
			{
				this.Device.Add(new Listik()
				{
					Key = x,
					Value = false,
					Header = ""
				});
			}
		}

		public static DeviceSingle getInstance(string [] key = null)
		{
			return instance ?? (instance = new DeviceSingle(key));
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
