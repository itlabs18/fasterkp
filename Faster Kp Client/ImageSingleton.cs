﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Faster_Kp_Client.Annotations;

namespace Faster_Kp_Client
{
	class ImageSingleton : INotifyPropertyChanged
	{
		public static ImageSingleton instance;

		private string _surname;
		public string Surname
		{
			get => _surname;
			set
			{
				_surname = value;
				OnPropertyChanged();
			}
		}

		private string _position;
		public string Position
		{
			get => _position;
			set
			{
				_position = value;
				OnPropertyChanged();
			}
		}

		private string _name;
		public string Name
		{
			get => _name;
			set
			{
				_name = value;
				OnPropertyChanged();
			}
		}

		private ImageSource _image;
		public ImageSource Image
		{
			get =>_image;
			set
			{
				_image = value;
				OnPropertyChanged();
			}

		}

		protected ImageSingleton(ImageSource _image, string _surname, string _name, string _position)
		{
			this.Image = _image;
			this.Surname = _surname;
			this.Name = _name;
			this.Position = _position;
		}

		public static ImageSingleton getInstance(ImageSource _image, string _surname, string _name, string _position)
		{
			if(instance == null)
				instance = new ImageSingleton(_image, _surname, _name, _position);
			return instance;
		}

		public event PropertyChangedEventHandler PropertyChanged;
		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}

	
}
