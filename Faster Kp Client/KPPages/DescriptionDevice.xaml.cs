﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Faster_Kp_Client.controls;
using Microsoft.Win32;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Navigation;
using Faster_Kp_Client.Annotations;
using Faster_Kp_Client.Models;


namespace Faster_Kp_Client.KPPages
{
	/// <summary>
	/// Логика взаимодействия для DescriptionDevice.xaml
	/// </summary>
	public partial class DescriptionDevice : Page, INotifyPropertyChanged
    {
        public DescriptionDevice (CostPrice cp,string type = null)
        {
            InitializeComponent();

            _costPrice = cp;
			if (!string.IsNullOrEmpty(type))
		        Type = type;

        }

        #region Свойства
        public static readonly DependencyProperty TypePOProperty = DependencyProperty.Register(
            "TypePO", typeof(string), typeof(DescriptionDevice), new PropertyMetadata(default(string)));
        public string TypePO
        {
            get => (string)GetValue(TypePOProperty);
            set => SetValue(TypePOProperty, value);
        }
        private string _type { get; set; }
        private CostPrice _costPrice;
        public string Type
        {
            get => _type;
            set => _type = value;
        }

        public ComputerParts comp = new ComputerParts();


        #endregion


        #region Комманды

        private ICommand _setCustom;
	    public ICommand SetCustom => _setCustom ?? (_setCustom = new Command(() =>
	    {
			OpenFileDialog fd = new OpenFileDialog();
		    fd.Filter = "Файлы изображений (*.png, *.jpg)|*.png;*jpg";
		    if ((bool) fd.ShowDialog())
		    {
				Model.Visibility = Visibility.Visible;
			    if (fd.SafeFileName.Contains(".png") || fd.SafeFileName.Contains(".jpg"))
			    {
				    Model.Content = fd.SafeFileName.Replace(".png", "");
				    Model.Content = ((string)Model.Content).Replace(".jpg", "");
			    }
			    Model.Tag = fd.FileName;
			    Panel.Visibility = Visibility.Collapsed;
			}
		    else
		    {
			    CustomDesign.IsChecked = false;
		    }
	    }));

	    public ICommand SetResult => _setCustom ?? (_setCustom = new Command(() =>
	    {
			OpenFileDialog fd = new OpenFileDialog();
		    fd.Filter = "Файлы изображений (*.png, *.jpg)|*.png;*jpg";
		    if ((bool)fd.ShowDialog())
		    {
			    ContinueSolution.Tag = fd.FileName;
		    }
		}));

		private ICommand _setPO;
	    public ICommand SetPO => _setPO ?? (_setPO = new Command(p =>
		                             {
			                             NavigationService.Navigate(new SelectPO(this));
		                             }));


		private ICommand _setProperty;
	    public ICommand SetProperty => _setProperty ?? (_setProperty = new Command(p =>
	    {
		    SelectPropertyDevice x = (SelectPropertyDevice) p;
		    if (x.Number == "2" || x.Number=="3")
		    {
				NavigationService.Navigate((SelectDescriptions)x.PageView??(x.PageView = 
				                               new SelectDescriptions(this, x, Regex.Match(Display.Placehold, "\\d{3}\\W\\d{1}|\\d{2}\\W\\d{1}|\\d{3}|\\d{2}").Value)));
			}
		    else
		    {
			    if (x.Name == "Computer"||x.Name == "ExtraDevice")
				    NavigationService.Navigate((SelectComp)x.PageView ?? (x.PageView = new SelectComp(this, x)));
			    else
			    {
				    NavigationService.Navigate((SelectDescriptions)x.PageView ?? 
				                               (x.PageView = new SelectDescriptions(this, x)));
			    }
		    }
	    }));
	    private ICommand _setModel;
	    public ICommand SetModel => _setModel?? (_setModel= new Command(p =>
	    {
		    NavigationService.Navigate(new SelectModel(this, _costPrice));
	    }));

	    private ICommand _changeModel;
	    public ICommand ChangeModel => _changeModel ?? (_changeModel = new Command(p =>
	    {
		    Button b = (Button) p;
		    b.Visibility = Visibility.Collapsed;
		    b.Tag = null;
		    b.Content = "";
		    Panel.Visibility = Visibility.Visible;
		    CustomDesign.IsChecked = false;
		    ReadySolution.IsChecked = false;
	    }));

		private ICommand _change;
	    public ICommand ChangeYounge => _change ?? (_change = new Command(p =>
	    {
		    AddModel1 add = (AddModel1) p;
		    add.Visibility = Visibility.Collapsed;
		    Panel.Visibility = Visibility.Visible;

	    }));
		#endregion
		

		public event PropertyChangedEventHandler PropertyChanged;

	    [NotifyPropertyChangedInvocator]
	    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
	    {
		    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	    }
    }


}
