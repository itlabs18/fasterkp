﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Faster_Kp_Client.KPPages
{
	/// <summary>
	/// Логика взаимодействия для NamesTables.xaml
	/// </summary>
	public partial class NamesTables : Page
	{
		public NamesTables(CostPrice price)
		{
			InitializeComponent();
			cp = price;
			IpCollection = cp.Item;
		}
		#region DependencyProperties

		public static readonly DependencyProperty ErrorTextProperty = DependencyProperty.Register(
			"ErrorText", typeof(string), typeof(NamesTables), new PropertyMetadata(default(string)));

		public string ErrorText
		{
			get { return (string)GetValue(ErrorTextProperty); }
			set { SetValue(ErrorTextProperty, value); }
		}

		public static readonly DependencyProperty CommandProperty = DependencyProperty.Register(
			"Command", typeof(ICommand), typeof(NamesTables), new PropertyMetadata(default(ICommand)));

		public ICommand Command
		{
			get { return (ICommand)GetValue(CommandProperty); }
			set { SetValue(CommandProperty, value); }
		}

		public static readonly DependencyProperty EmptyProperty = DependencyProperty.Register(
			"Empty", typeof(bool), typeof(NamesTables), new PropertyMetadata(true));

		public bool Empty
		{
			get => (bool)GetValue(EmptyProperty);
			set => SetValue(EmptyProperty, value);
		}

		public static readonly DependencyProperty DateTextProperty = DependencyProperty.Register(
			"DateText", typeof(string), typeof(NamesTables), new PropertyMetadata(DateTime.Today.Date.ToShortDateString() + " г."));

		public string DateText
		{
			get => (string)GetValue(DateTextProperty);
			set => SetValue(DateTextProperty, value);
		}

		public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
			"Text", typeof(string), typeof(NamesTables), new PropertyMetadata(""));

		public string Text
		{
			get => (string)GetValue(TextProperty);
			set => SetValue(TextProperty, value);
		}
		#endregion
		public CostPrice cp;
		#region Комманды

		private ObservableCollection<ItemProperty> _ipCollection;

		public ObservableCollection<ItemProperty> IpCollection
		{
			get => _ipCollection ?? (_ipCollection = new ObservableCollection<ItemProperty>());
			set { _ipCollection = value; }
		}

		/// <summary>
		/// Перейти к предпросмотру
		/// </summary>
		private ICommand _continue;
        /// <summary>
        /// Продолжить
        /// </summary>
		public ICommand Continue => _continue ?? (_continue = new Command(() =>
		{
			//if (string.IsNullOrWhiteSpace(Text))
			//{
			//	ErrorText = "Введите адрес доски в трелло!";
			//	Text = "";
			//}
			//else
			//{
				//if (IpCollection.All(x => !string.IsNullOrWhiteSpace(x.Header)))
				//{
					NavigationService.Navigate(cp);
					cp.GoKP.Execute(null);
				//}
				//else
				//	ErrorCount.Visibility = Visibility.Visible;
			//}
		}));
		/// <summary>
		/// Закрыть окно
		/// </summary>
		private ICommand _close;
		public ICommand Close => _close ?? (_close = new Command(() => { NavigationService.Navigate(cp); }));
		private ICommand _closeError;
		public ICommand CloseError => _closeError ?? (_closeError = new Command(() =>
		{
			ErrorCount.Visibility = Visibility.Collapsed; }));

		#endregion
	}
}
