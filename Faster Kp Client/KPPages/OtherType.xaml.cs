﻿using Faster_Kp_Client.Annotations;
using Faster_Kp_Client.controls;
using Faster_Kp_Client.Models;
using Microsoft.Win32;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;


namespace Faster_Kp_Client.KPPages
{
	/// <summary>
	/// Логика взаимодействия для DescriptionDevice.xaml
	/// </summary>
	public partial class OtherType: Page,INotifyPropertyChanged
    {
        public OtherType(CostPrice cp, string type = null)
        {
            InitializeComponent();
            _costPrice = cp;
	        if (!string.IsNullOrEmpty(type))
		        Type = type;
			Collection.Add(new PropertiesOther());

        }

		//public static readonly DependencyProperty CollectionProperty = DependencyProperty.Register(
		// "Collection", typeof(ObservableCollection<PropertiesOther>), typeof(OtherType), new PropertyMetadata(new ObservableCollection<PropertiesOther>()));

		//public ObservableCollection<PropertiesOther> Collection
		//{
		// get => (ObservableCollection<PropertiesOther>) GetValue(CollectionProperty);
		// set => SetValue(CollectionProperty, value);
		//}
        private CostPrice _costPrice;


		private ObservableCollection<PropertiesOther> _collection;
		public ObservableCollection<PropertiesOther> Collection
		{
			get => _collection ?? (_collection = new ObservableCollection<PropertiesOther>());
			set
			{
				_collection = value;
				OnPropertyChanged();
			}
		}
		private string _type { get; set; }

	   

		public string Type
	    {
		    get => _type;
		    set => _type = value;
	    }

		#region Комманды

	    private ICommand _setCustom;
	    public ICommand SetCustom => _setCustom ?? (_setCustom = new Command(() =>
	    {
			OpenFileDialog fd = new OpenFileDialog();
		    fd.Filter = "Файлы изображений (*.png, *.jpg)|*.png;*jpg";
		    if ((bool) fd.ShowDialog())
		    {
				Model.Visibility = Visibility.Visible;
			    if (fd.SafeFileName.Contains(".png") || fd.SafeFileName.Contains(".jpg"))
			    {
				    Model.Content = fd.SafeFileName.Replace(".png", "");
				    Model.Content = ((string)Model.Content).Replace(".jpg", "");
			    }
			    Model.Tag = fd.FileName;
			    Panel.Visibility = Visibility.Collapsed;
			}
		    else
		    {
			    CustomDesign.IsChecked = false;
		    }
	    }));
	    public ICommand SetResult => _setCustom ?? (_setCustom = new Command(() =>
	    {
		    OpenFileDialog fd = new OpenFileDialog();
		    fd.Filter = "Файлы изображений (*.png, *.jpg)|*.png;*jpg";
		    if ((bool)fd.ShowDialog())
		    {
				ContinueSolution.Tag = fd.FileName;
			}
		    
	    }));
		private ICommand _setModel;
	    public ICommand SetModel => _setModel?? (_setModel= new Command(p =>
	    {
		    NavigationService.Navigate(new SelectModel(this, _costPrice));
	    }));
	    private ICommand _add;
	    public ICommand Add => _add ?? (_add = new Command(() =>
	    {
		    Collection.Add(new PropertiesOther());
	    }));
	    private ICommand _delete;
	    public ICommand Delete => _delete ?? (_delete = new Command(() =>
	    {
		   if(Collection.Count>1)
			   Collection.RemoveAt(Collection.Count-1);
	    }));

		private ICommand _changeModel;
	    public ICommand ChangeModel => _changeModel ?? (_changeModel = new Command(p =>
	    {
		    Button b = (Button) p;
		    b.Visibility = Visibility.Collapsed;
		    b.Tag = null;
		    b.Content = "";
		    Panel.Visibility = Visibility.Visible;
		    CustomDesign.IsChecked = false;
		    ReadySolution.IsChecked = false;
	    }));

		private ICommand _change;
	    public ICommand ChangeYounge => _change ?? (_change = new Command(p =>
	    {
		    AddModel1 add = (AddModel1) p;
		    add.Visibility = Visibility.Collapsed;
		    Panel.Visibility = Visibility.Visible;

	    }));
	    #endregion

	    public event PropertyChangedEventHandler PropertyChanged;

	    [NotifyPropertyChangedInvocator]
	    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
	    {
		    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	    }
    }

	

}
