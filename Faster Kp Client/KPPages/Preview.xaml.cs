﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using Faster_Kp_Client.Utilities;
using Application = System.Windows.Application;
using Button = System.Windows.Controls.Button;


namespace Faster_Kp_Client.KPPages
{
	/// <summary>
	/// Логика взаимодействия для Preview.xaml
	/// </summary>
	public partial class Preview : Page
    {
	    public Preview(int ID, CostPrice cpPrice = null, ReadyKP prePage = null, ReadyDevice ready = null)
        {
            InitializeComponent();
            
            ChangeButton.IsEnabled = WordSingleton.instance != null && ExcelSingleton.instance != null;

            if (WordSingleton.instance==null)
            {
                Word1.Visibility = Visibility.Collapsed;
                Excel1.Visibility = Visibility.Visible;
            }

            WPFWordControl.Word.controlChange+= WordOnControlChange;
            WPFWordControl.Excel.controlChange += ExcelOnControlChange;
            PageCostPrice = cpPrice;
	        PrePage = prePage;
	        ReadyDevices = ready;
	        ID_KP = ID;
        }

	    private int ID_KP;
	    private void WordOnControlChange(object sender, EventArgs e)
	    {
	        if (WordSingleton.instance!= null)
	        {
	            Word1.control.StartWord(WordSingleton.instance.Application);
            }
            WPFWordControl.Word.controlChange -= WordOnControlChange;
	    }
        private void ExcelOnControlChange(object sender, EventArgs e)
        {
            if (ExcelSingleton.instance != null)
            {
                Excel1.control.StartWord(ExcelSingleton.instance.Application);
            }
            WPFWordControl.Excel.controlChange -= ExcelOnControlChange;
        }

        public ReadyKP PrePage;
	    public ReadyDevice ReadyDevices;
	    public CostPrice PageCostPrice;

	    public static readonly DependencyProperty Progress2Property = DependencyProperty.Register(
		    "Progress2", typeof(string), typeof(Preview), new PropertyMetadata(default(string)));

	    public string Progress2
	    {
		    get { return (string) GetValue(Progress2Property); }
		    set { SetValue(Progress2Property, value); }
	    }

	    public static readonly DependencyProperty ProgressProperty = DependencyProperty.Register(
		    "Progress", typeof(string), typeof(Preview), new PropertyMetadata(default(string)));

	    public string Progress
	    {
		    get { return (string) GetValue(ProgressProperty); }
		    set { SetValue(ProgressProperty, value); }
	    }

	    public static readonly DependencyProperty TextNameProperty = DependencyProperty.Register(
		    "TextName", typeof(string), typeof(Preview), new PropertyMetadata(string.Empty));

	    public string TextName
	    {
		    get => (string) GetValue(TextNameProperty);
		    set => SetValue(TextNameProperty, value);
	    }
	    private static void CloseWord()
	    {
		    try
		    {
			    WordSingleton.instance?.Application.Documents.Close(SaveChanges: false);
			    WordSingleton.instance?.Application.Quit(SaveChanges: false);
		        ExcelSingleton.instance?.Application.ActiveWorkbook.Close(SaveChanges:false);
		        ExcelSingleton.instance?.Application.Workbooks.Close();
                ExcelSingleton.instance?.Application.Quit();
            }
		    catch { }
            if(WordSingleton.instance!=null)
		        WordSingleton.instance.Application = null;
		    WordSingleton.instance = null;
	        if (ExcelSingleton.instance != null)
	            ExcelSingleton.instance.Application = null;
	        ExcelSingleton.instance = null;
	        KillWordandExcel.Kill();
        }

	    private void KickDocs(ref string[] paths)
	    {
			while (paths.Any(x => x.Contains(".docx"))) 
			{
				try
				{
					File.Delete(paths.FirstOrDefault(x => x.Contains(".docx")));
					paths = Directory.GetFiles(Directory.GetCurrentDirectory() + @"\SaveDocs");
				}
				catch (Exception e)
				{
					break;
				}
			    
		    }
	        while (paths.Any(x => x.Contains(".xlsx")))
	        {
	            try
	            {
	                File.Delete(paths.FirstOrDefault(x => x.Contains(".xlsx")));
	                paths = Directory.GetFiles(Directory.GetCurrentDirectory() + @"\SaveDocs");
	            }
	            catch (Exception e)
	            {
	                break;
	            }

	        }
        }

		#region Комманды
		private ICommand _close;
	    public ICommand Close => _close ?? (_close = new Command(() =>
	    {
		    //Word1.control.CloseWord(WordSingleton.instance.Application);
		    CloseWord();
		    string[] files = Directory.GetFiles(Directory.GetCurrentDirectory() + @"\SaveDocs");
		    KickDocs(ref files);
			if (PrePage != null)
			    NavigationService.Navigate(PrePage);
		    if (PageCostPrice != null)
				NavigationService.Navigate(null);
		    if (ReadyDevices != null)
			    NavigationService.Navigate(ReadyDevices);

		}));

	    private ICommand _ok;
	    public ICommand OK => _ok ?? (_ok = new Command(p =>
	    {
		    try
		    {
			    FolderBrowserDialog fd = new FolderBrowserDialog();
			    if (((Button)p).Name=="CloseIt" && fd.ShowDialog() == DialogResult.OK)
			    {
				    WordSingleton.instance.Application.ActiveDocument.SaveAs(fd.SelectedPath + @"\" + TextName + ".docx");
				    Preloader.Visibility = Visibility.Collapsed;
				    Progress2 = $"КП выгружено на компьютере как {TextName}.docx\r\nДля продолжения работы закройте предпросмотр.";
			    }
		        if (((Button)p).Name == "SaveSebes" && fd.ShowDialog() == DialogResult.OK)
		        {
		            ExcelSingleton.instance.Application.ActiveWorkbook.SaveAs(fd.SelectedPath + @"\" + TextName + ".xlsx");
		            Preloader.Visibility = Visibility.Collapsed;
		            Progress2 = $"Таблица выгружена на компьютере как {TextName}.xlsx\r\nДля продолжения работы закройте предпросмотр.";
		        }
            }
		    catch 
		    {
				Progress2 = "КП или Таблицу выгрузить не удалось";
			}
				
			

		}));

		//private ICommand _click;
	 //   public ICommand Click => _click ?? (_click = new Command(p =>
	 //   {
		//    Word1.Visibility = Visibility.Collapsed;
		//    DQ.Visibility = Visibility.Visible;
	 //   }));

	    private ICommand _save;
	    public ICommand Save => _save ?? (_save = new Command(async () =>
	    {
		    try
		    {
			    string PathWord = Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\SaveDocs").FullName +
			                      @"\" + TextName + "copy.docx";
				WordSingleton.instance?.Application.ActiveDocument.SaveAs(PathWord);
			    string PathWord1 = Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\SaveDocs").FullName +
			               @"\" + TextName + ".docx";
			    WordSingleton.instance?.Application.ActiveDocument.SaveAs(PathWord1);
			    string PathExcel = Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\SaveDocs").FullName +
			                      @"\" + TextName + "copy.xlsx";
			    ExcelSingleton.instance?.Application.ActiveWorkbook.SaveAs(PathExcel);
			    string PathExcel1 = Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\SaveDocs").FullName +
			                       @"\" + TextName + ".xlsx";
			    ExcelSingleton.instance?.Application.ActiveWorkbook.SaveAs(PathExcel1);
				if (await WebApi.UploadDoc(PathExcel, PathWord, UserSingleton.instance.Model.Login, TextName, ID_KP))
			    {
				    Progress = $"Документы сохранёны в базе как {TextName}";
				    File.Delete(PathWord);
				    File.Delete(PathExcel);
				}
			    else
			    {
					Progress = "Документ не сохранён";
				    File.Delete(PathWord);
				    File.Delete(PathExcel);
				}
			    
			}
		    catch (Exception e)
		    {
			    Progress = "Документ не сохранён";
		    }
		    
	    }));
	    private ICommand _newKP;
	    public ICommand CreateNew => _newKP ?? (_newKP = new Command(() =>
	    {
		    CloseWord();
		    string[] files = Directory.GetFiles(Directory.GetCurrentDirectory() + @"/SaveDocs");
			KickDocs(ref files);
			if(PageCostPrice!=null)
		        PageCostPrice.main.Pages.Navigate(new TypeOfDevise(PageCostPrice.main));
			if(PrePage!=null)
				(PrePage.page as TypeOfDevise).Prew.Navigate(null);
		    if (ReadyDevices != null)
			    ReadyDevices.Cost.main.Pages.Navigate(new TypeOfDevise(ReadyDevices.Cost.main));
	    }));

	    private ICommand _closeTrello;
	    public ICommand CloseTrello => _closeTrello ?? (_closeTrello = new Command(() =>
	    {
            Word1.Visibility = Visibility.Visible;
            SendTrello.Visibility = Visibility.Collapsed;
		}));
		private ICommand _trello;
		public ICommand Trello => _trello ?? (_trello = new Command(() =>
		{
			Word1.Visibility = Visibility.Collapsed;
		    Excel1.Visibility = Visibility.Collapsed;
            SendTrello.Visibility = Visibility.Visible;
			                          }));
        private ICommand _changetable;
        public ICommand Changetable => _changetable ?? (_changetable = new Command(() =>
        {
            if (SendTrello.Visibility != Visibility.Visible)
            {
                var x = Word1.Visibility;
                Word1.Visibility = Excel1.Visibility;
                Excel1.Visibility = x;
            }
        }));
        private ICommand _trelloSend;
	    public ICommand TrelloSend => _trelloSend ?? (_trelloSend = new Command(async () =>
	    {
		    if (!string.IsNullOrWhiteSpace(SendTrello.Text))
		    {
			    Preloader.Visibility = Visibility.Visible;
			string PathWord = Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\SaveDocs").FullName +
			                  @"\" + TextName + "copy.docx";
		    WordSingleton.instance?.Application.ActiveDocument.SaveAs(PathWord);
		    string PathWord1 = Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\SaveDocs").FullName +
		                       @"\" + TextName + ".docx";
			    WordSingleton.instance?.Application.ActiveDocument.SaveAs(PathWord1);
				if (await WebApi.SendTrello(SendTrello.Text, PathWord))
		    {
			    SendTrello.Error.Text = $"КП отправлено в Трелло";
			    File.Delete(PathWord);
		    }
		    else
		    {
			    SendTrello.Error.Text = "Ошибка Трелло";
			    File.Delete(PathWord);
		    }
			    Preloader.Visibility = Visibility.Collapsed;
			}
		    else
		    {
			    SendTrello.Text = "";
			    SendTrello.Error.Text = "Адрес не валиден";
		    }
		}));
	    
		#endregion




	}
}
