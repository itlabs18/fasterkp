﻿using Faster_Kp_Client.Annotations;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Word = Microsoft.Office.Interop.Word;

namespace Faster_Kp_Client.KPPages
{
	/// <summary>
	/// Логика взаимодействия для ReadyDevice.xaml
	/// </summary>
	[Serializable]
	public partial class ReadyDevice : Page,INotifyPropertyChanged
	{
		public ReadyDevice(CostPrice cp)
		{
			InitializeComponent();
			Cost = cp;
			Cost.PasteTable = null;
			Items.Add(new ReadiItem());
			Items.First().CornerFist = new CornerRadius(3,0,0,3);
			Items.First().CornerLast = new CornerRadius(0,3,3,0);
		}

		private async Task<bool?> CreateKP(ObservableCollection<ReadiItem> it, Word.Application _word)
		{
			return await Task.Run(() => {
				try
				{
					
					Dispatcher.Invoke(() => { _document = _word.Documents.Open(PathToTemplate); });
					
					
					Word.Table _table = _document.Tables[1];
					Dispatcher.Invoke(() => { _table.Cell(1, 1).Range.Text = NumKp.Text + "от" + NumKp.DateText; });
					_table = _document.Tables[3];
					Dispatcher.Invoke(() => { _table.Cell(1, 2).Range.Text = UserSingleton.instance.Model.Surname + " " + UserSingleton.instance.Model.Name + " " + UserSingleton.instance.Model.Telefon_number; });
					_table = _document.Tables[2];
					Dispatcher.Invoke(() => { _table.Cell(1, 1).Range.Text = TextName; });
					var items = it.Where(x => !string.IsNullOrEmpty(x.Cost) &
					                          !string.IsNullOrEmpty(x.Price) &
					                          !string.IsNullOrEmpty(x.Count) &
					                          !string.IsNullOrEmpty(x.Name));
					if (!items.Any())
					{
						_document.Close(SaveChanges: false);
						_word.Quit(SaveChanges: false);
						return (bool?)null;
					}
					int Enumerator = 2;
					foreach (var x in items)
					{
						Enumerator++;
						_table.Rows.Add();
						_table.Cell(Enumerator, 1).Range.Text = x.Name;
					    _table.Cell(Enumerator, 2).Range.Text = Convert.ToInt32(x.Price).ToString("N3", CultureInfo.GetCultureInfo("ru-RU")) + "\r\n("+Cost.PriceToText(x.Price)+")";
						_table.Cell(Enumerator, 3).Range.Text = x.Count;
						_table.Cell(Enumerator, 4).Range.Text = Convert.ToInt32(x.Cost).ToString("N3", CultureInfo.GetCultureInfo("ru-RU")) + "\r\n(" + Cost.PriceToText(x.Cost) + ")";
					}

					_table.Rows[2].Range.Bold = 1;

					Dispatcher.Invoke(() =>
					{
						
						if (Toggle1.IsChecked)
						{
							Clipboard.Clear();
							_document.Range(_table.Range.Start - 1, _table.Range.End + 1).Copy();
							Cost.PasteTable = Clipboard.GetData("Rich Text Format");
							_document.Close(SaveChanges: false);
							_word.Quit(SaveChanges: false);
							WordSingleton.instance.Application = null;
							WordSingleton.instance = null;
							KillWordandExcel.Kill();
						}
					});
					
					return true;
				}
				catch (Exception e)
				{
					try
					{
						Dispatcher.Invoke(() => { _document.Close(SaveChanges: false); });
					}
					catch {}
					try
					{
						Dispatcher.Invoke(() => { _word.Quit(SaveChanges: false); });
					}
					catch { }
					WordSingleton.instance.Application = null;
					WordSingleton.instance = null;
					KillWordandExcel.Kill();
					return false;
				}
			});
		}

		private ICommand _goKP;
		public ICommand GoKP => _goKP ?? (_goKP = new Command(async () =>
		{
			if (string.IsNullOrWhiteSpace(NumKp.Text))
			{
				NumKp.ErrorText = "Номер КП не может быть пустым";
				NumKp.Text = "";
			}
			else
			{
				
					NumKp.Visibility = Visibility.Collapsed;
					Preloader.Visibility = Visibility.Visible;
					if (Items.Any(t => !string.IsNullOrEmpty(t.Cost) &
					                   !string.IsNullOrEmpty(t.Price) &
					                   !string.IsNullOrEmpty(t.Count) &
					                   !string.IsNullOrEmpty(t.Name)))
					{
						Exeption.Visibility = Visibility.Collapsed;
						var x = await CreateKP(Items, WordSingleton.getInstance("WORD VIEWER").Application);
						if (x == true)
						{
							Cost.PasteTable = null;
							Preloader.Visibility = Visibility.Collapsed;
							NavigationService.Navigate(new Preview(0,null, null, this));
						}

						if (x == false)
						{
							Preloader.Visibility = Visibility.Collapsed;
							Exeption.Text = "Ошибка шаблона!";
							Exeption.Visibility = Visibility.Visible;
						}

						if (x == null)
						{
							Preloader.Visibility = Visibility.Collapsed;
							Exeption.Text = "Ошибка! Заполните все поля!";
							Exeption.Visibility = Visibility.Visible;
						}
					}
					else
					{
						Preloader.Visibility = Visibility.Collapsed;
						Exeption.Text = "Ошибка! Заполните все поля!";
						Exeption.Visibility = Visibility.Visible;
					}
				}
		}));
		private ICommand _ok;
		public ICommand OK => _ok ?? (_ok = new Command(async () =>
		{
			Exeption.Visibility = Visibility.Collapsed;
			if (!Toggle1.IsChecked)
			{
				if (string.IsNullOrWhiteSpace(NameTable.Text))
				{
					Exeption.Text = "Введите имя таблицы";
					Exeption.Visibility = Visibility.Visible;
				}
				else
				{
					if (Items.Any())
					{
						NumKp.Visibility = Visibility.Visible;
						Exeption.Visibility = Visibility.Collapsed;
					}
					else
					{
						Exeption.Text = "Добавьте элементы и введите данные!";
						Exeption.Visibility = Visibility.Visible;
					}
				}
			}
			else
			{
				if (string.IsNullOrWhiteSpace(NameTable.Text))
				{
					Exeption.Text = "Введите имя таблицы";
					Exeption.Visibility = Visibility.Visible;
				}
				else
				{
					if (Items.Any())
					{
						Preloader.Visibility = Visibility.Visible;
						if (Items.Any(t => !string.IsNullOrEmpty(t.Cost) &
						                   !string.IsNullOrEmpty(t.Price) &
						                   !string.IsNullOrEmpty(t.Count) &
						                   !string.IsNullOrEmpty(t.Name)))
						{
							
							var x = await CreateKP(Items, WordSingleton.getInstance("WORD VIEWER").Application);
							if (x == true)
							{
								Preloader.Visibility = Visibility.Collapsed;
								Cost.Product.IsChecked = true;
								NavigationService.Navigate(null);
								
								Cost.TextError.Text = "Готовое оборудование добавлено в основное КП";
								Cost.ErrorCount.Visibility = Visibility.Visible;
							}

							if (x == false)
							{
								Preloader.Visibility = Visibility.Collapsed;
								Exeption.Text = "Ошибка шаблона!";
								Exeption.Visibility = Visibility.Visible;
							}

							if (x == null)
							{
								Preloader.Visibility = Visibility.Collapsed;
								Exeption.Text = "Ошибка! Заполните все поля!";
								Exeption.Visibility = Visibility.Visible;
							}
						}
						else
						{
							Preloader.Visibility = Visibility.Collapsed;
							Exeption.Text = "Ошибка! Заполните все поля!";
							Exeption.Visibility = Visibility.Visible;
						}
					}
					else
					{
						Exeption.Text = "Добавьте элементы и введите данные!";
						Exeption.Visibility = Visibility.Visible;
					}
				}

			}
		}));
		private string PathToTemplate = Directory.GetCurrentDirectory() + @"\base\readysolution.docx";
		public CostPrice Cost;
		private ICommand _close;
		public ICommand Close => _close ?? (_close = new Command(() =>
		{
			Cost.Product.IsChecked = true;
			NavigationService.Navigate(null);

		}));
		private ObservableCollection<ReadiItem> _items;
		public ObservableCollection<ReadiItem> Items
		{
			get => _items ?? (_items = new ObservableCollection<ReadiItem>());
			set
			{
				_items = value;
				OnPropertyChanged();
			}
		}

		
		Word.Document _document;

		public static readonly DependencyProperty TextNameProperty = DependencyProperty.Register(
			"TextName", typeof(string), typeof(ReadyDevice), new PropertyMetadata(default(string)));

		public string TextName
		{
			get { return (string) GetValue(TextNameProperty); }
			set { SetValue(TextNameProperty, value); }
		}
		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		private void TextChanged(object s, TextCompositionEventArgs e)
		{
			e.Handled = !(char.IsDigit(e.Text, 0));
		}

		private ICommand _add;
		public ICommand Add => _add ?? (_add = new Command(() =>
		{
			Items.Add(new ReadiItem());
			if (Items.Count > 1)
			{ 
			Items.First().CornerFist = new CornerRadius(3,0,0,0);
			Items.First().CornerLast = new CornerRadius(0, 3, 0, 0);
			Items.Last().CornerFist = new CornerRadius(0,0,0,3);
			Items.Last().CornerLast = new CornerRadius(0,0,3,0);
				for (int i = 1; i>0 && i<Items.Count-1; i++)
				{
					Items[i].CornerFist = new CornerRadius(0,0,0,0);
					Items[i].CornerLast = new CornerRadius(0, 0, 0, 0);
				}
			}
			else
			{
				Items.First().CornerFist = new CornerRadius(3, 0, 0, 3);
				Items.First().CornerLast = new CornerRadius(0, 3, 3, 0);
			}
			
		}));
		private ICommand _delete;
		public ICommand Delete => _delete ?? (_delete = new Command(() =>
		{
			if (Items.Any())
			{
				Items.RemoveAt(Items.Count-1);
				if (Items.Count > 1)
				{
					Items.Last().CornerFist = new CornerRadius(0, 0, 0, 3);
					Items.Last().CornerLast = new CornerRadius(0, 0, 3, 0);
				}
				if (Items.Count == 1)
				{ 
					Items.First().CornerFist = new CornerRadius(3, 0, 0, 3);
					Items.First().CornerLast = new CornerRadius(0, 3, 3, 0);
				}

			}
		}));

		
	}
	

	public class ReadiItem : INotifyPropertyChanged
	{
		private string _name;
		public string Name
		{
			get => _name;
			set
			{
				_name = value;
				int i;
				int b;
				if (string.IsNullOrWhiteSpace(_price) || _price.Any(p => !char.IsDigit(p.ToString(), 0)))
				{
					i = 0;
				}
				else
				{
					i = Convert.ToInt32(Price ?? "0");
				}

				if (string.IsNullOrWhiteSpace(_count) || _count.Any(p => !char.IsDigit(p.ToString(), 0)))
				{
					b = 0;
				}
				else
				{
					b = Convert.ToInt32(Count ?? "0");
				}
				Cost = (i * b).ToString();
				OnPropertyChanged();
			}
		}

		private string _price;
		public string Price	
		{
			get
			{
				return _price;
			}
			set
			{
				_price = value;
				int i;
				int b;
				if (string.IsNullOrWhiteSpace(_price)|| _price.Any(p=>!char.IsDigit(p.ToString(),0)))
				{
					i = 0;
				}
				else
				{
					i = Convert.ToInt32(Price ?? "0");
				}

				if (string.IsNullOrWhiteSpace(_count) || _count.Any(p => !char.IsDigit(p.ToString(), 0)))
				{
					b = 0;
				}
				else
				{
					b = Convert.ToInt32(Count ?? "0");
				}
				Cost = (i * b).ToString();
				OnPropertyChanged();
			}
		}

		private string _count;
		public string Count
		{
			get
			{
				
				return _count;
			}
			set
			{
				_count = value;
				int i;
				int b;
				if (string.IsNullOrWhiteSpace(_price) || _price.Any(p => !char.IsDigit(p.ToString(), 0)))
				{
					i = 0;
				}
				else
				{
					i = Convert.ToInt32(Price ?? "0");
				}

				if (string.IsNullOrWhiteSpace(_count) || _count.Any(p => !char.IsDigit(p.ToString(), 0)))
				{
					b = 0;
				}
				else
				{
					b = Convert.ToInt32(Count ?? "0");
				}
				Cost = (i * b).ToString();
				OnPropertyChanged();
			}
		}

		private CornerRadius _cornerfist;
		public CornerRadius CornerFist
		{
			get => _cornerfist;
			set
			{
				_cornerfist = value;
				OnPropertyChanged();
			}
		}

		private CornerRadius _cornerlast;
		public CornerRadius CornerLast
		{
			get => _cornerlast;
			set
			{
				_cornerlast = value;
				OnPropertyChanged();
			}
		}

		private string _cost;
		public string Cost
		{
			get => _cost;
			set
			{
				_cost = value;
				OnPropertyChanged();
			}
		}
		
		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
