﻿using Faster_Kp_Client.Annotations;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Faster_Kp_Client.Models;
using Faster_Kp_Client.Utilities;
using Microsoft.Win32;

namespace Faster_Kp_Client.KPPages
{
	/// <summary>
	/// Логика взаимодействия для ReadyKP.xaml
	/// </summary>
	public partial class ReadyKP : Page, INotifyPropertyChanged
	{
		public ReadyKP(Page p, MainWindow mw)
		{
			InitializeComponent();
			main = mw;
			page = p;
		}

		public MainWindow main;
		public Page page;
		private ObservableCollection<ReadyDoc> _readyDocs;

		public ObservableCollection<ReadyDoc> ReadyDocs
		{
			get => _readyDocs ?? (_readyDocs = new ObservableCollection<ReadyDoc>());
			set
			{
				_readyDocs = value;
				OnPropertyChanged();
			}
		}

		private ObservableCollection<ReadyDoc> _readyExcels;

		public ObservableCollection<ReadyDoc> ReadyExcels
		{
			get => _readyExcels ?? (_readyExcels = new ObservableCollection<ReadyDoc>());
			set
			{
				_readyExcels = value;
				OnPropertyChanged();
			}
		}

		private string _path;

		public string Path
		{
			get => _path;
			set
			{
				_path = value;
				if (!string.IsNullOrWhiteSpace(value))
					ContinueButton.IsEnabled = true;
				OnPropertyChanged();
			}
		}

		public int ID_doc;

		private string _namedoc;

		public string Namedoc
		{
			get => _namedoc;
			set
			{
				_namedoc = value;
				OnPropertyChanged();
			}
		}

		public async Task<bool> GetElements(ObservableCollection<ReadyDoc> rd, ObservableCollection<ReadyDoc> re,
			string path)
		{
			return await Task.Run(async () =>
			{
				try
				{
					Dispatcher.Invoke(() => { Preloader.Visibility = Visibility.Visible; });
					Dispatcher.Invoke(() => { rd.Clear(); });
				    Dispatcher.Invoke(() => { re.Clear(); });
                    ObservableCollection<BaseModel> dic = await WebApi.GetDocs(UserSingleton.instance.Model.Login);
					if (dic == null)
					{
						return false;
					}

					foreach (var x in dic.Where(t=>!string.IsNullOrWhiteSpace(t.Name_KP)))
					{
						Dispatcher.Invoke(() =>
						{
							rd.Add(new ReadyDoc()
							{
								NameDoc = x.Name_KP,
								Path ="SaveDocs\\"+ x.Login+"\\"+x.Name_KP,
								CornerFist = new CornerRadius(0, 0, 0, 0),
								CornerLast = new CornerRadius(0, 0, 0, 0),
								DateDoc = x.Date_KP
							});
						});
					}

					foreach (var x in dic.Where(t => !string.IsNullOrWhiteSpace(t.Name_SB)))
					{
						Dispatcher.Invoke(() =>
						{
							re.Add(new ReadyDoc()
							{
								NameDoc = x.Name_SB,
								Path = "SaveDocs\\" + x.Login + "\\" + x.Name_SB,
								CornerFist = new CornerRadius(0, 0, 0, 0),
								CornerLast = new CornerRadius(0, 0, 0, 0),
								DateDoc = x.Date_SB
							});
						});
					}

					Dispatcher.Invoke(() =>
					{
						switch (rd.Count)
						{
							case 1:
								rd.First().CornerFist = new CornerRadius(3, 0, 0, 3);
								rd.First().CornerLast = new CornerRadius(0, 3, 3, 0);
								break;
							case 0:
								break;
							default:
								rd.First().CornerFist = new CornerRadius(3, 0, 0, 0);
								rd.First().CornerLast = new CornerRadius(0, 3, 0, 0);
								rd.Last().CornerFist = new CornerRadius(0, 0, 0, 3);
								rd.Last().CornerLast = new CornerRadius(0, 0, 3, 0);
								break;
						}

						switch (re.Count)
						{
							case 1:
								re.First().CornerFist = new CornerRadius(3, 0, 0, 3);
								re.First().CornerLast = new CornerRadius(0, 3, 3, 0);
								break;
							case 0:
								break;
							default:
								re.First().CornerFist = new CornerRadius(3, 0, 0, 0);
								re.First().CornerLast = new CornerRadius(0, 3, 0, 0);
								re.Last().CornerFist = new CornerRadius(0, 0, 0, 3);
								re.Last().CornerLast = new CornerRadius(0, 0, 3, 0);
								break;
						}
					});
					Dispatcher.Invoke(() => { Preloader.Visibility = Visibility.Collapsed; });
					return true;
				}
				catch (Exception e)
				{
					return false;
				}
			});
		}

		#region Комманды

		private ICommand _getPath;

		/// <summary>
		/// Получить путь
		/// </summary>
		public ICommand GetPath => _getPath ?? (_getPath = new Command(p =>
		{
			ReadyDoc rd = p as ReadyDoc;
			Path = rd.Path;
			Namedoc = rd.NameDoc;
		}));

		private ICommand _localDoc;

		/// <summary>
		/// Получить локальное КП
		/// </summary>
		public ICommand GetLocal => _localDoc ?? (_localDoc = new Command(() =>
		{
			OpenFileDialog opfd = new OpenFileDialog
			{
				Filter = "Локальное КП (*.docx|*.docx",
				Multiselect = false
			};
			if ((bool) opfd.ShowDialog())
			{
				Path = opfd.FileName;
				Continue.Execute(opfd.SafeFileName.Replace(".docx", ""));
			}
		}));

		private ICommand _reload;

		/// <summary>
		/// Обновить список
		/// </summary>
		public ICommand Reload => _reload ?? (_reload = new Command(async () =>
		{
			Preloader.Visibility = Visibility.Visible;
			await GetElements(ReadyDocs, ReadyExcels, Directory.GetCurrentDirectory() + @"\SaveDocs");
			Preloader.Visibility = Visibility.Collapsed;
		}));

		private ICommand _continue;

		/// <summary>
		/// Перейти к предпросмотру
		/// </summary>
		public ICommand Continue => _continue ?? (_continue = new Command(async p =>
		{
			Preloader.Visibility = Visibility.Visible;
			await Task.Run(async () =>
			{
				await Dispatcher.Invoke(async () =>
				{
				    if (Namedoc.Contains("xlsx"))
				    {
				        ExcelSingleton.getInstance("EXCEL VIEWER").Application.Workbooks
				            .Open(await WebApi.DownDoc(Path, Namedoc));
                    }
				    else
				    {
				        WordSingleton.getInstance("WORD VIEWER").Application.Documents
				            .Open(await WebApi.DownDoc(Path, Namedoc));
                    }
					
				});
			});
			NavigationService.Navigate(new Preview(ID_doc, null, this) {TextName = p?.ToString()});
		}));

		/// <summary>
		/// Закрыть окно
		/// </summary>
		private ICommand _close;

		public ICommand Close => _close ?? (_close = new Command(() => { NavigationService.Navigate(null); }));
		private ICommand _changetable;

		public ICommand Changetable => _changetable ?? (_changetable = new Command(() =>
		{
			if (Sebes.Visibility == Visibility.Collapsed)
			{
				KPs.Visibility = Visibility.Collapsed;
				Sebes.Visibility = Visibility.Visible;
				Tittle.Text = "Название таблицы себестоимости";
			}
			else
			{
				KPs.Visibility = Visibility.Visible;
				Sebes.Visibility = Visibility.Collapsed;
				Tittle.Text = "Название КП";
			}
		}));

		#endregion

		#region События

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		private async void ReadyKP_OnLoaded(object sender, RoutedEventArgs e)
		{
			Preloader.Visibility = Visibility.Visible;
			await GetElements(ReadyDocs, ReadyExcels, Directory.GetCurrentDirectory() + @"\SaveDocs");
			Preloader.Visibility = Visibility.Collapsed;
		}

		#endregion
	}

	public class ReadyDoc : INotifyPropertyChanged
	{
		private string _nameDoc;

		public string NameDoc
		{
			get => _nameDoc;
			set
			{
				_nameDoc = value;
				OnPropertyChanged();
			}
		}

		private string _dateDoc;

		public string DateDoc
		{
			get => _dateDoc;
			set
			{
				_dateDoc = value;
				OnPropertyChanged();
			}
		}

		private string _path;

		public string Path
		{
			get => _path;
			set
			{
				_path = value;
				OnPropertyChanged();
			}
		}

		private CornerRadius _cornerfist;

		public CornerRadius CornerFist
		{
			get => _cornerfist;
			set
			{
				_cornerfist = value;
				OnPropertyChanged();
			}
		}

		private CornerRadius _cornerlast;

		public CornerRadius CornerLast
		{
			get => _cornerlast;
			set
			{
				_cornerlast = value;
				OnPropertyChanged();
			}
		}


		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}