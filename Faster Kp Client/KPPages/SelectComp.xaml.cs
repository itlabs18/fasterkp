﻿using Faster_Kp_Client.Annotations;
using Faster_Kp_Client.controls;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using Faster_Kp_Client.Models;
using Faster_Kp_Client.Utilities;

namespace Faster_Kp_Client.KPPages
{
	/// <summary>
	/// Логика взаимодействия для SelectComp.xaml
	/// </summary>
	public partial class SelectComp : System.Windows.Controls.Page, INotifyPropertyChanged
	{
		public SelectComp(DescriptionDevice DeSe, SelectPropertyDevice SePeDe)
		{
			InitializeComponent();
			DS = DeSe;
			SPD = SePeDe;

		}

		
		//private async Task<bool> GetTableAsync()
		//{
		//	return await Task.Run(() =>
		//	{
		//		_appExcel = new Microsoft.Office.Interop.Excel.Application();
		//		try
		//		{
		//			Workbook _workbook = null;
		//			Dispatcher.Invoke(() => { _workbook = _appExcel.Workbooks.Open(Directory.GetCurrentDirectory() + @"\base\" + DS.Type + @"\" + SPD.Name + ".xlsx"); });
		//			Worksheet _worksheet = (Worksheet) _workbook.Sheets[1];
		//			foreach (Range x in _worksheet.Columns[1].Cells)
		//			{
		//				if (x.Row > 50)
		//					break;
		//				string head = Convert.ToString(x.Value);
		//				if (!string.IsNullOrEmpty(head))
		//				{
		//					ObservableCollection<Item> items = new ObservableCollection<Item>();
		//					foreach (Range y in _worksheet.Rows[x.Row].Cells)
		//					{
		//						if (y.Column == x.Column)
		//							continue;
		//						if (y.Column > 20)
		//							break;
		//						Range p = _worksheet.Cells[y.Row + 1, y.Column];
		//						string property = Convert.ToString(y.Value);
		//						string price = Convert.ToString(p.Value);
		//						if (!string.IsNullOrEmpty(property) || !string.IsNullOrEmpty(price))
		//						{
		//							items.Add(new Item() {Price = price, Property = property});
		//						}
		//					}

		//					Dispatcher.Invoke(() =>
		//					{
		//						Collectionheads.Add(new Headers() {Head = head, Collection = items});
		//					});
		//				}
		//			}

		//			Dispatcher.Invoke(() =>
		//			{
		//				_appExcel.ActiveWorkbook.Close(SaveChanges: false);
		//				_appExcel.Quit();
		//			});
		//		}
		//		catch (Exception e)
		//		{
		//			Dispatcher.Invoke(() =>
		//			{
		//				_appExcel.ActiveWorkbook.Close(SaveChanges: false);
		//				_appExcel.Quit();
		//			});
		//			return false;
		//		}

		//		return true;
		//	});
		//}

		private RbButton _rdButton { get; set; }

		public RbButton RdButton
		{
			get => _rdButton;
			set
			{
				_rdButton = value;
			}
		}

		private string Concat(ObservableCollection<RbButton> ob)
		{
			string content = "";
			string dopcontent1 = ";";
			string dopcontent2 = ";";
			string dopcontent3 = ";";
			if (ob.Any(t=>t.Head.Contains("ОЗУ")|| t.Head.Contains("Накопители") ||t.Head.Contains("Дополнительное оборудование")))
				foreach (RbButton y in ob.Where(t =>
					t.Head.Contains("ОЗУ") || t.Head.Contains("Накопители") ||
					t.Head.Contains("Дополнительное оборудование")))
				{
					
				}
			foreach (RbButton x in ob)
			{
				//if(x.Head.Contains("ОЗУ") || x.Head.Contains("НАКОПИТЕЛИ") || x.Head.Contains("Дополнительное оборудование"))
				switch (x.Head)
				{
					case "ОЗУ":
						dopcontent1 = dopcontent1!=";"?dopcontent1.Replace(";", ",- " + (x.Properties ?? "") + ";"): dopcontent1.Replace(";", " - " + (x.Properties ?? "") + ";");
						break;
					case "Накопители":
						dopcontent2 = dopcontent2 != ";" ? dopcontent2.Replace(";", ",- " + (x.Properties ?? "") + ";") : dopcontent2.Replace(";", " - " + (x.Properties ?? "") + ";");
						break;
					case "Дополнительное оборудование":
						dopcontent3 = dopcontent3 != ";" ? dopcontent3.Replace(";", ",- " + (x.Properties ?? "") + ";") : dopcontent3.Replace(";", " - " + (x.Properties ?? "") + ";");
						break;
					default:
						content += (x.Head ?? "") + " - " + (x.Properties ?? "") + "; ";
						break;
				}
				
			}
			content += dopcontent1 == ";" ? "" : "ОЗУ" + dopcontent1;
			content += dopcontent2 == ";" ? "" : "Накопители" + dopcontent2;
			content += dopcontent3 == ";" ? "" : dopcontent3;

			return content;
		}

		private string GetPrice(ObservableCollection<RbButton> ob)
		{
			int sum = 0;
			int dollar = 0;
		    if (SPD.Name.Contains("Computer"))
		    {
		        var x = DS.comp.Extradevice;
		        DS.comp = new ComputerParts{Extradevice = x};
		    }
		    if (SPD.Name.Contains("Extra"))
		    {
		        DS.comp.Extradevice = new List<string[]>();
		    }
            foreach (RbButton x in ob)
			{
			    dollar = Convert.ToInt32(x.Price??"0");
				if (x.Price.Contains("$"))
				{
					dollar = Convert.ToInt32((Convert.ToDouble((x.Price.Replace("$", "")))) * (65.63));
					sum += dollar;
				}
				else
				{
					sum += Convert.ToInt32(x.Price??"0");
				}
                
			    switch (x.Head)
			    {
			        case "ОЗУ":
			            DS.comp.OZU.Add(new[] {x.Properties, dollar.ToString() });
			            break;
			        case "Накопители":
			            DS.comp.Disk.Add(new[] { x.Properties, dollar.ToString() });
                        break;
			        case "Видеокарты":
			            DS.comp.VideoCard = new[] { x.Properties, dollar.ToString() };
			            break;
			        case "Процессоры":
			            DS.comp.Processor = new[] { x.Properties, dollar.ToString() };
			            break;
			        case "Блок питания":
			            DS.comp.Block = new[] { x.Properties, dollar.ToString() };
			            break;
			        case "Материнская плата":
			            DS.comp.Mat = new[] { x.Properties, dollar.ToString() };
			            break;
			        case "Кулер ЦПУ":
			            DS.comp.Cooler = new[] { x.Properties, dollar.ToString() };
			            break;
			        case "Дополнительное оборудование":
			            DS.comp.Extradevice.Add(new []{x.Properties, dollar.ToString() });
			            break;
                }
            }

			return sum.ToString();
		}

		private ICommand _ok;

		public ICommand OK => _ok ?? (_ok = new Command(() =>
		{
			SPD.Placehold = Concat(Rb);
			SPD.Price = GetPrice(Rb);
			SPD.ColorFrom = Colors.Gold;
			NavigationService.Navigate(DS);
		}));

		private ICommand _checkButton;

		public ICommand CheckButton => _checkButton ?? (_checkButton = new Command(p =>
		{
			RdButton = (RbButton) p;
				

			if (RdButton.what)
			{
				RdButton.IsChecked = false;
				for (int i = 0; i < Rb.Count; i++)
				{
					if (Rb[i]==RdButton&&Rb[i].GroupName!="")
						Rb.Remove(Rb[i]);
				}
				return;
			}
			for (int i=0;i<Rb.Count;i++)
			{
				
				if (Rb[i].GroupName == RdButton.GroupName&& (RdButton.GroupName!="ОЗУ"&&RdButton.GroupName != "Накопители" && RdButton.GroupName != "Дополнительное оборудование"))
				Rb.Remove(Rb[i]);
			}
			if ((bool) RdButton.IsChecked)
			{
				
				Rb.Add(RdButton);
			}
			
				
		}));

		private ICommand _close;
		public ICommand Close => _close ?? (_close = new Command(() =>
		{
			NavigationService.Navigate(DS);
		}));

	    private ICommand _info;
	    public ICommand Info => _info ?? (_info = new Command(p =>
	    {
            RbButton rb = p as RbButton;
	        InfoCheck.NameWindow = rb.Properties;
	        InfoCheck.Descv = rb.Descript;
	        InfoCheck.Visibility = Visibility.Visible;
	    }));

        private ObservableCollection<RbButton> _rb;
		public ObservableCollection<RbButton> Rb
		{
			get => _rb ?? (Rb = new ObservableCollection<RbButton>());
			set
			{
				_rb = value;
				_rb.CollectionChanged += _rb_CollectionChanged;
				OnPropertyChanged();
			}
		}

		private void _rb_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			ButtonOk.IsEnabled = _rb.Any();
		}

		private SelectPropertyDevice _spd;

		public SelectPropertyDevice SPD
		{
			get => _spd;
			set
			{
				_spd = value;
				OnPropertyChanged();
			}
		}

		private DescriptionDevice _ds;

		public DescriptionDevice DS
		{
			get => _ds;
			set
			{
				_ds = value;
				OnPropertyChanged();
			}
		}



		private ObservableCollection<Headers> _collectionheads;

		public ObservableCollection<Headers> Collectionheads
		{
			get => _collectionheads ?? (_collectionheads = new ObservableCollection<Headers>());
			set
			{
				_collectionheads = value; 
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}


		private async void SelectComp_OnLoaded(object sender, RoutedEventArgs e)
		{
			Collectionheads = Collectionheads.Any(x => !x.Head.Contains("ERROR, BASE NOT FOUND"))? Collectionheads: await WebApi.GetTable(null, DS.Type, SPD.Name) ?? new ObservableCollection<Headers>()
			{
				new Headers
				{
					Head = "ERROR, BASE NOT FOUND"
				}
			};
			if (Collectionheads.Any(x => x.Head.Contains("ERROR, BASE NOT FOUND")))
			{
				SPD.Placehold = "Ошибка базы";
				NavigationService.Navigate(DS);
			}
			else
			{
				Preloader.Visibility = Visibility.Collapsed;
			}
		}
		
		public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
		{
			if (depObj != null)
			{
				for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
				{
					DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
					if (child != null && child is T)
					{
						yield return (T)child;
					}

					foreach (T childOfChild in FindVisualChildren<T>(child))
					{
						yield return childOfChild;
					}
				}
			}
		}



	}
	
}


