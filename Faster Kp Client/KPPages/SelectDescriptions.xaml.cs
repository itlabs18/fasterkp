﻿using Faster_Kp_Client.Annotations;
using Faster_Kp_Client.controls;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using Faster_Kp_Client.Utilities;

namespace Faster_Kp_Client.KPPages
{
	/// <summary>
	/// Логика взаимодействия для SelectDescriptions.xaml
	/// </summary>
	public partial class SelectDescriptions : System.Windows.Controls.Page, INotifyPropertyChanged
    {
        public SelectDescriptions(DescriptionDevice DeSe, SelectPropertyDevice SePeDe, string diag=null)
        {
            InitializeComponent();
	        DS = DeSe;
	        SPD = SePeDe;
	        Diag = diag;

        }

	    private Microsoft.Office.Interop.Excel.Application _appExcel;

		private string _diag {get; set;}
		public string Diag
		{
			get => _diag;
			set => _diag = value;
		}

	    private async void SelectDescriptions_OnLoaded(object sender, RoutedEventArgs e)
	    {
		    Collectionheads = Collectionheads.Any(x => !x.Head.Contains("ERROR, BASE NOT FOUND")) ?Collectionheads: await WebApi.GetTable(Diag, DS.Type, SPD.Name)?? new ObservableCollection<Headers>()
		    {
				new Headers
				{
					Head = "ERROR, BASE NOT FOUND"
				}
		    };

		    if (Collectionheads.Any(x=>x.Head.Contains("ERROR, BASE NOT FOUND")))
		    {
			    SPD.Placehold = "Ошибка базы";
			    NavigationService.Navigate(DS);
			}
		    else
		    {
			    Preloader.Visibility = Visibility.Collapsed;
		    }
	    }
		//private async Task<bool> GetTableAsync()
	 //   {
		//  return await Task.Run(() => 
		//    { 
		//	    _appExcel = new Microsoft.Office.Interop.Excel.Application();
		//	    try
		//	    {
		//		    Workbook _workbook = null;
		//		    if (string.IsNullOrEmpty(Diag))
		//		    {
		//				Dispatcher.Invoke(() => { _workbook = _appExcel.Workbooks.Open(Directory.GetCurrentDirectory() + @"\base\" + DS.Type + @"\" + SPD.Name + ".xlsx"); });
		//			}
		//		    else
		//		    {
		//				Dispatcher.Invoke(() => { _workbook = _appExcel.Workbooks.Open(Directory.GetCurrentDirectory() + @"\base\" + DS.Type + @"\"+Diag+ @"\" + SPD.Name + ".xlsx"); });
		//			}
					
		//		    Worksheet _worksheet = (Worksheet) _workbook.Sheets[1];
		//		    foreach (Range x in _worksheet.Columns[1].Cells)
		//		    {
		//			    if (x.Row > 50)
		//				    break;
		//			    string head = Convert.ToString(x.Value);
		//			    if (!string.IsNullOrEmpty(head))
		//			    {
		//				    ObservableCollection<Item> items = new ObservableCollection<Item>();
		//				    foreach (Range y in _worksheet.Rows[x.Row].Cells)
		//				    {
		//					    if (y.Column == x.Column)
		//						    continue;
		//					    if (y.Column > 20)
		//						    break;
		//					    Range p = _worksheet.Cells[y.Row + 1, y.Column];
		//					    string property = Convert.ToString(y.Value);
		//					    string price = Convert.ToString(p.Value);
		//					    if (!string.IsNullOrEmpty(property) || !string.IsNullOrEmpty(price))
		//					    {
		//						    items.Add(new Item() {Price = price, Property = property});
		//					    }
		//				    }

		//					Dispatcher.Invoke(() =>
		//					{
		//						Collectionheads.Add(new Headers() {Head = head, Collection = items});
		//					});
		//			    }
		//		    }

		//		    Dispatcher.Invoke(() =>
		//		    {
		//			    _appExcel.ActiveWorkbook.Close(SaveChanges: false);
		//				_appExcel.Quit();
		//		    });
		//	    }
		//	    catch(Exception e)
		//	    {
		//		    Dispatcher.Invoke(() =>
		//		    {
		//			    _appExcel.ActiveWorkbook.Close(SaveChanges: false);
		//				_appExcel.Quit();
		//		    });
		//		    return false;
		//	    }
		//	    return true;
		//	});
		//}

	    private RbButton _rdButton;
	    public RbButton RdButton
	    {
		    get => _rdButton;
		    set
		    {
			    _rdButton = value;
			    OnPropertyChanged();
		    }
	    }

	    private ICommand _ok;
	    public ICommand OK => _ok ?? (_ok = new Command(() =>
	    {
		    SPD.Placehold = (RdButton.Properties??"");
            SPD.Price = RdButton.Price??"";
		    SPD.ColorFrom = Colors.Gold;
		    if (SPD.Number == "1")
		    {
			    DS.Body.ColorFrom = Colors.White;
		        DS.Body.Price = "0";
			    DS.Typesensor.ColorFrom = Colors.White;
		        DS.Typesensor.Price = "0";
                DS.Body.Placehold = "Выберите материал корпуса";
			    DS.Typesensor.Placehold = "Выберите тип сенсорного ввода";
		        DS.Typesensor.PageView = null;
		        DS.Body.PageView = null;
            }
		    NavigationService.Navigate(DS);
	    }));
	    private ICommand _checkButton;
	    public ICommand CheckButton => _checkButton ?? (_checkButton = new Command(p =>
	    {
		    RdButton = (RbButton)p;
		    if ((bool)RdButton.IsChecked)
			    ButtonOk.IsEnabled = true;
		    else
			    ButtonOk.IsEnabled = false;
	    }));
	    private ICommand _close;
	    public ICommand Close => _close ?? (_close = new Command(() =>
	    {
		    try
		    {
			    _appExcel.ActiveWorkbook.Close(SaveChanges: false);
			    _appExcel.Quit();
		    }
		    catch
		    {

			}
			_appExcel = null;
			NavigationService.Navigate(DS);
	    }));
        private ICommand _info;
        public ICommand Info => _info ?? (_info = new Command(p =>
        {
            RbButton rb = p as RbButton;
            InfoCheck.NameWindow = rb.Properties;
            InfoCheck.Descv = rb.Descript;
            InfoCheck.Visibility = Visibility.Visible;
        }));
        private SelectPropertyDevice _spd;
	    public SelectPropertyDevice SPD
	    {
		    get => _spd;
		    set
		    {
			    _spd = value;
			    OnPropertyChanged();
		    }
	    }

		private DescriptionDevice _ds;
	    public DescriptionDevice DS
	    {
		    get => _ds;
		    set
		    {
			    _ds = value;
				OnPropertyChanged();
		    }
	    }

		

		private ObservableCollection<Headers> _collectionheads;

	    public ObservableCollection<Headers> Collectionheads
	    {
		    get => _collectionheads ?? (_collectionheads = new ObservableCollection<Headers>());
		    set
		    {
			    _collectionheads = value;
				OnPropertyChanged();
		    }
	    }

	    public event PropertyChangedEventHandler PropertyChanged;

	    [NotifyPropertyChangedInvocator]
	    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
	    {
		    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	    }

	   
    }
	public class Headers : INotifyPropertyChanged
	{
		private string _head;
		public string Head
		{
			get => _head;
			set
			{
				_head = value;
				OnPropertyChanged();
			}
		}
		private ObservableCollection<Item> _collection;
		public ObservableCollection<Item> Collection
		{
			get => _collection ?? (_collection = new ObservableCollection<Item>());
			set
			{
				_collection = value;
				OnPropertyChanged();
			}
		}


		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
	
	public class Item : INotifyPropertyChanged
	{
		private string _property;
		public string Property
		{
			get => _property;
			set
			{
				_property = value;
				OnPropertyChanged();
			}
		}
		private string _price;
		public string Price
		{
			get => _price;
			set
			{
				_price = value;
				OnPropertyChanged();
			}
		}
	    private string _desc;
	    public string Desc
	    {
	        get => _desc;
	        set
	        {
	            _desc = value;
	            OnPropertyChanged();
	        }
	    }

        public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
