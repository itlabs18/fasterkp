﻿using Faster_Kp_Client.Annotations;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Faster_Kp_Client.Utilities;
using Color = System.Windows.Media.Color;
using Image = System.Drawing.Image;

namespace Faster_Kp_Client.KPPages
{
	/// <summary>
	/// Логика взаимодействия для SelectModel.xaml
	/// </summary>
	
    
	public partial class SelectModel : Page, INotifyPropertyChanged
	{
		public SelectModel(Page DeSe, CostPrice cp)
		{
			InitializeComponent();
			if (DeSe is DescriptionDevice)
				DS = DeSe as DescriptionDevice;
			else
				OT = DeSe as OtherType;
			PG = DeSe;
		    _cp = cp;
		    
		}
        
	    

	    

		private ObservableCollection<ImageModels> _modelses;
		public ObservableCollection<ImageModels> Modelses
		{
			get => _modelses??(_modelses=new ObservableCollection<ImageModels>());
			set
			{
				_modelses = value;
				OnPropertyChanged();
			}
		}
		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

	    private CostPrice _cp;
		private Page _pg;
		public Page PG
		{
			get => _pg;
			set
			{
				_pg = value;
				OnPropertyChanged();
			}
		}
		private DescriptionDevice _ds;
		public DescriptionDevice DS
		{
			get => _ds;
			set
			{
				_ds = value;
				OnPropertyChanged();
			}
		}
		private OtherType _otherType;
		public OtherType OT
		{
			get => _otherType;
			set
			{
				_otherType = value;
				OnPropertyChanged();
			}
		}
		private ImageModels _rdButton;
		public ImageModels RdButton
		{
			get => _rdButton;
			set
			{
				_rdButton = value;
				OnPropertyChanged();
			}
		}
		private ICommand _ok;
		public ICommand OK => _ok ?? (_ok = new Command(() =>
		{
			if (DS != null)
			{
				DS.Model.Visibility = Visibility.Visible;
				DS.Model.Content = RdButton.Name;
				DS.Model.Tag = RdButton.Path;
				DS.Panel.Visibility = Visibility.Collapsed;
			}
			else
			{
				OT.Model.Visibility = Visibility.Visible;
				OT.Model.Content = RdButton.Name;
				OT.Model.Tag = RdButton.Path;
				OT.Panel.Visibility = Visibility.Collapsed;
			}
			NavigationService.Navigate(PG);
			Modelses.Clear();
			
			

		}));
		private ICommand _checkButton;
		public ICommand CheckButton => _checkButton ?? (_checkButton = new Command(p =>
		{
			RdButton = (ImageModels)p;
				ButtonOk.IsEnabled = true;
		}));
		private ICommand _close;
		public ICommand Close => _close ?? (_close = new Command(() =>
		{if(DS!=null)
			DS.ReadySolution.IsChecked = false;
			else
			{
				OT.ReadySolution.IsChecked = false;
			}
            NavigationService.LoadCompleted += NavigationService_LoadCompleted;
		    NavigationService.Navigate(PG);
		    this.KeepAlive = false;
		    this.Content = null;
            this.Dispose(this);
		}));

        private void NavigationService_LoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            Frame frame = sender as Frame;
            
            while (frame.NavigationService.CanGoBack)
            {
                frame.NavigationService.RemoveBackEntry();
            }

            foreach (System.Windows.Controls.Image image in Images.Img)
            {
                image.Source = null;
            }

            foreach (BMImages image in Images.Bmi)
            {
                
                image.img = null;
            }
                
            Images.Bmi.Clear();
            Images.Img.Clear();
            frame.LoadCompleted -= NavigationService_LoadCompleted;
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
        }

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
	    public static extern bool DeleteObject(IntPtr hObject);


	   

        private void Dispose(SelectModel sel)
	    {
	        sel = new SelectModel(null,null);
	    }

		private async void SelectModel_OnLoaded(object sender, RoutedEventArgs e)
		{
			Modelses = await WebApi.GetModels(PG is DescriptionDevice ? DS.Type : OT.Type)??new ObservableCollection<ImageModels>
			{
				new ImageModels
				{
					Name = "ERROR, BASE NOT FOUND"
				}
			};
			if (!Modelses.Any())
			{
				if (PG is DescriptionDevice)
					DS.ReadySolution.Content = "Ошибка базы";
				else
					OT.ReadySolution.Content = "Ошибка базы";
				NavigationService.Navigate(PG);
			}
			else
			{
				Preloader.Visibility = Visibility.Collapsed;
			}
		}

		//private async Task<bool> getImages()
		//{
		//	return await Task.Run(() =>
		//	{
		//		try
		//		{
		//			var files = Directory.GetFiles(Directory.GetCurrentDirectory()+@"\models\"+((PG is DescriptionDevice)?DS.Type:OT.Type));
		//			foreach (var x in files.Where(t => t.Contains(".png")))
		//			{
		//				string match = Regex.Match(x, @"(?<=\\)([^\\]*?)(?=.png)").Value;
		//				Dispatcher.Invoke(() =>
		//				{
		//					ImageSource img = (ImageSource) new ImageSourceConverter().ConvertFromString(x);
		//					img.Freeze();
		//					Modelses.Add(new ImageModels
		//						{ Name = match, Path = img });

		//				});	
						
		//			}

		//			return true;
		//		}
		//		catch (Exception e)
		//		{
		//			return false;
		//		}
				

		//	});

		//}
	}

	public class ImageModels:INotifyPropertyChanged
	{
		private string _path;
		public string Path
		{
			get => _path;
			set
			{
				_path = value;
				OnPropertyChanged();
			}
		}
		private string _name;
		public string Name
		{
			get => _name;
			set
			{
				_name = value;
				OnPropertyChanged();
			}
		}



		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
