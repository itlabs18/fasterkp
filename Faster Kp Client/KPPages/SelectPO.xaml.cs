﻿using Faster_Kp_Client.Annotations;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Faster_Kp_Client.KPPages
{
	/// <summary>
	/// Логика взаимодействия для SelectPO.xaml
	/// </summary>
	public partial class SelectPO : Page, INotifyPropertyChanged

    {
        public SelectPO(DescriptionDevice DeSe)
        {
            InitializeComponent();
	        DS = DeSe;

        }

	    private DescriptionDevice ds; 
		public DescriptionDevice DS
		{
			get => ds;
			set
			{
				ds = value;
				OnPropertyChanged();
			}
		}
	    private RadioButton _rdButton;
	    public RadioButton RdButton
	    {
		    get => _rdButton;
		    set
		    {
			    _rdButton = value;
				OnPropertyChanged();
		    }
	    }

	    private string _price;
	    public string Price
	    {
		    get => _price;
		    set
		    {
			    _price = value;
				OnPropertyChanged();
		    }
	    }
	    private ICommand _ok;
		public ICommand OK => _ok ?? (_ok = new Command(() =>
		{
			DS.PO.Placehold = RdButton.Content.ToString();
			DS.PO.Price = string.IsNullOrWhiteSpace(Price)?"0":Price;
			DS.PO.ColorFrom = Colors.Gold;
			NavigationService.Navigate(DS);
		}));
	    private ICommand _checkButton;
	    public ICommand CheckButton => _checkButton ?? (_checkButton = new Command(p =>
	    {
		    RdButton = (RadioButton)p;
		    if ((bool) RdButton.IsChecked)
			    ButtonOk.IsEnabled = true;
		    else
				ButtonOk.IsEnabled = false;
		}));
	    private ICommand _close;
		public  ICommand Close => _close??(_close = new Command(() => { NavigationService.Navigate(DS);}));

	    public event PropertyChangedEventHandler PropertyChanged;

	    [NotifyPropertyChangedInvocator]
	    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
	    {
		    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	    }

	    private void Price_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
	    {
		    e.Handled = !(char.IsDigit(e.Text, 0));
		}
    }
}
