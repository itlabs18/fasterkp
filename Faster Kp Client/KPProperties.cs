﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Faster_Kp_Client.Annotations;

namespace Faster_Kp_Client
{
    class KPProperties: INotifyPropertyChanged
    {
	    private string _korpus;
	    public string Korpus
	    {
		    get => _korpus;
		    set
		    {
			    _korpus = value;
				OnPropertyChanged();
		    }
	    }

	    private string _display;
	    public string Display
	    {
		    get => _display;
		    set
		    {
			    _display = value;
				OnPropertyChanged();
		    }
	    }

	    private string _computer;
	    public string Computer
	    {
		    get => _computer;
		    set
		    {
			    _computer = value;
				OnPropertyChanged();
		    }
	    }

	    private string _typesensor;
	    public string Typesensor
	    {
		    get => _typesensor;
		    set
		    {
			    _typesensor = value;
				OnPropertyChanged();
		    }
	    }

	    private string _dopdevice;
		public string Dopdevice
		{
		    get => _dopdevice;
		    set
		    {
			    _dopdevice = value;

				OnPropertyChanged();
		    }
	    }

	    private string _po;
	    public string PO
	    {
		    get => _po;
		    set
		    {
			    _po = value;
				OnPropertyChanged();
		    }
	    }

	    private string _count;
	    public string Count
	    {
		    get => _count;
		    set
		    {
			    _count = value;
				OnPropertyChanged();
		    }
	    }

	    public event PropertyChangedEventHandler PropertyChanged;
	    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
	    {
		    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	    }
    }
}
