﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Faster_Kp_Client.Annotations;
using Faster_Kp_Client.KPPages;
using Faster_Kp_Client.Models;
using Faster_Kp_Client.Utilities;
using Border = System.Windows.Controls.Border;
using Control = System.Windows.Controls.Control;

namespace Faster_Kp_Client
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window, INotifyPropertyChanged
	{
		public MainWindow()
		{
			InitializeComponent();
			ExeptionEx += CloseWord;
			MainDispatcher = Dispatcher;
		}
		private ICommand _navigateTo;
		public ICommand NavigateTo => _navigateTo ?? (_navigateTo = new Command(async () =>
			                              {
				                              Preloader.Visibility = Visibility.Visible;
				                              if (File.Exists("session.dat"))
				                              {
					                              try
					                              {
													  using (StreamReader sr = new StreamReader("session.dat", Encoding.UTF8))
													  {
														  UserModel user = await WebApi.GetUser(int.Parse(sr.ReadLine()));
														  if (user.Is_active && user.Login == sr.ReadLine() && user.Password == sr.ReadLine())
														  {
															  UserSingleton.instance = null;
															  UserSingleton.GetInstance(user, this);
															  Account.Name = user.Name;
															  Account.Surname = user.Surname;
															  Account.Position = user.Position;
															  Account.Icon = "http://195.133.1.197:2019/Api/user/getimage/?nameImage=" + user.Image_name;
															  Pages.Navigate(new TypeOfDevise(this));
														  }
														  else
														  {
															  File.Delete("session.dat");
															  Pages.Navigate(new Autorization(this));
														  }

													  }
					                              }
					                              catch (Exception e)
					                              {

						                              File.Delete("session.dat");
						                              Pages.Navigate(new Autorization(this));
					                              }
											  }
				                              else
					                              Pages.Navigate(new Autorization(this));
				                              Preloader.Visibility = Visibility.Collapsed;
										  }
		));

		public Dispatcher MainDispatcher;
		

		private void CloseWord(object sender, EventArgs e)
		{
			try
			{
				try
				{
					
					WordSingleton.instance.Application.Documents.Close(SaveChanges: false);
				}
				catch { }

				WordSingleton.instance.Application.Quit(SaveChanges: false);
			}
			catch { }
			try
			{
				WordSingleton.instance.Application = null;
				WordSingleton.instance = null;
			}
			catch { }
			KillWordandExcel.Kill();
		}

		private UserWho _account;
		public UserWho Account
		{
			get => _account ?? (_account = new UserWho());
			set
			{
				_account = value;
				OnPropertyChanged();
			}
		}

		private ObservableCollection<FrameworkElement> _collection;
		public ObservableCollection<FrameworkElement> Collection
		{
			get => _collection ?? (_collection = new ObservableCollection<FrameworkElement>());
			set
			{
				_collection = value;
				OnPropertyChanged();
			}
		}

		private void Viewbox_OnMouseDown(object sender, MouseButtonEventArgs e)
		{
			if (Collection.Any(t => (t.RenderTransform as TranslateTransform).X != 0))
			{
				Collection.First(x => (x.RenderTransform as TranslateTransform).X != 0).RenderTransform = new TranslateTransform(0,0);
			}
			
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		private async void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
		{
			Collection.Add(Boar);
			Collection.Add(NotTears);
			Collection.Add(Think);
			Collection.Add(Do);
			Collection.Add(Coolman);
			KillWordandExcel.Kill();
            

		}

		public event UnhandledExceptionEventHandler ExeptionEx;

		private async void MainWindow_OnClosed(object sender, EventArgs e)
		{
			try
			{
				try
				{
					WordSingleton.instance.Application.Documents.Close(SaveChanges: false);
				}
				catch
				{
				}
				WordSingleton.instance.Application.Quit(SaveChanges: false);

			}
			catch (Exception exception)
			{

			}

			try
			{
				WordSingleton.instance.Application = null;
				WordSingleton.instance = null;
				KillWordandExcel.Kill();
				UserSingleton.instance.Model = null;
			}
			catch
			{
				try
				{
					UserSingleton.instance.Model = null;
				}
				catch { }

			} 
		}
	}

	public static class KillWordandExcel
	{
		public static void Kill()
		{
            ClearSingle();
			try
			{
				var process = Process.GetProcessesByName("winword").Where(proc =>
					proc.MainWindowTitle.Contains("WORD VIEWER") || proc.MainWindowTitle == "");
				for (int i = 0; i < process.Count(); i++)
				{
					process.First().Kill();
				}
			}
			catch (Exception e)
			{

			}

			try
			{
				var process = Process.GetProcessesByName("excel").Where(proc =>
					proc.MainWindowTitle.Contains("EXCEL VIEWER") || proc.MainWindowTitle == "");
				for (int i = 0; i < process.Count(); i++)
				{
					process.First().Kill();
				}
			}
			catch (Exception exception)
			{

			}
		}

	    private static void ClearSingle()
	    {
            WordSingleton.instance?.Application?.Quit(null);
	        ExcelSingleton.instance?.Application?.Quit();
	        WordSingleton.instance = null;
	        ExcelSingleton.instance = null;
	    }
	}
	public class UserWho : INotifyPropertyChanged
	{
		private string _name;
		public string Name
		{
			get => _name;
			set
			{
				_name = value;
				OnPropertyChanged();
			}
		}

		private string _surname;
		public string Surname
		{
			get => _surname;
			set
			{
				_surname = value;
				OnPropertyChanged();
			}
		}

		private string _position;
		public string Position
		{
			get => _position;
			set
			{
				_position = value;
				OnPropertyChanged();
			}
		}

		private string _icon;
		public string Icon
		{
			get => _icon;
			set
			{
				_icon = value;
				OnPropertyChanged();
			}
		}

		public async Task<bool> CheckTask(MainWindow mw, bool x)
		{
				UserModel user = null;
				if (x)
				{
				user = await WebApi.GetUser(UserSingleton.instance.Model.ID);
				//user = new UserModel
				//	{
				//		Login = "Petr",
				//		Password = "ПЕТРОВИЧ",
				//		Name = "Чича",
				//		Surname = "Чепаев",
				//		Position = "Могущий",
				//		ID = 4,
				//		Image_name = @"C:\Users\Николай\Pictures\Camera Roll\WIN_20181004_09_28_15_Pro.jpg",
				//		Is_active = true
				//	};
					if (user.Is_active)
					{
						Name = user.Name;
						Surname = user.Surname;
						Position = user.Position;
						Icon = "http://195.133.1.197:2019/Api/user/getimage/?nameImage=" + user.Image_name;
						UserSingleton.instance.Model = user;
						Thread.Sleep(7000);
						return await CheckTask(mw, user.Is_active);
					}
				}
				Dispatcher ds = mw.MainDispatcher;
				ds.Invoke(() =>
				{
					File.Delete("session.dat");
					UserSingleton.instance = null;
					mw.Account = null;
				});
				 ds.Invoke(() =>
				{
					mw.Pages.Navigate(new Autorization(mw));
				});
				return false;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		
	}
}
