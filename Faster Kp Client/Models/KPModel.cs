﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Faster_Kp_Client.KPPages;

namespace Faster_Kp_Client.Models
{
    public	class KPModel
    {
	    private string _header { get; set; }
	    public string Header
	    {
		    get => _header;
		    set => _header = value;
	    }
		private string _name { get; set; }
	    public string Name
	    {
		    get => _name;
		    set => _name = value;
	    }
		private string _body { get; set; }
		public string Body
		{
			get => _body;
			set => _body = value;
		}
	    private string _display { get; set; }
	    public string Display
	    {
		    get => _display;
		    set => _display = value;
	    }
		private string _computer { get; set; }
		public string Computer
		{
			get => _computer;
			set => _computer = value;
		}

        private ComputerParts _computerParts;
        public ComputerParts ComputerParts
        {
            get => _computerParts;
            set => _computerParts = value;
        }
	    private string _extradevice { get; set; }
	    public string Extradevice
	    {
		    get => _extradevice;
		    set => _extradevice = value;
	    }
	    private string _typesensor { get; set; }
	    public string Typesensor
	    {
		    get => _typesensor;
		    set => _typesensor = value;
	    }
	    private string _price { get; set; }
	    public string Price
	    {
		    get => _price;
		    set => _price = value;
	    }
	    private string _typePO { get; set; }
	    public string TypePO
	    {
		    get => _typePO;
		    set => _typePO = value;
	    }
		private string _path { get; set; }
	    public string Path
	    {
		    get => _path;
		    set => _path = value;
	    }
		private string _cost { get; set; }
	    public string Cost
	    {
		    get => _cost;
		    set => _cost = value;
	    }
		private string _count { get; set; }
	    public string Count
	    {
		    get => _count;
		    set => _count = value;
	    }
        private string _type { get; set; }
        public string Type
        {
            get => _type;
            set => _type = value;
        }

        
		private DescriptionDevice _page;
	    public DescriptionDevice Page
	    {
		    get => _page;
		    set => _page = value ?? (_page = new DescriptionDevice(null));
	    }
    }
    public class ComputerParts
    {
        private string[] _block;
        public string[] Block
        {
            get => _block?? (_block = new string[] { });
            set => _block = value;
        }
        private string[] _mat;
        public string[] Mat
        {
            get => _mat ?? (_mat = new string[] { });
            set => _mat = value;
        }
        private List<string[]> _ozu;

        public List<string[]> OZU
        {
            get => _ozu ??(_ozu = new List<string[]>());
            set => _ozu = value;
        }

        private string[] _processor;
        public string[] Processor
        {
            get => _processor ?? (_processor = new string[] { });
            set => _processor = value;
        }
        private List<string[]> _disk;

        public List<string[]> Disk
        {
            get => _disk ?? (_disk = new List<string[]>());
            set => _disk = value;
        }

        private string[] _videCard;
        public string[] VideoCard
        {
            get => _videCard ?? (_videCard = new string[] { });
            set => _videCard = value;
        }

        private string[] _cooler;

        public string[] Cooler
        {
            get => _cooler ?? (_cooler = new string[] { });
            set => _cooler = value;
        }
        private List<string[]> _extradevice { get; set; }
        public List<string[]> Extradevice
        {
            get => _extradevice ?? (_extradevice = new List<string[]>());
            set => _extradevice = value;
        }
    }
}
