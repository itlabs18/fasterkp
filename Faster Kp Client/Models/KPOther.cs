﻿using Faster_Kp_Client.KPPages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Faster_Kp_Client.Annotations;

namespace Faster_Kp_Client.Models
{
	public class KPOther
	{
		private string _header { get; set; }
		public string Header
		{
			get => _header;
			set { _header = value; }
		}

		private string _name { get; set; }
		public string Name
		{
			get => _name;
			set { _name = value; }
		}

		private string _price { get; set; }
		public string Price
		{
			get => _price;
			set { _price = value; }
		}

		private string _path { get; set; }
		public string Path
		{
			get => _path;
			set { _path = value; }
		}
		private string _cost { get; set; }
		public string Cost
		{
			get => _cost;
			set { _cost = value; }
		}
		private string _count { get; set; }

		public string Count
		{
			get => _count;
			set { _count = value; }
		}
	    private string _type { get; set; }
	    public string Type
	    {
	        get => _type;
	        set => _type = value;
	    }

        private ObservableCollection<PropertiesOther> _collection { get; set; }
		public ObservableCollection<PropertiesOther> CollectionOthers
		{
			get => _collection ?? (_collection = new ObservableCollection<PropertiesOther>());
			set => _collection = value;
		}
		private OtherType _page;
		public OtherType Page
		{
			get => _page;
			set { _page = value ?? (_page = new OtherType(null)); }
		}
	}

	public class PropertiesOther : INotifyPropertyChanged
	{
		private string _name;
		public string Name
		{
			get => _name ?? "";
			set
			{
				_name = value;
				OnPropertyChanged();
			}
		}
		private string _prop;
		public string Prop
		{
			get => _prop ?? "";
			set
			{
				_prop = value;
				OnPropertyChanged();
			}
		}
		private string _price;
		public string Price
		{
			get => _price ?? "";
			set
			{
				_price = value;
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
