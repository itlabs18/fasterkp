﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Faster_Kp_Client.Annotations;

namespace Faster_Kp_Client.Models
{
    public class RadioModel : INotifyPropertyChanged
    {
	    private bool _ischecked;
	    public bool IsChecked
	    {
		    get => _ischecked;
		    set
		    {
			    _ischecked = value;
				OnPropertyChanged();
		    }
	    }

	    private object _page;

	    public object Page
	    {
		    get => _page;
		    set
		    {
			    _page = value;
				OnPropertyChanged();
		    }
	    }


	    public event PropertyChangedEventHandler PropertyChanged;

	    [NotifyPropertyChangedInvocator]
	    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
	    {
		    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	    }
    }
}
