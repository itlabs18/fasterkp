﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Faster_Kp_Client.Annotations;

namespace Faster_Kp_Client.Models
{
	public class UserModel: INotifyPropertyChanged
	{
		public int ID { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }
		private string _name;
		public string Name
		{
			get => _name;
			set
			{
				_name = value;
				OnPropertyChanged();
			}
		}

		private string _position;
		public string Position { get=>_position;
			set
			{
				_position = value;
				OnPropertyChanged();
			}
		}

		private string _surname;
		public string Surname
		{
			get => _surname;
			set
			{
				_surname = value;
				OnPropertyChanged();
			}
		}

		public string Telefon_number { get; set; }
		public string Image_name { get; set; }
		public bool Is_admin { get; set; }
		public bool Is_active { get; set; }

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
