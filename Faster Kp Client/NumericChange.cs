﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Faster_Kp_Client
{
	class NumericChange : INotifyPropertyChanged
	{
		private bool _Checked;

		public bool Checked
		{
			get => _Checked;
			set
			{
				_Checked = value;
				if (!_Checked)
				Text = "0";
				OnPropertyChanged();
			}
		}

		private string _text = "0";
		public string Text
		{
			get => _text;

			set
			{
				
				_text = value;
				if (_text == "")
					_text += "0";
				OnPropertyChanged();
			}	
		}
		public event PropertyChangedEventHandler PropertyChanged;
		public void OnPropertyChanged([CallerMemberName] string prop = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
		}
		
	}
}
