﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Faster_Kp_Client.controls;
using Faster_Kp_Client.KPPages;
using  Faster_Kp_Client.Utilities;

namespace Faster_Kp_Client
{
	/// <summary>
	/// Логика взаимодействия для Type_of_Devise.xaml
	/// </summary>
	public partial class TypeOfDevise : Page
	{
		public TypeOfDevise(MainWindow mw)
		{
			InitializeComponent();
			main = mw;

		}

		private MainWindow main;

		//private void ButtonSub_OnClick(object sender, RoutedEventArgs e)
		//{
		//	NumericChange NC = (NumericChange)this.Resources["NumericChange"];
		//	if ((Convert.ToInt32(NC.Text) > 0))
		//	NC.Text = (Convert.ToInt32(NC.Text) - 1).ToString();
		//}

		//private void ButtonAdd_OnClick(object sender, RoutedEventArgs e)
		//{
		//	NumericChange NC = (NumericChange)this.Resources["NumericChange"];
		//	NC.Text = (Convert.ToInt32(NC.Text) + 1).ToString();
		//}

		private ICommand _backMain;
		public ICommand BackMain => _backMain ?? (_backMain = new Command(async() =>
		{
			main.MainDispatcher.Invoke(() => { main.Account = null; });
		    await main.MainDispatcher.InvokeAsync(async () => { UserSingleton.instance.Model = await WebApi.GetUser(UserSingleton.instance.Model.ID, true); });
			main.MainDispatcher.Invoke(() => { main.Pages.Navigate(null); });

		}));
		private ICommand _checkKP;
		public ICommand CheckKp => _checkKP ?? (_checkKP = new Command(() =>
		{
			Prew.Navigate(new ReadyKP(this,main));
		}));

		private ICommand _navigate;
		public ICommand Navigate => _navigate ?? (_navigate = new Command(() =>
		{
			DeviceSingle.getInstance(null).Device[0].Value = InteractiveBoard.Checked;
			DeviceSingle.getInstance(null).Device[1].Value = SensorKiosk.Checked;
			DeviceSingle.getInstance(null).Device[2].Value = InteractivePanel.Checked;
			DeviceSingle.getInstance(null).Device[3].Value = SensorTerminal.Checked;
			DeviceSingle.getInstance(null).Device[4].Value = VirtualPromouter.Checked;
			DeviceSingle.getInstance(null).Device[5].Value = HolographicFan.Checked;
			DeviceSingle.getInstance(null).Device[6].Value = HolographicPyramid.Checked;
			DeviceSingle.getInstance(null).Device[7].Value = HolographicCube.Checked;
            NavigationService.Navigate(new CostPrice(this, main));
        }));

		private void TypeOfDevise_OnLoaded(object sender, RoutedEventArgs e)
		{
			string[] keys = new []
			{
				InteractiveBoard.TextName,
				SensorKiosk.TextName,
				InteractivePanel.TextName,
				SensorTerminal.TextName,
				VirtualPromouter.TextName,
				HolographicFan.TextName,
				HolographicPyramid.TextName,
				HolographicCube.TextName
			};
			DeviceSingle.getInstance(keys);

		}

	    private void TypeOfDevise_OnPreviewKeyDown(object sender, KeyEventArgs e)
	    {
	        if (e.Key == Key.Enter)
	        {
	            NextButton.Focus();
                NextButton.IsDefault = true;
	        }
	    }
	}
}
