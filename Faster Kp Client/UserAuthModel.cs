﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Faster_Kp_Client
{
	class UserAuthModel : INotifyPropertyChanged
	{
		private string _email;
		public  string Email
		{
			get => _email;
			set
			{
				ErrorEmail = string.Empty;
				_email = value;
				OnPropertyChanged();
			}
		}
		private string _password;
		public string Password
		{
			get => _password;
			set
			{
				ErrorPassword=String.Empty;
				_password = value;
				OnPropertyChanged();
			}
		}

		private string _errorEmail;
		public string ErrorEmail
		{
			get => _errorEmail;
			set
			{
				_errorEmail = value;
				OnPropertyChanged();
			}
		}
		private string _errorpassword;
		public string ErrorPassword
		{
			get => _errorpassword;
			set
			{
				_errorpassword = value;
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
		public void OnPropertyChanged([CallerMemberName] string prop = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
		}
	}
	
}
