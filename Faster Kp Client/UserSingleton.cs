﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Faster_Kp_Client.Models;
using Faster_Kp_Client.Utilities;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Animation;
using Faster_Kp_Client.Annotations;

namespace Faster_Kp_Client
{
	public class UserSingleton : Animatable, INotifyPropertyChanged
    {


		public static UserSingleton instance;

        public static readonly DependencyProperty CheckSebesProperty = DependencyProperty.Register(
            "CheckSebes", typeof(Visibility), typeof(UserSingleton), new PropertyMetadata(default(Visibility)));

        public Visibility CheckSebes
        {
            get { return (Visibility) GetValue(CheckSebesProperty); }
            set { SetValue(CheckSebesProperty, value); }
        }

        public static readonly DependencyProperty OpacityProperty = DependencyProperty.Register(
            "Opacity", typeof(double), typeof(UserSingleton), new PropertyMetadata((double)1));

        public double Opacity
        {
            get { return (double) GetValue(OpacityProperty); }
            set { SetValue(OpacityProperty, value); }
        }

        private UserModel _model;
		public UserModel Model
		{
			get => _model;
			set
			{
				_model = value;
				OnPropertyChanged();
			}
		}

		protected UserSingleton(UserModel model)
		{
			Model = model;
		}

		public static UserSingleton GetInstance(UserModel model, MainWindow mw)
		{
			
			return instance??(instance = new UserSingleton(model));
		}
		


		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

        protected override Freezable CreateInstanceCore()
        {
            throw new System.NotImplementedException();
        }
    }
}