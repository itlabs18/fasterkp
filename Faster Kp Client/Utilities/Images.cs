﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Faster_Kp_Client.Utilities
{
    public static class Images
    {
        private static ObservableCollection<Image> _img; 
        public static ObservableCollection<Image> Img
        {
            get => _img??(_img = new ObservableCollection<Image>());
            set => _img = value;
        }

        private static ObservableCollection<BMImages> _bmi;
        public static ObservableCollection<BMImages> Bmi
        {
            get => _bmi ?? (_bmi = new ObservableCollection<BMImages>());
            set => _bmi = value;
        }
    }

    public class BMImages
    {
        public BitmapImage img;
    }
}
