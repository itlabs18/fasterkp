﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Faster_Kp_Client.KPPages;
using Faster_Kp_Client.Models;
using Newtonsoft.Json;
using MailMessage = Microsoft.Office.Interop.Word.MailMessage;
using Task = System.Threading.Tasks.Task;

namespace Faster_Kp_Client.Utilities
{
	public static class WebApi
	{
		private static string _host = /*@"http://localhost:57911/Api"*/@"http://195.133.1.197:2019/Api";
		private static HttpClient _httpClient = new HttpClient();

		public static async Task<UserModel> Login(string login, string password)
		{

			//ByteArrayContent content = new ByteArrayContent(File.ReadAllBytes(""));
			//_httpClient.PostAsync("", content);
			return await await _httpClient.GetAsync($"{_host}/User/LoginUserAsync?login={login}&password={password}").ContinueWith(async r =>
			{
				if (r.IsFaulted) return null;
				UserModel model = JsonConvert.DeserializeObject<UserModel>(await r.Result.Content.ReadAsStringAsync());
				return model;
			});
		}
		public static async Task<bool> SendTrello(string adress, string file)
		{
			return await Task.Run(() =>
			{
				try
				{
					using (SmtpClient client = new SmtpClient("smtp.mail.ru"))
					{
						client.EnableSsl = true;
						client.Port = 25;
						client.Credentials = new NetworkCredential("it@bm-technology.ru", "technology10");
						System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
						mailMessage.From = new MailAddress("it@bm-technology.ru", "IT отдел BMGroup");
						mailMessage.To.Add(adress);
						mailMessage.Subject = "Новое КП";
						using (Attachment attach = new Attachment(file))
						{
							mailMessage.Attachments.Add(attach);
							client.Send(mailMessage);
						}						
					}
					return true;
				}
				catch (Exception e)
				{
					return false;
				}
			
			});
		}

		public static async Task<ImageSource> GetImage(string nameImage)
		{
			return await await _httpClient.GetAsync($"{_host}/user/getimage?nameImage={nameImage}")
				.ContinueWith(
					async r =>
					{
						if (r.IsFaulted) return null;
						return JsonConvert.DeserializeObject<ImageSource>(await r.Result.Content.ReadAsStringAsync());
					});

		}
		public static async Task<ObservableCollection<Headers>> GetTable(string Diag, string Type, string Name)
		{
			return await await _httpClient.GetAsync($"{_host}/table/gettableasync?Diag={Diag}&Type={Type}&Name={Name}")
				.ContinueWith(
					async r =>
					{
						if (r.IsFaulted) return null;
						ObservableCollection<Headers> collection = JsonConvert.DeserializeObject<ObservableCollection<Headers>>(await r.Result.Content.ReadAsStringAsync());
						return collection;
					});

		}
		public static async Task<ObservableCollection<ImageModels>> GetModels(string Type)
		{
			return await await _httpClient.GetAsync($"{_host}/table/getmodelsasync?Type={Type}")
				.ContinueWith(
					async r =>
					{
						if (r.IsFaulted) return null;
						ObservableCollection<ImageModels> collection = JsonConvert.DeserializeObject<ObservableCollection<ImageModels>>(await r.Result.Content.ReadAsStringAsync());
						return collection;
					});

		}
		public static async Task<UserModel> GetUser(int ID, bool logout = false)
		{
			return await await _httpClient.GetAsync($"{_host}/user/getuserasync?ID={ID}&logout={logout}")
				.ContinueWith(
					async r =>
					{
						if (r.IsFaulted) return null;
						UserModel user = JsonConvert.DeserializeObject<UserModel>(await r.Result.Content.ReadAsStringAsync());
						return user;
					});

		}
		public static async Task<BaseModel> GetBaseModel(string adress, string login)
		{
			return await await _httpClient.GetAsync($"{_host}/table/GetID?adress={adress}&login={login}")
				.ContinueWith(
					async r =>
					{
						if (r.IsFaulted) return null;
						BaseModel user = JsonConvert.DeserializeObject<BaseModel>(await r.Result.Content.ReadAsStringAsync());
						return user?? new BaseModel();
					});

		}
		public static async Task<ObservableCollection<BaseModel>> GetDocs(string login)
		{
			return await await _httpClient.GetAsync($"{_host}/table/getdocsasync/?login={login}")
				.ContinueWith(
					async r =>
					{
						if (r.IsFaulted) return null;
						var x = await r.Result.Content.ReadAsStringAsync();
						ObservableCollection<BaseModel> user = JsonConvert.DeserializeObject<ObservableCollection<BaseModel>>(await r.Result.Content.ReadAsStringAsync());
						return user;
					});

		}
		public static async Task<string> DownDoc(string path, string name)
		{
			return await await _httpClient.GetAsync($"{_host}/table/downdocasync?path={path}")
				.ContinueWith(
					async r =>
					{
						if (r.IsFaulted) return null;
						byte[] user = JsonConvert.DeserializeObject <byte[]>(await r.Result.Content.ReadAsStringAsync());
						FileStream fs = File.Create(Directory.GetCurrentDirectory() + $@"\SaveDocs\{name}", (int) user.Length);
						fs.Write(user,0,user.Length);
						fs.Dispose();
						return Directory.GetCurrentDirectory()+$@"\SaveDocs\{name}";
					});

		}
		public static async Task<bool> UploadDoc(string phe, string phd, string login, string name, int ID)
		{
			var content = new StringContent(JsonConvert.SerializeObject(new UploadModel()
			{
				login = login,
				name = name,
				pathd = File.Exists(phd) ? File.ReadAllBytes(phd) : null,
				pathe = File.Exists(phe) ? File.ReadAllBytes(phe) : null,
				ID = ID
			}));
			content.Headers.ContentType = JsonMediaTypeFormatter.DefaultMediaType;
			return await await _httpClient.PostAsync($"{_host}/table/uploaddocasync", content)
				.ContinueWith(
					async r =>
					{
						if (r.IsFaulted) return false;
						
						return JsonConvert.DeserializeObject<bool>(await r.Result.Content.ReadAsStringAsync());
					});

		}

		public static async Task<string> GetDollar()
		{
			return await await _httpClient.GetAsync($"{_host}/table/getdollar")
				.ContinueWith(
					async r =>
					{
						if (r.IsFaulted) return null;
						string x = JsonConvert.DeserializeObject<string>(await r.Result.Content.ReadAsStringAsync());
						return x;
					});
		}

	}

	public class UploadModel
	{
		public byte[] pathd { get; set; }
		public byte[] pathe { get; set; }
		public string login { get; set; }
		public string name { get; set; }
		public int ID { get; set; }
	}
}
