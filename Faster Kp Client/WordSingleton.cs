﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Word = Microsoft.Office.Interop.Word;
using Excel = Microsoft.Office.Interop.Excel;

namespace Faster_Kp_Client
{
	class WordSingleton
	{
		public static WordSingleton instance;

		private Word.Application _application;

		public Word.Application Application
		{
			get => _application ?? (_application = new Word.Application());
			set
			{
				_application = value;
			}
		}

		protected WordSingleton(string _caption)
		{
			Application = new Word.Application()
			{
				Caption = _caption
			};
		}

		#region Инстанс
		///<summary>
		///Создать синглтон
		///</summary>
		/// <param name="caption">Заголовок окна</param>
		public static WordSingleton getInstance(string caption)
		{
			return instance ?? (instance = new WordSingleton(caption));
		}
		#endregion
	}
	class ExcelSingleton
	{
		public static ExcelSingleton instance;

		private Excel.Application _application;

		public Excel.Application Application
		{
			get => _application ?? (_application = new Excel.Application());
			set
			{
				_application = value;
			}
		}

		protected ExcelSingleton(string _caption)
		{
			Application = new Excel.Application()
			{
				Caption = _caption
			};
		}
		#region Инстанс
		///<summary>
		///Создать синглтон
		///</summary>
		/// <param name="caption">Заголовок окна</param>
		public static ExcelSingleton getInstance(string caption)
		{
			return instance ?? (instance = new ExcelSingleton(caption));
		}
		#endregion
	}
}
