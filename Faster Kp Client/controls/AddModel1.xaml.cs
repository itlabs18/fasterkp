﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Faster_Kp_Client.controls
{
    /// <summary>
    /// Логика взаимодействия для AddModel1.xaml
    /// </summary>
    public partial class AddModel1 : UserControl
    {
        public AddModel1()
        {
            InitializeComponent();
        }

	    public static readonly DependencyProperty CommandProperty = DependencyProperty.Register(
		    "Command", typeof(ICommand), typeof(AddModel1), new PropertyMetadata(default(ICommand)));

	    public ICommand Command
	    {
		    get { return (ICommand) GetValue(CommandProperty); }
		    set { SetValue(CommandProperty, value); }
	    }

	    public static readonly DependencyProperty CommandParameterProperty = DependencyProperty.Register(
		    "CommandParameter", typeof(object), typeof(AddModel1), new PropertyMetadata(default(object)));

	    public object CommandParameter
	    {
		    get { return (object) GetValue(CommandParameterProperty); }
		    set { SetValue(CommandParameterProperty, value); }
	    }
	   
    }
}
