﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Faster_Kp_Client.controls
{
    /// <summary>
    /// Логика взаимодействия для Button_CostPrice.xaml
    /// </summary>
    public partial class Button_CostPrice : UserControl
    {
        public Button_CostPrice()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register(
            "Command", typeof(object), typeof(Button_CostPrice), new PropertyMetadata(default(object)));

        public object Command
        {
            get { return (object) GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public static readonly DependencyProperty MarginSvgProperty = DependencyProperty.Register(
            "MarginSvg", typeof(Thickness), typeof(Button_CostPrice), new PropertyMetadata(default(Thickness)));

        public Thickness MarginSvg
        {
            get { return (Thickness) GetValue(MarginSvgProperty); }
            set { SetValue(MarginSvgProperty, value); }
        }

        public static readonly DependencyProperty DirectionProperty = DependencyProperty.Register(
            "Direction", typeof(double), typeof(Button_CostPrice), new PropertyMetadata((double)315));

        public double Direction
        {
            get { return (double) GetValue(DirectionProperty); }
            set { SetValue(DirectionProperty, value); }
        }

        public static readonly DependencyProperty ColorBackgroundProperty = DependencyProperty.Register(
            "ColorBackground", typeof(Color), typeof(Button_CostPrice), new PropertyMetadata(Color.FromArgb(001,00,00,00)));

        public Color ColorBackground
        {
            get => (Color) GetValue(BackgroundProperty);
            set => SetValue(BackgroundProperty, value);
        }

        public static readonly DependencyProperty DataPathProperty = DependencyProperty.Register(
            "DataPath", typeof(object), typeof(Button_CostPrice), new PropertyMetadata(default(object)));

        public object DataPath
        {
            get => (string) GetValue(DataPathProperty);
            set => SetValue(DataPathProperty, value);
        }
    }
}
