﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Faster_Kp_Client.controls
{
    /// <summary>
    /// Логика взаимодействия для CheckBoxText.xaml
    /// </summary>
    public partial class CheckBoxText : UserControl
    {
        public CheckBoxText()
        {
            InitializeComponent();
            
        }

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
            "Text", typeof(string), typeof(CheckBoxText), new PropertyMetadata(default(string)));

        public string Text
        {
            get { return (string) GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty FontSizeToggleProperty = DependencyProperty.Register(
            "FontSizeToggle", typeof(object), typeof(CheckBoxText), new PropertyMetadata(default(object)));

        public object FontSizeToggle
        {
            get { return (object) GetValue(FontSizeToggleProperty); }
            set { SetValue(FontSizeToggleProperty, value); }
        }

        public static readonly DependencyProperty FontFamilyToggleProperty = DependencyProperty.Register(
            "FontFamilyToggle", typeof(object), typeof(CheckBoxText), new PropertyMetadata(default(object)));

        public object FontFamilyToggle
        {
            get { return (object) GetValue(FontFamilyToggleProperty); }
            set { SetValue(FontFamilyToggleProperty, value); }
        }

        public static readonly DependencyProperty BackToggleProperty = DependencyProperty.Register(
            "BackToggle", typeof(object), typeof(CheckBoxText), new PropertyMetadata(default(object)));

        public object BackToggle
        {
            get { return (object) GetValue(BackToggleProperty); }
            set { SetValue(BackToggleProperty, value); }
        }

        public static readonly DependencyProperty BorderBrushToggleProperty = DependencyProperty.Register(
            "BorderBrushToggle", typeof(object), typeof(CheckBoxText), new PropertyMetadata(default(object)));

        public object BorderBrushToggle
        {
            get { return (object) GetValue(BorderBrushToggleProperty); }
            set { SetValue(BorderBrushToggleProperty, value); }
        }

        public static readonly DependencyProperty DataPathProperty = DependencyProperty.Register(
            "DataPath", typeof(object), typeof(CheckBoxText), new PropertyMetadata(default(object)));

        public object DataPath
        {
            get { return (object) GetValue(DataPathProperty); }
            set { SetValue(DataPathProperty, value); }
        }

        public static readonly DependencyProperty IsCheckedProperty = DependencyProperty.Register(
            "IsChecked", typeof(bool), typeof(CheckBoxText), new PropertyMetadata(default(bool)));

        public bool IsChecked
        {
            get { return (bool) GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }
    }
}
