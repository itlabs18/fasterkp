﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Faster_Kp_Client.controls
{
	/// <summary>
	/// Логика взаимодействия для Devices.xaml
	/// </summary>
	public partial class Devices : UserControl
	{
		public Devices()
		{
			InitializeComponent();
		}

		public static readonly DependencyProperty CheckedProperty = DependencyProperty.Register(
			"Checked", typeof(bool), typeof(Devices), new PropertyMetadata(default(bool)));

		public bool Checked
		{
			get { return (bool) GetValue(CheckedProperty); }
			set { SetValue(CheckedProperty, value); }
		}

		public static readonly DependencyProperty TextNameProperty = DependencyProperty.Register(
			"TextName", typeof(string), typeof(Devices), new PropertyMetadata(default(string)));

		public string TextName
		{
			get { return (string) GetValue(TextNameProperty); }
			set { SetValue(TextNameProperty, value); }
		}

		public static readonly DependencyProperty ImageSvgProperty = DependencyProperty.Register(
			"ImageSvg", typeof(object), typeof(Devices), new PropertyMetadata(default(object)));

		public object ImageSvg
		{
			get { return (object) GetValue(ImageSvgProperty); }
			set { SetValue(ImageSvgProperty, value); }
		}
	}
}
