﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Faster_Kp_Client.Annotations;

namespace Faster_Kp_Client.controls
{
	/// <summary>
	/// Логика взаимодействия для DownloadQuest.xaml
	/// </summary>
	public partial class DownloadQuest : UserControl, INotifyPropertyChanged
	{
		public DownloadQuest()
		{
			InitializeComponent();
		}

		#region DependencyProperties

		public static readonly DependencyProperty ErrorTextProperty = DependencyProperty.Register(
			"ErrorText", typeof(string), typeof(DownloadQuest), new PropertyMetadata(default(string)));

		public string ErrorText
		{
			get { return (string) GetValue(ErrorTextProperty); }
			set { SetValue(ErrorTextProperty, value); }
		}

		public static readonly DependencyProperty CommandProperty = DependencyProperty.Register(
			"Command", typeof(ICommand), typeof(DownloadQuest), new PropertyMetadata(default(ICommand)));

		public ICommand Command
		{
			get { return (ICommand) GetValue(CommandProperty); }
			set { SetValue(CommandProperty, value); }
		}

		public static readonly DependencyProperty EmptyProperty = DependencyProperty.Register(
			"Empty", typeof(bool), typeof(DownloadQuest), new PropertyMetadata(true));

		public bool Empty
		{
			get => (bool) GetValue(EmptyProperty);
			set => SetValue(EmptyProperty, value);
		}

		public static readonly DependencyProperty NameofKPProperty = DependencyProperty.Register(
			"NameofKP", typeof(string), typeof(DownloadQuest), new PropertyMetadata(default(string)));

		public string NameofKP
		{
			get { return (string) GetValue(NameofKPProperty); }
			set { SetValue(NameofKPProperty, value); }
		}

		public static readonly DependencyProperty DateTextProperty = DependencyProperty.Register(
			"DateText", typeof(string), typeof(DownloadQuest), new PropertyMetadata(DateTime.Today.Date.ToShortDateString() + " г."));

		public string DateText
		{
			get => (string) GetValue(DateTextProperty);
			set => SetValue(DateTextProperty, value);
		}

		public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
			"Text", typeof(string), typeof(DownloadQuest), new PropertyMetadata(""));

		public string Text
		{
			get => (string) GetValue(TextProperty);
			set => SetValue(TextProperty, value);
		}
		#endregion



		private ICommand _close;
		public ICommand Close => _close ?? (_close = new Command(() =>
		{
			Text = "";
			Control.Visibility = Visibility.Collapsed;
		}));


		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
