﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Faster_Kp_Client.controls
{
    /// <summary>
    /// Логика взаимодействия для Info.xaml
    /// </summary>
    public partial class Info : UserControl
    {
        public Info()
        {
            InitializeComponent();
        }
        private ICommand _close;
        public ICommand Close => _close ?? (_close = new Command(() => { this.Visibility = Visibility.Collapsed; }));

        public static readonly DependencyProperty NameWindowProperty = DependencyProperty.Register(
            "NameWindow", typeof(string), typeof(Info), new PropertyMetadata(default(string)));

        public string NameWindow
        {
            get { return (string) GetValue(NameWindowProperty); }
            set { SetValue(NameWindowProperty, value); }
        }

        public static readonly DependencyProperty DescvProperty = DependencyProperty.Register(
            "Descv", typeof(string), typeof(Info), new PropertyMetadata(default(string)));

        public string Descv
        {
            get { return (string) GetValue(DescvProperty); }
            set { SetValue(DescvProperty, value); }
        }
    }
}
