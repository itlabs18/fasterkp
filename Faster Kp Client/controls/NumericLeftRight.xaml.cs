﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Faster_Kp_Client.Annotations;

namespace Faster_Kp_Client.controls
{
    /// <summary>
    /// Логика взаимодействия для NumericLeftRight.xaml
    /// </summary>
    public partial class NumericLeftRight : UserControl, INotifyPropertyChanged
    {
        public NumericLeftRight()
        {
            InitializeComponent();
	
        }

	    public static readonly DependencyProperty EnabledProperty = DependencyProperty.Register(
		    "Enabled", typeof(bool), typeof(NumericLeftRight), new PropertyMetadata(default(bool)));

	    public bool Enabled
	    {
		    get => (bool) GetValue(EnabledProperty);
		    set
		    {
			    SetValue(EnabledProperty, value);
			    OnPropertyChanged();
		    }

	    }

	    public static readonly DependencyProperty BackroundButtonProperty = DependencyProperty.Register(
		    "BackroundButton", typeof(Brush), typeof(NumericLeftRight), new PropertyMetadata(default(Brush)));

	    public Brush BackroundButton
	    {
		    get => (Brush) GetValue(BackroundButtonProperty);
		    set
		    {
			    SetValue(BackroundButtonProperty, value);
			    OnPropertyChanged();
		    }
	    }
		
	    public static readonly DependencyProperty TextpropProperty = DependencyProperty.Register(
		    "Textprop", typeof(string), typeof(NumericLeftRight), new PropertyMetadata("0"));

	    public string Textprop 
	    {
		    get
		    {
				return (string) GetValue(TextpropProperty);
		    }
		    set
		    {
				SetValue(TextpropProperty, value);
			    OnPropertyChanged();

		    }
	    }

	    public static readonly DependencyProperty CheckedProperty = DependencyProperty.Register(
		    "Checked", typeof(bool), typeof(NumericLeftRight), new PropertyMetadata(default(bool)));

	    public bool Checked
	    {
		    get { return (bool) GetValue(CheckedProperty); }
		    set
		    {
			    if (!(bool)GetValue(CheckedProperty))
					SetValue(TextpropProperty, "0");
			    SetValue(CheckedProperty, value);
			    OnPropertyChanged();
		    }
	    }

	    private ICommand _subtract;
	    public ICommand Subtract => _subtract ?? (_subtract = new Command(() =>
	    {
		    if ((Convert.ToInt32(Textprop) > 0))
			    Textprop = (Convert.ToInt32(Textprop) - 1).ToString();
	    }));
	    private ICommand _add;
	    public ICommand Add => _add ?? (_add = new Command(() =>
	    {
		    Textprop = (Convert.ToInt32(string.IsNullOrEmpty(Textprop)?"0":Textprop) + 1).ToString();
	    }));
		private void TextBox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
	    {
			e.Handled = !(char.IsDigit(e.Text, 0));
		}

	    public event PropertyChangedEventHandler PropertyChanged;

	    [NotifyPropertyChangedInvocator]
	    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
	    {
		    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	    }
    }
}
