﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Faster_Kp_Client.controls
{
	/// <summary>
	/// Логика взаимодействия для OtherProperty.xaml
	/// </summary>
	public partial class OtherProperty : UserControl
	{
		public OtherProperty()
		{
			InitializeComponent();
		}

		public static readonly DependencyProperty PriceProperty = DependencyProperty.Register(
			"Price", typeof(string), typeof(OtherProperty), new PropertyMetadata(string.Empty));

		public string Price
		{
			get { return (string) GetValue(PriceProperty); }
			set { SetValue(PriceProperty, value); }
		}

		public static readonly DependencyProperty PropertyNameProperty = DependencyProperty.Register(
			"PropertyName", typeof(string), typeof(OtherProperty), new PropertyMetadata(string.Empty));

		public string PropertyName
		{
			get { return (string) GetValue(PropertyNameProperty); }
			set { SetValue(PropertyNameProperty, value); }
		}

		public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
			"Text", typeof(string), typeof(OtherProperty), new PropertyMetadata(string.Empty));

		public string Text
		{
			get { return (string) GetValue(TextProperty); }
			set { SetValue(TextProperty, value); }
		}

		private void UIElement_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			e.Handled = !(char.IsDigit(e.Text, 0));
		}
	}
}
