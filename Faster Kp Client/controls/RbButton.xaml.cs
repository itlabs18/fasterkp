﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RadioButton = System.Windows.Controls.RadioButton;

namespace Faster_Kp_Client.controls
{
    /// <summary>
    /// Логика взаимодействия для RbButton.xaml
    /// </summary>
    public partial class RbButton : UserControl
    {
        public RbButton()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty CommandInfoProperty = DependencyProperty.Register(
            "CommandInfo", typeof(ICommand), typeof(RbButton), new PropertyMetadata(default(ICommand)));

        public ICommand CommandInfo
        {
            get { return (ICommand) GetValue(CommandInfoProperty); }
            set { SetValue(CommandInfoProperty, value); }
        }

        public static readonly DependencyProperty DescriptProperty = DependencyProperty.Register(
            "Descript", typeof(string), typeof(RbButton), new PropertyMetadata(default(string)));

        public string Descript
        {
            get { return (string) GetValue(DescriptProperty); }
            set { SetValue(DescriptProperty, value); }
        }

	    public static readonly DependencyProperty FlagsProperty = DependencyProperty.Register(
		    "Flags", typeof(string), typeof(RbButton), new PropertyMetadata(default(string), PropertyChangedCallback));

	    private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
	    {
		    RbButton rb = d as RbButton;
		    if (rb.Flags.Contains("ОЗУ")|| rb.Flags.Contains("Накопители")|| rb.Flags.Contains("Дополнительное оборудование"))
		    {
			    BindingOperations.ClearBinding(rb.RadioButton1, GroupNameProperty);
			    rb.RadioButton1.GroupName = null;
		    }
				
	    }

	    public string Flags
	    {
		    get { return (string) GetValue(FlagsProperty); }
		    set {SetValue(FlagsProperty, value);}
	    }

	    public bool what;

	    public static readonly DependencyProperty IsCheckedProperty = DependencyProperty.Register(
		    "IsChecked", typeof(bool), typeof(RbButton), new PropertyMetadata(false));

	    public bool IsChecked
	    {
		    get { return (bool) GetValue(IsCheckedProperty); }
		    set { SetValue(IsCheckedProperty, value); }
	    }
	    public static readonly DependencyProperty GroupNameProperty = DependencyProperty.Register(
		    "GroupName", typeof(string), typeof(RbButton), new PropertyMetadata(default(string)));

	    public string GroupName
	    {
		    get { return (string) GetValue(GroupNameProperty); }
		    set { SetValue(GroupNameProperty, value); }
	    }

	    public static readonly DependencyProperty HeadProperty = DependencyProperty.Register(
		    "Head", typeof(string), typeof(RbButton), new PropertyMetadata(default(string)));

	    public string Head
	    {
		    get { return (string) GetValue(HeadProperty); }
		    set { SetValue(HeadProperty, value); }
	    }

	    public static readonly DependencyProperty CommandProperty = DependencyProperty.Register(
		    "Command", typeof(ICommand), typeof(RbButton), new PropertyMetadata(default(ICommand)));

	    public ICommand Command
	    {
		    get { return (ICommand) GetValue(CommandProperty); }
		    set { SetValue(CommandProperty, value); }
	    }

	    public static readonly DependencyProperty CommandParameterProperty = DependencyProperty.Register(
		    "CommandParameter", typeof(object), typeof(RbButton), new PropertyMetadata(default(object)));

	    public object CommandParameter
	    {
		    get { return (object) GetValue(CommandParameterProperty); }
		    set { SetValue(CommandParameterProperty, value); }
	    }

	    public static readonly DependencyProperty PropertiesProperty = DependencyProperty.Register(
		    "Properties", typeof(string), typeof(RbButton), new PropertyMetadata(default(string)));

	    public string Properties
	    {
		    get { return (string) GetValue(PropertiesProperty); }
		    set { SetValue(PropertiesProperty, value); }
	    }

	    public static readonly DependencyProperty PriceProperty = DependencyProperty.Register(
		    "Price", typeof(string), typeof(RbButton), new PropertyMetadata(default(string)));

	    public string Price
	    {
		    get { return (string) GetValue(PriceProperty); }
		    set { SetValue(PriceProperty, value); }
	    }
	    private void UIElement_OnPreviewMouseUp(object sender, MouseButtonEventArgs e)
	    {
			RadioButton rdButton = sender as RadioButton;
		    if (rdButton.IsChecked == true)
			    what = true;
		    else
			    what = false;
	    }
    }
}
