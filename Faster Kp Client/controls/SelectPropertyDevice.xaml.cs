﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Faster_Kp_Client.Annotations;

namespace Faster_Kp_Client.controls
{
    /// <summary>
    /// Логика взаимодействия для SelectPropertyDevice.xaml
    /// </summary>
    public partial class SelectPropertyDevice : UserControl, INotifyPropertyChanged
    {
	    public SelectPropertyDevice()
	    {
		    InitializeComponent();
            
	    }

        public static readonly DependencyProperty PageViewProperty = DependencyProperty.Register(
            "PageView", typeof(Page), typeof(SelectPropertyDevice), new PropertyMetadata(default(Page)));

        public Page PageView
        {
            get { return (Page) GetValue(PageViewProperty); }
            set { SetValue(PageViewProperty, value); }
            
        }

		public static readonly DependencyProperty ColorFromProperty = DependencyProperty.Register(
		    "ColorFrom", typeof(Color), typeof(SelectPropertyDevice), new PropertyMetadata(default(Color)));

	    public Color ColorFrom
	    {
		    get { return (Color) GetValue(ColorFromProperty); }
		    set { SetValue(ColorFromProperty, value); OnPropertyChanged(); }
	    }

	    public static readonly DependencyProperty CommandParameterProperty = DependencyProperty.Register(
		    "CommandParameter", typeof(object), typeof(SelectPropertyDevice), new PropertyMetadata(default(object)));

	    public object CommandParameter
	    {
		    get { return (object) GetValue(CommandParameterProperty); }
		    set { SetValue(CommandParameterProperty, value); }
	    }

	    public static readonly DependencyProperty CommandProperty = DependencyProperty.Register(
		    "Command", typeof(ICommand), typeof(SelectPropertyDevice), new PropertyMetadata(default(ICommand)));

	    public ICommand Command
	    {
		    get { return (ICommand) GetValue(CommandProperty); }
		    set { SetValue(CommandProperty, value); }
	    }


	    public static readonly DependencyProperty NumberProperty = DependencyProperty.Register(
		    "Number", typeof(string), typeof(SelectPropertyDevice), new PropertyMetadata(default(string)));

	    public string Number
	    {
		    get { return (string) GetValue(NumberProperty); }
		    set { SetValue(NumberProperty, value); OnPropertyChanged(); }
	    }

	    public static readonly DependencyProperty FontSizeProperty = DependencyProperty.Register(
		    "FontSize", typeof(double), typeof(SelectPropertyDevice), new PropertyMetadata(default(double)));

	    public double FontSize
	    {
		    get { return (double) GetValue(FontSizeProperty); }
		    set { SetValue(FontSizeProperty, value); OnPropertyChanged(); }
	    }

	    public static readonly DependencyProperty HeadProperty = DependencyProperty.Register(
		    "Head", typeof(string), typeof(SelectPropertyDevice), new PropertyMetadata(default(string)));

	    public string Head
	    {
		    get { return (string) GetValue(HeadProperty); }
		    set { SetValue(HeadProperty, value); OnPropertyChanged(); }
	    }

	    public static readonly DependencyProperty PriceProperty = DependencyProperty.Register(
		    "Price", typeof(string), typeof(SelectPropertyDevice), new PropertyMetadata(""));

	    public string Price
	    {
		    get { return (string) GetValue(PriceProperty); }
		    set { SetValue(PriceProperty, value); OnPropertyChanged(); }
	    }

	    public static readonly DependencyProperty PlaceholdcolorProperty = DependencyProperty.Register(
		    "Placeholdcolor", typeof(Color), typeof(SelectPropertyDevice), new PropertyMetadata(default(Color)));

	    public Color Placeholdcolor
	    {
		    get { return (Color) GetValue(PlaceholdcolorProperty); }
		    set { SetValue(PlaceholdcolorProperty, value); OnPropertyChanged(); }
	    }

	    public static readonly DependencyProperty PlaceholdProperty = DependencyProperty.Register(
		    "Placehold", typeof(string), typeof(SelectPropertyDevice), new PropertyMetadata(default(string)));

       

        public string Placehold
	    {
		    get { return (string) GetValue(PlaceholdProperty); }
		    set { SetValue(PlaceholdProperty, value);
			    OnPropertyChanged();
		    }
	    }

	    public event PropertyChangedEventHandler PropertyChanged;

	    [NotifyPropertyChangedInvocator]
	    public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
	    {
		    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	    }

        public void UIElement_OnTextInput(object sender, TextCompositionEventArgs e)
        {
            
        }
    }
}
