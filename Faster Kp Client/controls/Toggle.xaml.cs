﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Faster_Kp_Client.Annotations;

namespace Faster_Kp_Client.controls
{
    /// <summary>
    /// Логика взаимодействия для Toggle.xaml
    /// </summary>
    public partial class Toggle : UserControl, INotifyPropertyChanged
    {
        public Toggle()
        {
            InitializeComponent();
        }

	    public static readonly DependencyProperty IsCheckedProperty = DependencyProperty.Register(
		    "IsChecked", typeof(bool), typeof(Toggle), new PropertyMetadata(default(bool)));

	    public bool IsChecked
	    {
		    get => (bool) GetValue(IsCheckedProperty);
		    set
		    {
			    SetValue(IsCheckedProperty, value);
			    OnPropertyChanged();
		    }

	    }

	    public static readonly DependencyProperty BackgroundColorProperty = DependencyProperty.Register(
		    "BackgroundColor", typeof(Color), typeof(Toggle), new PropertyMetadata(default(Color)));

	    public Color BackgroundColor
	    {
		    get { return (Color) GetValue(BackgroundColorProperty); }
		    set { SetValue(BackgroundColorProperty, value); }
	    }

	    public static readonly DependencyProperty BorderColorProperty = DependencyProperty.Register(
		    "BorderColor", typeof(Color), typeof(Toggle), new PropertyMetadata(default(Color)));

	    public Color BorderColor
	    {
		    get { return (Color) GetValue(BorderColorProperty); }
		    set { SetValue(BorderColorProperty, value); }
	    }

	    public static readonly DependencyProperty ColorFillProperty = DependencyProperty.Register(
		    "ColorFill", typeof(Color), typeof(Toggle), new PropertyMetadata(default(Color)));

	    public Color ColorFill
	    {
		    get { return (Color) GetValue(ColorFillProperty); }
		    set { SetValue(ColorFillProperty, value); }
	    }

	    public event PropertyChangedEventHandler PropertyChanged;

	    private Color _botd = Color.FromArgb(255,99,99,99);


		[NotifyPropertyChangedInvocator]
	    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
	    {
		    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		   
	    }

	    //private void Toggle_OnLoaded(object sender, RoutedEventArgs e)
	    //{
		   // _botd = ;
	    //}

	    private void ToggleButton_OnChecked(object sender, RoutedEventArgs e)
	    {
			if (IsChecked)
				BorderColor = ColorFill;
		}

	    private void ToggleButton_OnUnchecked(object sender, RoutedEventArgs e)
	    {
		    if(!IsChecked)
			    BorderColor = _botd;
		}
    }
}
