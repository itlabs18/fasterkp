﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Faster_Kp_Client.Utilities;

namespace Faster_Kp_Client.controls
{
	/// <summary>
	/// Логика взаимодействия для WhoUser.xaml
	/// </summary>
	public partial class WhoUser : UserControl
	{
		public WhoUser()
		{
			InitializeComponent();
		}

		public static readonly DependencyProperty FotoWidthProperty = DependencyProperty.Register(
			"FotoWidth", typeof(GridLength), typeof(WhoUser), new PropertyMetadata(default(GridLength)));

		public GridLength FotoWidth
		{
			get { return (GridLength) GetValue(FotoWidthProperty); }
			set { SetValue(FotoWidthProperty, value); }
		}

		public static readonly DependencyProperty SingleNameProperty = DependencyProperty.Register(
			"SingleName", typeof(string), typeof(WhoUser), new PropertyMetadata(default(string)));

		public string SingleName
		{
			get { return (string) GetValue(SingleNameProperty); }
			set { SetValue(SingleNameProperty, value); }
		}

		public static readonly DependencyProperty SingleSurnameProperty = DependencyProperty.Register(
			"SingleSurname", typeof(string), typeof(WhoUser), new PropertyMetadata(default(string)));

		public string SingleSurname
		{
			get { return (string) GetValue(SingleSurnameProperty); }
			set { SetValue(SingleSurnameProperty, value); }
		}

		public static readonly DependencyProperty IconProperty = DependencyProperty.Register(
			"Icon", typeof(string), typeof(WhoUser), new PropertyMetadata(default(string)));

		public string Icon
		{
			get { return (string) GetValue(IconProperty); }
			set { SetValue(IconProperty, value); }
		}

		

		public static readonly DependencyProperty SinglePositionProperty = DependencyProperty.Register(
			"SinglePosition", typeof(string), typeof(WhoUser), new PropertyMetadata(default(string)));

		public string SinglePosition
		{
			get { return (string) GetValue(SinglePositionProperty); }
			set { SetValue(SinglePositionProperty, value); }
		}
	}
}
