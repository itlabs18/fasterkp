﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Faster_Kp_Client.Utilities;

namespace Faster_Kp_Client.converters
{
	public class CostMultiConveter : IMultiValueConverter
	{

	//	<Binding ElementName = "Body" Path="Price"/>
	//<Binding ElementName = "Display" Path="Price"/>
	//<Binding ElementName = "Computer" Path="Price"/>
	//<Binding ElementName = "Typesensor" Path="Price"/>
	//<Binding ElementName = "Extradevice" Path="Price"/>
	//<Binding ElementName = "PO" Path="Price"/>
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
		
			int cost = 0;
			foreach (var x in values.ToList().Where(t=>Array.IndexOf(values,t)<6&!string.IsNullOrEmpty((string)t)))
			{
				string price = x.ToString();
			    price = price.Replace("₽", "").Replace(" ","");
				if (x.ToString().Contains("$"))
				{
					string xui = price.Replace("$", "").Replace(".", ",");
					price = (decimal.Parse(xui) * decimal.Parse(GetDollar.Dollar.Replace(".",","))).ToString();
					price = System.Convert.ToInt32(decimal.Round(decimal.Parse(price), 1)).ToString();
				}
				cost += System.Convert.ToInt32(price.Replace(",",".")); 
			}
			string costes = (cost * int.Parse(values[6].ToString())*2).ToString();
			return costes;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
	public static class GetDollar
	{
		private static string _dollar;

		public static string Dollar
		{
			get
			{
				
				Task.WaitAny(Task.Run(async () =>
				{
					_dollar = await WebApi.GetDollar();
				}));
				return _dollar;
			}
			
		}

	}
}
