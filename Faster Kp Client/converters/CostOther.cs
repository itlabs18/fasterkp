﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Faster_Kp_Client.KPPages;
using Faster_Kp_Client.Models;

namespace Faster_Kp_Client.converters
{
	public class CostOther : IMultiValueConverter
	{

	//	<Binding ElementName = "Body" Path="Price"/>
	//<Binding ElementName = "Display" Path="Price"/>
	//<Binding ElementName = "Computer" Path="Price"/>
	//<Binding ElementName = "Typesensor" Path="Price"/>
	//<Binding ElementName = "Extradevice" Path="Price"/>
	//<Binding ElementName = "PO" Path="Price"/>
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (values[0] is ObservableCollection<PropertiesOther>)
			{
				ObservableCollection<PropertiesOther> OthP = values[0] as ObservableCollection<PropertiesOther>;
				if (OthP.All(x => !string.IsNullOrWhiteSpace(x.Price)))
				{
					int cost = 0;
					if (int.Parse((string) values.Last()) > 0 & values.Length > 1)
					{
						for (int i = 0; i < values.Length - 1; i++)
						{
							foreach (PropertiesOther xtOther in OthP)
							{
								cost += int.Parse(xtOther.Price);
							}
						}
						string costes = (cost * int.Parse(values[values.Length-1].ToString())).ToString();
						return costes;
					}
					
				}
			}
			return "0";
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
