﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Faster_Kp_Client.Utilities;
using Image = System.Windows.Controls.Image;

namespace Faster_Kp_Client.converters
{
    class ImageConverter:IMultiValueConverter
    {
        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool DeleteObject(IntPtr value);
        public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
            Images.Img.Add((Image) value[1]);
            ImageSource bitmapSource;
            //using (WebClient wc = new WebClient())
            //{
            //Image myImage = Image.FromStream(wc.OpenRead((string)value));
            //    BitmapImage img;
            //    img.BeginInit();
            //var bitmap = new Bitmap(myImage);
            //IntPtr bmpPt = bitmap.GetHbitmap();
            //bitmapSource =
            //    (ImageSource)System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
            //        bmpPt,
            //        IntPtr.Zero,
            //        Int32Rect.Empty,
            //        BitmapSizeOptions.FromEmptyOptions());

            ////freeze bitmapSource and clear memory to avoid memory leaks
            //bitmapSource.Freeze();
            //DeleteObject(bmpPt);
            //}
            BitmapImage bi = new BitmapImage();
            bi.DownloadCompleted += source2_DownloadCompleted;
            bi.BeginInit();
            bi.CacheOption = BitmapCacheOption.None;
            bi.UriSource = new Uri((string)value[0]);
            bi.EndInit();
            Images.Bmi.Add(new BMImages{img = bi});
            return bitmapSource = bi;
        }
        void source2_DownloadCompleted(object sender, EventArgs e)
        {
            BitmapImage source2 = (BitmapImage)sender;
            source2.Freeze();
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
