﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using FasterKP.Models;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity.Migrations;

namespace FasterKP.Controllers
{
    public class DevicesController : ApiController
    { 
		[HttpPost]
		public bool AddOrUpdateNewDevice([FromBody]DeviceModel deviceModel)
		{
			try
			{
				using (ContextDb contextDb = new ContextDb()) { contextDb.DeviceModels.AddOrUpdate(deviceModel); contextDb.SaveChanges(); }

				return true;
			}
			catch  {return false; }
		}
		[HttpGet]
		public bool DeleteDevice(int ID)
		{

			try
			{
				using (ContextDb contextDb = new ContextDb()) { contextDb.DeviceModels.Remove(contextDb.DeviceModels.Find(ID)); contextDb.SaveChanges(); }
				return true;
			}
			catch { return false; }
		}
		
    }
}
