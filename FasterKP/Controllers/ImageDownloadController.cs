﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;

namespace FasterKP.Controllers
{
    public class ImageDownloadController : ApiController
    {
	    private bool CheckDirectory(string directory, string imagename = null)
	    {
		    if (Directory.Exists(directory))
		    {
			    if (!string.IsNullOrEmpty(imagename))
				   return File.Exists(directory + $"\\{imagename}.png");
			    return true;
		    }
		    return false;
	    }

	    [HttpGet]
	    public async Task<byte[]> GetImage([FromUri] string nameImage)
	    {
		    try
		    {
			    if (CheckDirectory(HostingEnvironment.ApplicationPhysicalPath + $"\\Userimages"))
				    if (CheckDirectory(HostingEnvironment.ApplicationPhysicalPath + $"\\Userimages", nameImage))
				    {
					    return File.ReadAllBytes(HostingEnvironment.ApplicationPhysicalPath + $"\\Userimages\\{nameImage}.png");
				    }

			    return null;
			}
		    catch (Exception e)
		    {
			    return null;
		    }

	    }

	    [HttpPost]
	    public async Task<bool> UploadImage([FromBody]string nameImage)
	    {
		    try
		    {
				if (!CheckDirectory(HostingEnvironment.ApplicationPhysicalPath + $"\\Userimages"))
				    Directory.CreateDirectory(HostingEnvironment.ApplicationPhysicalPath + $"\\Userimages");
			    if (!CheckDirectory(HostingEnvironment.ApplicationPhysicalPath + $"\\Userimages", nameImage))
			    {
				    File.WriteAllBytes(System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + $"\\Userimages\\{nameImage}.png",
					    await Request.Content.ReadAsByteArrayAsync());
				    return true;
			    }

			    return false;

		    }
		    catch (Exception e)
		    {
			    return false;
		    }
	    }
	}
}
