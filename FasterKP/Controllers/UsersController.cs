﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FasterKP.Models;
using Microsoft.Ajax.Utilities;

namespace FasterKP.Controllers
{
    public class UsersController : ApiController
    {
		[HttpPost]
		public bool AddOrUpdateNewUser([FromBody]UserModel userModel)
		{
			try
			{
				using (ContextDb contextDb = new ContextDb()) { contextDb.UserModels.AddOrUpdate(userModel); contextDb.SaveChanges(); }
				return true;
			}
			catch  { return false; }
		}
		[HttpGet]
		public bool DeleteUser(int ID)
		{
			try
			{
				using (ContextDb contextDb = new ContextDb()) { contextDb.UserModels.Remove(contextDb.UserModels.Find(ID)); contextDb.SaveChanges(); }
				return true;
			}
			catch { return false; }
		}
		[HttpGet]
		public UserModel Login(string login, string password)
		{
			try
			{
				using (ContextDb contextDb = new ContextDb())
				{
					var user = contextDb.UserModels.FirstOrDefault(f => f.Login == login && f.Password == password);
					return user;
				}
			}
			catch { return null; }
		}
	}
}
