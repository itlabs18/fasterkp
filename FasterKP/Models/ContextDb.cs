﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FasterKP.Models
{
	public class ContextDb : DbContext 
	{
		public DbSet<DeviceModel> DeviceModels { get; set; }
		public DbSet<UserModel> UserModels { get; set; }
		public ContextDb (): base("FasterKpDB")
			{
		}

	}
}