﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FasterKP.Models
{
	public class DeviceModel
	{
		[Key]
		public int ID { get; set; }
		public string Name { get; set; }
		public int Price { get; set; }
		public List <DescriptionModel> Description { get; set; }

	}
}