﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FasterKP.Models
{
	public class UserModel
	{
		public int ID { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }
		public string Name { get; set; }
		public string Position { get; set; }
		public string Surname { get; set; }
		public string Telefon_number { get; set; }
		public  string Image_name { get; set; }
		public bool Is_admin { get; set; }
	}	
}