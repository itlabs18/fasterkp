﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FasterKpAdmin.Startup))]
namespace FasterKpAdmin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
