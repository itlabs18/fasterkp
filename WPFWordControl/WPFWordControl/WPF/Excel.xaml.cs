﻿namespace WPFWordControl
{
    #region Using
    using System;
    using System.Windows;
    using System.Windows.Controls;

	using WPFWordControl.WindowsForms;
    #endregion

    public partial class Excel : UserControl
    {
        #region Свойства

	    private ExcelWindowsForms _control;
	    public ExcelWindowsForms control
	    {
		    get => _control;
		    set
		    {
			    _control = value;
				controlChange.Invoke(null,null);
		    }
	    }

	    #endregion

        #region Конструктор
        public Excel()
        {
            InitializeComponent();
			this.Loaded += UserControl_Loaded;
			
        }
        #endregion

        #region Общие методы
        //public void Print()
        //{
        //    this.control.PrintWordDocument();
        //}
        #endregion

        #region Dependency Property
        /// <summary>
        /// Путь к файлу
        /// </summary>
        #endregion

        #region События
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
			this.control = new ExcelWindowsForms();
			this.wordHost.Child = this.control;
        }

	    public static event EventHandler controlChange;

	    #endregion
    }
}
