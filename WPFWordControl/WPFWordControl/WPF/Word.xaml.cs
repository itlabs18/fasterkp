﻿namespace WPFWordControl
{
    #region Using
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using Word1 = Microsoft.Office.Interop.Word;
	using WPFWordControl.WindowsForms;
    #endregion

    public partial class Word : UserControl
    {
        #region Свойства

	    private WordWindowsForms _control;
	    public WordWindowsForms control
	    {
		    get => _control;
		    set
		    {
			    _control = value;
				controlChange.Invoke(null,null);
		    }
	    }

	    #endregion

        #region Конструктор
        public Word()
        {
            InitializeComponent();
			this.Loaded += UserControl_Loaded;
			
        }
        #endregion

        #region Общие методы
        public void Print()
        {
            this.control.PrintWordDocument();
        }
        #endregion

        #region Dependency Property
        /// <summary>
        /// Путь к файлу
        /// </summary>
        #endregion

        #region События
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
			this.control = new WordWindowsForms();
			this.wordHost.Child = this.control;
	        
        }

	    public static event EventHandler controlChange;

	    #endregion
    }
}
