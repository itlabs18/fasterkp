﻿namespace WPFWordControl.WindowsForms
{
    partial class WordWindowsForms
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
			this.wordContainer = new System.Windows.Forms.Panel();
			this.SuspendLayout();
			// 
			// wordContainer
			// 
			this.wordContainer.BackColor = System.Drawing.SystemColors.Control;
			this.wordContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.wordContainer.Location = new System.Drawing.Point(0, 0);
			this.wordContainer.Name = "wordContainer";
			this.wordContainer.Size = new System.Drawing.Size(500, 300);
			this.wordContainer.TabIndex = 0;
			// 
			// WordWindowsForms
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.wordContainer);
			this.Name = "WordWindowsForms";
			this.Size = new System.Drawing.Size(500, 300);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel wordContainer;
    }
}
